! *****************************************************************
subroutine OutMapsFiles
USE DFLOGM
use eis_mod
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype, IDC
TYPE (dialog) dlg
external  aggiorna_outmaps
! ----------------------------------------------------------------
IF ( .not. DlgInit( IDD_OutMaps, dlg ) ) THEN
   WRITE (*,*) "Error: dialog not found"
ELSE
! Activate the modal dialog. 
do IDC = IDC_EDT_Maps1, IDC_EDT_Maps10
  index1 = IDC-IDC_EDT_Maps1+1
  retlog = DlgSet( dlg, IDC, DataMap_str(index1))
!  call aggiorna_outmaps( dlg, IDC, dlg_change)
enddo
do IDC = IDC_Maps_File1, IDC_Maps_File10
  index1 = IDC - IDC_Maps_File1 + 1
  retlog = DlgSet( dlg, IDC, fileMaps(index1))
enddo
! ...............................................................
do IDC = IDC_BTN_Maps1, IDC_BTN_Maps10
   retlog = DlgSetSub( dlg, IDC, aggiorna_outmaps )
enddo
do IDC = IDC_EDT_Maps1, IDC_EDT_Maps10
   retlog = DlgSetSub( dlg, IDC, aggiorna_outmaps )
enddo
! ...............................................................
retint = DlgModal( dlg ) 
CALL DlgUninit( dlg )
END IF
return
end
! *******************************************************************
subroutine aggiorna_outmaps ( dlg, control_name, callbacktype )
!DEC$ ATTRIBUTES DEFAULT :: aggiorna_outmaps
USE DFLOGM
use eis_mod
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype
character*256 NoCString
TYPE (dialog) dlg
! -------------------------------------------------------------
SELECT CASE (control_name)
! -------------------------------------------
  CASE (IDC_BTN_Maps1:IDC_BTN_Maps10)
    estensione = '*.flt'
    tipofile = 'Output Map Filename'
    index1 = control_name-IDC_BTN_Maps1+1
    call OpenNewFile (retint, estensione, tipofile, fileMaps(index1))
    fileMaps(index1) = NoCString(fileMaps(index1))
    retint = scan (fileMaps(index1),'.',back=.true.)
    fileMaps(index1) = trim(fileMaps(index1)(1:retint-1))
    retlog = DlgSet( dlg, control_name+100, fileMaps(index1))
    do index1=1,MAX_MAP_FILE
      if (fileMaps(index1) =='.') exit
    enddo
    no_Mappe = index1 - 1
! -------------------------------------------
  CASE (IDC_EDT_Maps1:IDC_EDT_Maps10)
    retlog = DlgGet( dlg, control_name, text )
    index1 = control_name-IDC_EDT_Maps1+1
    DataMap_str(index1) = NoCString(text)
! -------------------------------------------
END SELECT
end subroutine aggiorna_outmaps
! *******************************************************************
