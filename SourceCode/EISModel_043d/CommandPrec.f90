! *****************************************************************
subroutine PrecipitationControl
USE DFLOGM
use eis_mod
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype, IDC
TYPE (dialog) dlg
external  aggiorna_precip
! ----------------------------------------------------------------
IF ( .not. DlgInit( IDD_PrecCntrl, dlg ) ) THEN
   WRITE (*,*) "Error: dialog not found"
ELSE

call NablaByFile( dlg, control_name, callbacktype )

    write (text,'(f15.5)') NablaPMIN
    retlog = DlgSet( dlg, IDC_EDT_PNablaF1, text)
    write (text,'(f15.5)') NablaPMAX
    retlog = DlgSet( dlg, IDC_EDT_PNablaF2, text)
    write (text,'(f15.5)') NablaPrec
    retlog = DlgSet( dlg, IDC_EDT_NablaPrec, text)
    write (text,'(f15.5)') PrecEleRef
    retlog = DlgSet( dlg, IDC_EDT_NablaPrec2, text)
    write (text,'(f14.5)') SCF
    retlog = DlgSet( dlg, IDC_EDT_SCF, text)
    retlog = DlgSet( dlg, IDC_input_PNablaF, fileNablaPrec)
    retlog = DlgSet( dlg, IDC_Input_SRF, fileSRF )
    retlog = DlgSet( dlg, IDC_CHECK_NELE, normalizeELE)
    retlog = DlgSet( dlg, IDC_CHECK_NSCF, normalizeSCF)
    retlog = DlgSet( dlg, IDC_CHECK_NSRF, normalizeSRF)
! ...............................................................
    retlog = DlgSetSub( dlg, IDC_Btn_Pnabla1, aggiorna_precip )
    retlog = DlgSetSub( dlg, IDC_Btn_Pnabla2, aggiorna_precip )
    retlog = DlgSetSub( dlg, IDC_EDT_PNablaF1, aggiorna_precip )
    retlog = DlgSetSub( dlg, IDC_EDT_PNablaF2, aggiorna_precip )
    retlog = DlgSetSub( dlg, IDC_EDT_NablaPrec, aggiorna_precip )
    retlog = DlgSetSub( dlg, IDC_EDT_NablaPrec2, aggiorna_precip )
    retlog = DlgSetSub( dlg, IDC_EDT_SCF, aggiorna_precip )
    retlog = DlgSetSub( dlg, IDC_BTN_PNablaF, aggiorna_precip )
    retlog = DlgSetSub( dlg, IDC_BTN_SRF, aggiorna_precip )
    retlog = DlgSetSub( dlg, IDC_CHECK_NELE, aggiorna_precip )
    retlog = DlgSetSub( dlg, IDC_CHECK_NSCF, aggiorna_precip )
    retlog = DlgSetSub( dlg, IDC_CHECK_NSRF, aggiorna_precip )
! ...............................................................
    retint = DlgModal( dlg ) 
    CALL DlgUninit( dlg )
END IF
return
end
! *****************************************************************
subroutine PrecipitationFiles
USE DFLOGM
use eis_mod
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype
TYPE (dialog) dlg
external  aggiorna_precip
! ----------------------------------------------------------------
IF ( .not. DlgInit( IDD_PrecFiles, dlg ) ) THEN
   WRITE (*,*) "Error: dialog not found"
ELSE
! -------------------
! Activate the modal dialog.
call PrecCoordWriter ( dlg, control_name, callbacktype )
! ..................................................
retlog = DlgSetSub( dlg, IDC_EDT_PrecNo, aggiorna_precip )
retlog = DlgSetSub( dlg, IDC_BTN_Prec1, aggiorna_precip )
retlog = DlgSetSub( dlg, IDC_BTN_Prec2, aggiorna_precip )
! ...............................................................
retint = DlgModal( dlg ) 
CALL DlgUninit( dlg )
END IF
return
end
! *****************************************************************
subroutine PrecCoordWriter ( dlg, control_name, callbacktype )
USE DFLOGM
use eis_mod
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype,  j
TYPE (dialog) dlg
! -----------------------------------------------------
if (filePrecip /= '.') then
    open (99,file=filePrecip,status='old',err=50)
    read (99,*,end=50) d1       ! -------------- lettura DUMMY
    read (99,*,end=50) no_Precip, d2, d3, d4, (Pxcoord(j), j= 1, no_Precip)
    read (99,*,end=50)        d1, d2, d3, d4, (Pycoord(j), j= 1, no_Precip)
    read (99,*,end=50)        d1, d2, d3, d4, (Pzcoord(j), j= 1, no_Precip)
50  close (99)
endif

write (text,'(i10)') no_Precip
retlog = DlgSet( dlg, IDC_EDT_PrecNo, text )
retlog = DlgSet( dlg, IDC_Prec_File1, filePrecip )
retlog = DlgSet( dlg, IDC_Prec_File2, fileMonthPrec )
do j=1, no_Precip
    write (text,'(f15.2)') Pxcoord(j)
    retlog = DlgSet( dlg, IDC_EDT_PEast1+j-1, text)
    write (text,'(f15.2)') Pycoord(j)
    retlog = DlgSet( dlg, IDC_EDT_PNorth1+j-1, text)
    write (text,'(f15.2)') Pzcoord(j)
    retlog = DlgSet( dlg, IDC_EDT_PElev1+j-1, text)
enddo

return
end
! *****************************************************************
subroutine PrecLapseRWriter ( dlg, control_name, callbacktype )
USE DFLOGM
use eis_mod
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype
TYPE (dialog) dlg
! -----------------------------------------------------
retlog = DlgSet( dlg, IDC_Input_PNablaF, fileNablaPrec)
if (fileNablaPrec /= '.') then
    open (99,file=fileNablaPrec,status='old',blocksize=512,err=60)
    read (99,*,end=60) d1     ! -------------- lettura DUMMY
    read (99,*,end=60) d1, d2, d3, d4, NablaPMIN
    read (99,*,end=60) d1, d2, d3, d4, NablaPMAX
    read (99,*,end=60) d1, d2, d3, d4, PrecEleRef
60  close (99)
endif
    write (text,'(f15.5)') NablaPMIN
    retlog = DlgSet( dlg, IDC_EDT_PNablaF1, text)
    write (text,'(f15.5)') NablaPMAX
    retlog = DlgSet( dlg, IDC_EDT_PNablaF2, text)
    write (text,'(f15.5)') PrecEleRef
    retlog = DlgSet( dlg, IDC_EDT_NablaPrec2, text)
return
end
! *******************************************************************
subroutine aggiorna_precip ( dlg, control_name, callbacktype )
!DEC$ ATTRIBUTES DEFAULT :: aggiorna_precip
USE DFLOGM
use eis_mod
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype
character*256 NoCString
TYPE (dialog) dlg
! -------------------------------------------------------------
SELECT CASE (control_name)
  CASE (IDC_Btn_Pnabla1)
    NP_cost    = .false.
    call NablaByFile( dlg, control_name, callbacktype )
  CASE (IDC_Btn_Pnabla2)
    NP_cost    = .true.
    call NablaByFile( dlg, control_name, callbacktype )
  CASE (IDC_BTN_PNablaF)
    estensione = '*.csv'
    tipofile = 'Precip. Lapse Rate'
    call OpenOldFile (retint, estensione, tipofile, fileNablaPrec)
    fileNablaPrec = NoCString(fileNablaPrec)
    call PrecLapseRWriter ( dlg, control_name, callbacktype )
  CASE (IDC_EDT_NablaPrec)
    retlog = DlgGet( dlg, IDC_EDT_NablaPrec, text )
    read (text,*,iostat=retint) NablaPrec  
  CASE (IDC_EDT_NablaPrec2)
    retlog = DlgGet( dlg, IDC_EDT_NablaPrec2, text )
    read (text,*,iostat=retint) PrecEleRef  
  CASE (IDC_EDT_SCF)
    retlog = DlgGet( dlg, IDC_EDT_SCF, text )
    read (text, *, iostat=retint) SCF
  CASE (IDC_CHECK_NELE)
    normalizeELE = .not.normalizeELE
  CASE (IDC_CHECK_NSCF)
    normalizeSCF = .not.normalizeSCF
  CASE (IDC_CHECK_NSRF)
    normalizeSRF = .not.normalizeSRF
! -------------------------------------------
  CASE (IDC_BTN_SRF)
    estensione = '*.flt'
    tipofile = 'Snow Redist.Factor'
    call OpenOldFile (retint, estensione, tipofile, fileSRF)
    fileSRF = NoCString(fileSRF)
    retlog = DlgSet( dlg, IDC_Input_SRF, fileSRF)
! -------------------------------------------
  CASE (IDC_BTN_Prec1)
    estensione = '*.csv'
    tipofile = 'Precipitation File'
    call OpenOldFile (retint, estensione, tipofile, filePrecip)
    filePrecip = NoCString(filePrecip)
    call PrecCoordWriter ( dlg, control_name, callbacktype )
! -------------------------------------------
  CASE (IDC_BTN_Prec2)
    estensione = '*.csv'
    tipofile = 'Monthly Precip. File'
    call OpenOldFile (retint, estensione, tipofile, fileMonthPrec)
    fileMonthPrec = NoCString(fileMonthPrec)
    retlog = DlgSet( dlg, IDC_Prec_File2, fileMonthPrec )
    
END SELECT
end subroutine aggiorna_precip
! ************************************************************************
subroutine NablaByFile( dlg, control_name, callbacktype )
!DEC$ ATTRIBUTES DEFAULT :: NablaByFile
USE IFLOGM
use eis_mod
implicit none
INCLUDE 'RESOURCE.FD'
integer control_name
integer callbacktype
type (dialog) dlg
! -----------------------------------------------------------------
! ----------------------------------- lapse rate costante o da file
retlog = DlgSet( dlg, IDC_Btn_Pnabla1,         NP_cost, dlg_enable)
retlog = DlgSet( dlg, IDC_Btn_Pnabla2,  (.not.NP_cost), dlg_enable)
retlog = DlgSet( dlg, IDC_BTN_PNablaF,  (.not.NP_cost), dlg_enable)
retlog = DlgSet( dlg, IDC_input_PNablaF,(.not.NP_cost), dlg_enable)
retlog = DlgSet( dlg, IDC_STA_PNablaF1, (.not.NP_cost), dlg_enable)
retlog = DlgSet( dlg, IDC_STA_PNablaF2, (.not.NP_cost), dlg_enable)
retlog = DlgSet( dlg, IDC_STA_NablaPrec,       NP_cost, dlg_enable)
retlog = DlgSet( dlg, IDC_EDT_NablaPrec,       NP_cost, dlg_enable)
retlog = DlgSet( dlg, IDC_EDT_NablaPrec2,      NP_cost, dlg_enable)
!
end subroutine NablaByFile
! *******************************************************************
