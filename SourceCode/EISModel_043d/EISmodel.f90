program EISModel
! -------------- Energy Index Snow Model - with ice functions
! ---------------------------------------------------------------- ver. 036b ---- 
!
use dflib
use eis_mod
implicit none
! ----------------------------------------------------------
real           start_time, stop_time, tempo
integer        iore, minuti, isecond, icents
integer*2      get_status
character*8    orario
character*5    show
!STRUCTURE /QWINFO/
!        INTEGER*2 TYPE          ! REQUEST TYPE
!        INTEGER*2 X             ! X COORDINATE FOR UPPER LEFT
!        INTEGER*2 Y             ! Y COORDINATE FOR UPPER LEFT
!        INTEGER*2 H             ! WINDOW HEIGHT
!        INTEGER*2 W             ! WINDOW WIDTH
!END STRUCTURE
type (qwinfo)  winfo
! ------------------------------------------------------------------
call date_and_time (oggi,adesso) 
call getarg (1, fileComandi,  get_status)
call getarg (2, show)
showprogress = (get_status <= 0 .or. trim(show) /= '/mute')
! -------------------------------------------------------
if (showprogress) then
    open (io_win,file='user',title='EISModel - Energy Index Snow Model',carriagecontrol='fortran')
    i4 = setbkcolor(15)
    i4 = settextcolor(INT2(0)) 
    call clearscreen($GCLEARSCREEN)
endif
! -----------------------------------------------------------------------------------------
if (get_status > 0) then
    if (showprogress) then
        i4 = SETEXITQQ(QWIN$EXITPERSIST)
    else
        i4 = SETEXITQQ(QWIN$EXITNOPERSIST)
        winfo%TYPE = int2(1)
        i4 = SETWSIZEQQ(#80000000, winfo)
    endif
    call readinput
    call decodeinput
else
    i4 = SETEXITQQ(QWIN$EXITPERSIST)
    call CommandFile
    estensione = '*.csv'
    tipofile = 'File Comandi'
    call OpenNewFile (retint, estensione, tipofile, fileComandi)
    if (retint .eqv. .FALSE.) stop
    call decodeinput
    open (io_com,file=fileComandi)
    call writeinput (io_com)
    close (io_com)
endif
! -------------------------------------------------------
call cpu_time (start_time)
call time(orario)
if (showprogress) write (io_win,'(  " execution begins at ",a8)') orario
write (io_log,'(//"., execution begins at ,",a8)') orario
! .............................................................
if (fileTestPoints /= '.'.and. fileTestPoints /= 'none') then
   call readTPFile
endif
call simula_neve
call scrivi_output
close (104)
close (204)
close (304)
! ..............................................................
call time (orario)
call cpu_time (stop_time)
tempo = stop_time-start_time
call hhmmss(tempo, iore, minuti,isecond,icents)
write (io_log,'("., execution  ends  at ,",a8)') orario
write (io_log,'("., execution  time  is ,",i2,a1,i2.2,a1,i2.2,".",i2.2,a1)')  &
              iore,char(104),minuti,char(39),isecond,icents,char(34)
if (showprogress) then
    write (io_win,'(" execution  ends  at ",a8)') orario
    write (io_win,'(" execution  time  is ",i2,a1,i2.2,a1,i2.2,".",i2.2,a1)') &
                  iore,char(104),minuti,char(39),isecond,icents,char(34)
endif
if (showprogress) stop '-------------------------------------------------------- cz'
end
! *************************************************************
subroutine simula_neve
use eis_mod
implicit none
logical       itisday, leapyear
integer       julian_data, last_year_day
integer       n, j, iz, n1, it, ic, ipnt, imelt, late
integer       pxeast, pxnorth, julian, current_hour, tempInt, posizione
real          RadSolare, RadSolareOraria, Cloud_Index, SkyViewFactor
real          Temperature, tt, pp, rspp, eccesso, gela, albedo_layer, increment
real          altezza_sole, deltaq, lqwmax, MeltTot, rainTot, snowTot, fondi, snowfall, rainfall, mlt_type(0:5)
real          CloudIndex, WE_Zero, WEQ1_last, WEQ1_loss, WE_balance, En_Index, percento, Sunrise, Sunset
real          PMx, NabP_X, ppN(10)
real*8        Energy, MeltEIndex, MeltTIndex
character*256 TPFileName
character*8   aaaammdd,TempChar8, data8
! -------------------------------------------------------------------
if (showprogress) then
    write (io_win,'(" ... Wait for Maps Initialization ...........")')
    write (io_win,'("     0.00 % simulation done .................")')
endif
! -------------------------------------------------------------------
cell_cnt  = 0.
daynig    = 0.
barerain  = 0.
rainsnow  = 0.
dou       = 0.
rliquid   = 0.      
En_Index  = 0.
! ---------------------------- increment is to define the on-screen progression frequency
increment = max(10.,10.**int(log10(true_cell/1000.)))
!� ----------------------------- Considera un pixel alla volta
!dir$ loop count min(512)
do row = 1, no_rows
  do col = 1, no_columns  
    iz        = int(zon(col,row))
    MeltTot   = 0.0
    rainTot   = 0.             ! � --- Pioggia giornaliera cumulata
    snowTot   = 0.             ! � --- Neve giornaliera cumulata
    WEQ1_last = weq(col,row,1) !   --- WE giorno precedente
    WE_Zero   = weq(col,row,1) + weq(col,row,2) ! WE totale iniziale
    WELayer   = 0.
    DataLayer = 0
    T_hr_Acc  = 0.
! ---------------------------------------------------------------------------------
    if (maskGRD(col,row) /= esterno) then
 !� -------------------------------------- Check if pixel coincides with Test Point
        if (fileTestPoints /= '.' .and. fileTestPoints /= 'none') then
            do n1 = 1, NumPoints
                pxeast  = floor(((TPxys(n1,2) - or_easting)  / pixsize) + 1)
                pxnorth = floor(no_rows - ((TPxys(n1,3) - or_northing) / pixsize) + 1)
! ------------------------------ If pixel coincides with test point, create output file for that point
                if (pxeast == col .and. pxnorth == row)then
                    isTPPixel = .true.
                    write (TempChar8, '(i8.8)') int(TPxys(n1,1))
                    tempInt = scan(trim(fileLog),'\', back=.true.)
! --------------------------------------------------------- Write file header
                    TPFileName = fileLog(1:tempInt) // 'TP' // trim(TempChar8) // txtDataStep // '.csv'
                    open  (104, file = TPFileName)
                    write (104,'("col,",i10,",row,",i10,",E,",f10.1,",N,",f10.1,",ele,",f10.1,",SRF,",f10.5)') col, row, TPxys(n1,2), TPxys(n1,3), ele(col,row), SRF(col,row) 
                    write (104,'("horizon,",f7.1,23(",",f7.1))')    ((alzo(col,row,n)/radiante),n=0,23)
                    write (104,'(24(".,"),".")')
                    write (104,'("yr, mo, dy, hr,WE_Snow   ,WE_Ice    ,WE_Balance,Melt      ,SumMelt   ,SnowFall  ,SumSnowF  ,RainFall  ,SumRainF  ,LiquidWtr ,", &
                                                "Freeze    ,BottomWtr ,RadDiffuse,RadDirect ,EnIndex   ,CloudIndx%,ActiveLyer,WELayer   ,Temperat. ,SumTemper ,Albedo    ,LayerData ")')
! -------------------------------------------------------- File Katabatic Cooling
                    if (KatabaticCooling) then
                        TPFileName = fileLog(1:tempInt) // 'KT' // trim(TempChar8) // txtDataStep // '.csv'
                        open  (204, file = TPFileName)
                        write (204,'("col,",i10,",row,",i10,",E,",f10.1,",N,",f10.1,",ele,",f10.1,",SRF,",f10.5)') col, row, TPxys(n1,2), TPxys(n1,3), ele(col,row), SRF(col,row) 
                        write (204,'(23(".,"),".")')
                        write (204,'(23(".,"),".")')
                        write (204,'("yr, mo, dy, hr, Glacier, Temp, AmbientLapseRate, Elevation, ZUpperGlacierLine, ResponseLength, Xzero, BulkTransferCoefficient, DryAdiabaticLapseRate, ", &
                                     "RL_pixel, RL_FZT, RunLength, SlopeGlacier, FreezingLevel, LapseRateSlope, EquilibriumTemperature, Tzero, Zzero, theta, KataTemp, Delta")')
                    endif
! -------------------------------------------------------- File SnowFall
                    TPFileName = fileLog(1:tempInt) // 'SF' // trim(TempChar8) // txtDataStep // '.csv'
                    open  (304, file = TPFileName)
                    write (304,'("col,",i10,",row,",i10,",E,",f10.1,",N,",f10.1,",ele,",f10.1,",SRF,",f10.5)') col, row, TPxys(n1,2), TPxys(n1,3), ele(col,row), SRF(col,row) 
                    write (304,'(8(".,"),".")')
                    write (304,'(8(".,"),".")')
                    write (304,'("yr, mo, dy, hr, SnowFall  ,ActiveLyer,WE_Snow   ,WESubLyers ,Temperat. ")')
! -------------------------------------------------------- File melt
                    TPFileName = fileLog(1:tempInt) // 'ML' // trim(TempChar8) // txtDataStep // '.csv'
                    open  (404, file = TPFileName)
                    write (404,'("col,",i10,",row,",i10,",E,",f10.1,",N,",f10.1,",ele,",f10.1,",SRF,",f10.5)') col, row, TPxys(n1,2), TPxys(n1,3), ele(col,row), SRF(col,row) 
                    write (404,'(12(".,"),".")')
                    write (404,'(12(".,"),".")')
                    write (404,'("yr, mo, dy, hr, Temperat. ,SnowFall  ,RainFall  ,Melt      ,RT_mlt    ,NigntT_mlt,SnowT_mlt ,Rain_mlt  ,Refreezing")')
                endif
           end do   
        endif
! --------------------------------- initialise the snow stack with some layers (one per year to become ice) Snow_to_IceYears
        if (Snow_to_IceYears > 0) then
            do j = 1, Snow_to_IceYears
                WELayer(j) = weq(col,row,1) / Snow_to_IceYears
                DataLayer(j) = dataInitSim_int - (Snow_to_IceYears-j+1)*365 
                T_hr_Acc(j) =  10**((albedo_curve_intercept - snow_init_albedo) / albedo_curve_slope)  ! --- init somma termica
            enddo        
            ActiveLayer = Snow_to_IceYears 
        else
            WELayer(1) = weq(col,row,1) 
            DataLayer(1) = dataInitSim_int
            T_hr_Acc(1) = 10**((albedo_curve_intercept - snow_init_albedo) / albedo_curve_slope)  ! --- init somma termica
            ActiveLayer = 1
        endif
! -----------------------------------------------
        Albedo(2) = ice_albedo(col,row)      !   --- albedo ghiaccio
        if (snow_init_albedo >= fresh_snow_albedo) then
          Albedo(1) = fresh_snow_albedo      !   --- albedo iniziale
          T_hr_Acc(ActiveLayer) = 0. 
        else 
          Albedo(1) = snow_init_albedo      !   --- albedo iniziale
          T_hr_Acc(ActiveLayer) =  10**((albedo_curve_intercept - Albedo(1)) / albedo_curve_slope)  ! --- init somma termica
        endif
! ---------------------------------------------------------------------
        cell_cnt = cell_cnt + 1.
        percento = 100. * cell_cnt/true_cell
        if ((mod(cell_cnt,increment) == 0 .or. cell_cnt == true_cell).and.showprogress) write (io_win,'( ''+'',f8.2)') percento
!� ------------------------------  Deltaquota
        deltaq = ele(col,row)-elebase
!� ------------------------------  Pesa i dati meteo delle stazioni (IDW)
        call PrecWeight
        Energy = 0.
!        sumMinEI = 0.
!� ------------------------------  Calcola skyview factor
        SkyView = SkyViewFactor() 
!� ------------------------------ Ora considera un giorno alla volta <------- DAY DAY DAY DAY DAY DAY
! DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
        do dataCurrent_int = dataInitSim_int, dataEnd_Sim_int
            julian = julian_data(dataCurrent_int)            
!� ------------------------------  Giorno dell'anno
!� ---- Ha gi� calcolato i parametri di Swift, qui calcola ora inizio e fine giornata astronomici
            Sunrise = 12 + alba(julian) 
            Sunset  = 12 + tramonto(julian) 
! -------------------------------- azimuth alba e tramonto per calcolo Cloud Index     
            ora =     alba(julian)
            call    zntazm(julian)
            azm_alba = azm
            ora = tramonto(julian)
            call    zntazm(julian)
            azm_tram = azm
!� ------------------------------  correzione con funzione giornaliera di nuvolosit�
            CloudIndex = Cloud_Index()
! ----------------------------------------------------- cumula solo a partire dal solstizio (DataEI_jul)
!� ------------------------------  Se l'energia � calcolata su base giornaliera
            if (txtRadStep == 'd' .or. txtRadStep == 'D') then
!� ------------------------------  Energy � calcolato online
!� ------------------------------  If after solstice, cumulate and average
                if (CumulateEI) then
                    !if (dataCurrent_int == 32507) then
                    !    continue
                    !endif
                    if (julian < DataWinterSnow_jul) then
! ----------------------------------------------------------------------------------- cumulate EI 
! ------------------------------------------------------------------- vale se DataEI_jul > DataWinSnow
! ------------------------------------ emisfero boreale   DataWinSnow = 01.10 = 274 --- DataEI = 21.12  = 355
! ------------------------------------ emisfero australe  DataWinSnow = 01.04 =  91 --- DataEI = 21.06  = 172
                        dataEI_int = last_year_day(dataCurrent_int) - 365 + dataEI_jul
                        if (leapyear(last_year_day(dataCurrent_int))) dataEI_int = dataEI_int - 1
! ------------------------------  En_Index in W.m-2
                        Energy = Energy + RadSolare(julian)
                        En_Index = Energy /(dataCurrent_int-DataEI_int+1)
                    elseif (julian >= dataEI_jul) then
! ----------------------------------------------------------------------------------- cumulate EI 
                        dataEI_int = last_year_day(dataCurrent_int) + dataEI_jul 
                        Energy = Energy + RadSolare(julian)
                        En_Index = Energy /(dataCurrent_int-DataEI_int+1)
                    else
                        En_Index = RadSolare(julian)
                        Energy   = 0.
                    endif
                else
! ------------------------------  En_Index in W.m-2
                    En_Index = RadSolare(julian)
                endif
! -------------------------------  Cloud Correction Factor * Cloud Index
                En_Index = En_Index  * (1.0 - CCF * CloudIndex)
! -------------------------------- correzione climatica giornaliera
                if (ClimaticCorrection) En_Index    = En_Index * cli(col,row)   
            endif
! -----------------------------------------------------
!------------------------------ Ora considera un'ora alla volta <----- HOUR HOUR HOUR HOUR HOUR HOUR
! HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
            do current_hour = hrbeg, 24            !if daily data then hrbeg = 24 else hrbeg = 1
                fondi      = 0.
                snowfall   = 0.
                rainfall   = 0.
                gela       = 0.
                eccesso    = 0.
                mlt_type   = 0.
!� ---------------------------- it=ore da inizio simulazione
                it = (dataCurrent_int - dataInitSim_int) * 24 + current_hour
! ------------------------------------------------------- day or night ? (con orizzonte apparente)
! ------------------------------  if (current_hour > nint(Sunrise).and.current_hour <= nint(Sunset)) then
!� ---------------------------- Dalla prima ora dopo l'alba alla prima ora dopo il tramonto
                itisday = (current_hour > int(Sunrise).and.current_hour <= int(Sunset+.99))
                if (itisday .or. .not.hourly_data) then
                    if (hourly_data) then
                        ora = float(current_hour) - 0.5 * solpasso - 12.          ! ------ valore medio al centro intervallo
                    else
                        ora = 0.                                                  ! ------ midday
                    endif   
!�  ------------------------------   Se l'energia � calcolata su base oraria
                    if(txtRadStep == 'h' .or. txtRadStep == 'H') then
                        !En_Index = 0.
! -------------------------------  En_Index in W.m-2
                        En_Index = RadSolareOraria(julian,float(current_hour-12))  
! -------------------------------- Cloud Reduction factor
                        if (HourlyCloudy) then
                            En_Index = En_Index * Cloudy(it)
                        else
! -------------------------------  Cloud Correction Factor * Cloud Index
                            En_Index = En_Index * (1.0 - CCF * CloudIndex)
                        endif
! -------------------------------- correzione climatica
                        if (ClimaticCorrection) En_Index = En_Index * cli(col,row)   
! -----------------------------------------------------
                    endif
                    ipnt = floor(ora/solpasso)
                    altezza_sole = A090 - zenith(julian,ipnt)
                    cos_teta = 0.
! ------------- controlla la visibilit� del sole per pendenza ed esposizione
                    angolo = abs (azimuth(julian,ipnt) - esposizione(col,row))
                    if (angolo <= A090 .or. angolo >= A270 .or. pendenza(col,row) < altezza_sole) then
                        posizione = mod(nint(azimuth(julian,ipnt) / VELOCITA),24)
                        itisday = (altezza_sole > alzo(col,row,posizione))
                    endif 
                !elseif(txtRadStep == 'h' .or. txtRadStep == 'H') then
                !    En_Index = 0.                
                endif
                if (.not.hourly_data) itisday = .true.
                csr_zona (it,iz) = csr_zona (it,iz) + En_Index/real(zoncel(iz))
!                itisday = (current_hour > nint(Sunrise).and.current_hour <= nint(Sunset))
! -----------------------------------------------------------------
!� ------------------------------  Calcola temperatura (tt)
                tt = Temperature(it, deltaq, ele(col,row))
                if (KatabaticCooling) call Katabatic_Cooling (current_hour, it, tt)

!� ------------------------------  Compute precipitation pp 
                rspp = 0.
                pp = 0.0
                if (wide_P_gradient) then
! ------------------------------- IDW averaging (Inverse Distance Weighting)
                    PMx = 0.0
                    do j=1,no_Precip
                        PMx = PMx + PM_dat_in(it,j)*wgt(j)
                    enddo
! ------------------------------- IDW averaging at Reference elevation
                    do j=1,no_Precip
                        pp = pp + wgt(j)*Pr_dat_in(it,j)*( 1.0 + NabP_S(it,j)*1e-5*(PrecEleRef-Pzcoord(j)))
                    enddo
! ------------------------------- pixel gradient
                    NabP_X = 100.* NabP_Rmm(it)/PMx
! ------------------------------- transpose at pixel elevation
                    pp = pp *( 1.0 + NabP_X*1e-5*(ele(col,row) - PrecEleRef))
                else
! ------------------------------- IDW averaging
                    do j=1,no_Precip
                        pp = pp + Pr_dat_in(it,j)*wgt(j)
                    enddo
!� ------------------------------  Account for vertical gradient: from Pzcoord(1) to pixel elevation
                    pp = pp *( 1.0 + NabP_R(it) * 1e-5 *( ele(col,row) - PrecEleRef)) 
                endif
!� ------------------------------------------------------------------- 
! ######################### PIOGGIA E NEVE ####################
!� ------------------------------  Ora considera gli apporti da precipitazione
                if (pp > 0.) then
! ------------------------------------------in caso di precipitazione
!� ------------------------------  Soglia tt < pioggia-neve, weq aumenta
                    if (tt <= (PXTEM-PXrange)) then ! ------------------------------ 100% snow
                        RainFrac = 0.0
                    elseif (tt >= (PXTEM+PXrange)) then  ! --------------------------- 100% rain
                        RainFrac = 1.0
                    else         ! ----------------------------------------------------- 50% rain and 50% snow
! ------------------------------- compute snow and rain fractions based on temperature
                        RainFrac = (tt-(PXTEM-PXrange))/(2*PXrange)
                    endif
!� ------------------------------  La pp � gi� corretta per la tt del pluviometro (vedi sub NormalizePrec)
                    rainfall = pp*RainFrac
! -------------------------------  Account for wind and avalanche redistribution
                    snowfall = pp*(1.0-RainFrac)*SRF(col,row)
! -----------------------------------------------------------------------------------------
                    snowTot = snowTot + snowfall
! ---------------------------------------- � neve fresca e aumenta WE neve [WE(1)]
                    weq(col,row,1) = weq(col,row,1) + snowfall
                    rainTot = rainTot + rainfall
                    if(weq(col,row,1)+weq(col,row,2) > 0.)then
!� ------------------------------  Pioggia su neve se c'� manto � acqua liquida
                        rspp = rainfall
                        rainsnow = rainsnow + rainfall/true_cell
                    else
! ------------------------------ e se non c'� manto entra in baseflow dividendo la rainfall oraria per i cicli
                        do ic = (it-1)*bascicli+1, it*bascicli
                            baseflow (ic,iz) = baseflow(ic,iz) + rainfall / (real(zoncel(iz))*bascicli)
                        enddo
                        barerain  = barerain  + rainfall/true_cell
                    endif    ! -------------------------------- weq <> 0
                endif            ! -------------------------------- pp  >  0
! ######################### FUSIONE E RICONGELAMENTO ########################################
!� ------------------------------ Ora calcola processi nel ghiacciaio (fusione e ricongelamento)
! -------------------------------------------- fusione su neve (1) o su ghiaccio (2) ?           
                if(weq(col,row,1) > 0.) then
! -------------------------------------------- neve
                    wi = 1
                else
! -------------------------------------------- ghiaccio
                    wi = 2
                endif
                melt  = 0.
                imelt = 0
! ------------------------------------------------se c'� manto nevoso o ghiaccio
                if(weq(col,row,1)+weq(col,row,2) > 0.)then
!� ------------------------------ Se la temperatura � alta (>Tf)
                    if (tt > BASMELT) then
! ----------------------------------------------- NO precipitation --> RTmelt
                       if (rainfall <= 0.001 .and. snowfall <= 0.001) then
! --------------------------------------------------------se � giorno
                            if (itisday) then
! -------------------------------------------- fusione morfoenergetica
                                MeltEIndex  = 1.0D-3 * RMF(wi) * En_Index * (1.0-Albedo(wi))
                                MeltTIndex  = TMF(wi) * (tt - BASMELT)
! ------------------------------------------ algoritmo moltiplicativo (CZ,DF)
                                if (Algorithm == 1) then
                                   melt(wi) = MeltTIndex * MeltEIndex
! ------------------------------------------------ se EI � piccolo (alba, tramonto) melt non deve essere minore di quello termico.
                                   if ((NMF*(tt - BASMELT) > melt(wi)) .and. hourly_data) then
                                       melt(wi) =  NMF*(tt - BASMELT)  
                                       daynig(10) = daynig(10) + melt(wi)/true_cell
                                   else
                                       daynig(9) = daynig(9) + melt(wi)/true_cell
                                   endif
                                elseif (Algorithm == 2) then
! ------------------------------------------ algoritmo additivo (Pellicciotti) 
                                    melt(wi) = MeltTIndex + MeltEIndex
                                    daynig(9) = daynig(9) + MeltEIndex/true_cell
                                    daynig(10) = daynig(10) + MeltTIndex/true_cell
                                else
 ! ------------------------------------------ algoritmo moltiplicativo esteso (combinato) (Hock)
                                   melt(wi) = MeltTIndex + MeltEIndex * (tt - BASMELT)
                                   daynig(9) = daynig(9) + (tt - BASMELT)*MeltEIndex/true_cell
                                   daynig(10) = daynig(10) + MeltTIndex/true_cell
                                endif
                                imelt = 1
!� ------------------------------------------------------ Se � notte
                            else
! ------------------------------------------ fusione solo termica se � notte
                                if (NMFeqTMF .and. Algorithm > 1) NMF = TMF(wi)
                                MeltEIndex  = 0.
                                MeltTIndex  = NMF * (tt - BASMELT)
                                melt(wi) = MeltTIndex
                                imelt = 2
                            end if  
                            melt(wi) = amax1(melt(wi),0.)  ! potrebbe essere negativo con TMF < 0
!� ----------------------------------------- Se nevica o piove poco fusione solo termica
                        elseif ((snowfall > 0.0 .and. rainfall <= 0.001) .or. (rainfall > 0 .and. Rainfall < pp_low)) then
                            MeltEIndex  = 0.
                            MeltTIndex  = NMF * (tt - BASMELT)
                            melt(wi) = MeltTIndex
                            imelt = 3
!� ----------------------------------------- Se piove abbastanza (rainfall >= pp_low mm)
                        elseif (rainfall >= pp_low) then                            
! ------------------------------------------------- fusione da pioggia se non � nevicato
                            melt(wi) = ( RainMF + 0.0125 * rainfall) * (tt - BASMELT) 
                            imelt = 4
                        else
! --------------------------------- qualcosa non va
                            imelt = 5
                            pause ' ------------ something is wrong in Eismodel.f90 row 424 - press <CR>'
                        end if     ! -------------------------------- rainfall <> pp_low
! --------------------------------------------- verifica se � finita la neve fonde il ghiaccio
                        if (wi == 2) then  ! ----------------------------- ice
! --------------------------------------------- no snow - ice melts
                            melt(2) = min(weq(col,row,2),melt(2))
                            weq(col,row,2) = weq(col,row,2) - melt(2)
                        else               ! ----------------------------- snow
                            if (weq(col,row,1) >= melt(1)) then
! ------------------------------------------------------- snow > 0, it melts
                                weq(col,row,1) = weq(col,row,1) - melt(1)
                            else
! ------------------------------------------------------- few snow, it melts, then ice melts
                                melt(2) = melt(1) - weq(col,row,1) 
                                melt(1) = weq(col,row,1)
                                weq(col,row,1) = 0.0
                                melt(2) = min(weq(col,row,2),melt(2))
                                weq(col,row,2) = weq(col,row,2) - melt(2)
                                wi = 2
                            endif
                        endif    
! -------------------------------------------------------------------
!� ------------------------------ Calcolo fondi
                        fondi=sum(melt)  
!� ------------------------------ Dove va l'acqua liquida?
!� ------------------------------ Calcola mm di liquido max potenziali in neve (no ghiaccio)
                        lqwmax = (weq(col,row,1))*RLIQFR 
!� ------------------------------ Calcola mm di acqua liquida effettivamente presente
                        lqw(col,row) = lqw(col,row) + fondi + rspp
! -----------------------------------------la fusione � acqua liquida
!� ------------------------------ Se l'acqua liquida eccede la max acqua liquida potenziale
                        if (lqw(col,row) > lqwmax) then
! -------------------------------se eccede il massimo arriva al suolo
!� ------------------------------ Eccesso di lqw che arriva al suolo
                            eccesso = lqw(col,row) - lqwmax
!� ------------------------------ Lqw rimanente nel ghiaccio
                            lqw(col,row) = lqwmax
! -------------------------------- ritarda 'fondi' e assegnalo alla zona                
!� ------------------------------ Questo distingue tra neve e ghiaccio. Il ghiaccio non rallenta il flusso:
                            late = nint(ritardo * (weq(col,row,1)/1000.) * bascicli)  ! in cicli piccoli  
                            late = max(late,0)
                            ic = it * bascicli + late
                            baseflow (ic,iz) = baseflow(ic,iz) + eccesso/real(zoncel(iz))
                        endif
                        MeltTot  = MeltTot  + fondi
! ------------------------------------aggiorna la fusione media oraria
                        daynig(imelt) = daynig(imelt) + fondi/true_cell
! -------------------------------distribuisci il dato medio di fusione
!� ------------------------------ Se � giorno ???
                        if (itisday) then
                            daynig(6) = daynig(6) + fondi/true_cell
                        else
                            daynig(7) = daynig(7) + fondi/true_cell
                        endif
! ------------------------------ Se la temperatura � bassa (T<Tf)
                        !mlt_type(imelt) = melt(wi)
                        mlt_type(imelt) = melt(1) + melt(2)

                    else
! -----------------------------------------refreezing (tt � minore di BASMELT)
!� ------------------------------ Acqua potenzialmente ricongelata
                        gela = FREEZE * (BASMELT - tt)  !� Calcolato su ora = mm/(C ora) * C * ora
!� ------------------------------ Se c'� tanta acqua da ricongelare, gela solo la max potenziale
                        if (lqw(col,row) > gela) then
!� ------------------------------ Lwq di partenza � dato da input
                            lqw(col,row) = lqw(col,row) - gela
! --------------------------------ricongela acqua liquida (se ce n'�)
!� ------------------------------ Se c'� poca acqua da ricongelare, gela tutta
                        else
                            gela = lqw(col,row)
                            lqw(col,row) = 0.0
                        endif
!� ------------------------------ ???
                        daynig(8) = daynig(8) + gela/true_cell
!� ------------------------------ Se c'� neve, ricongela tutto in neve 
                        if(weq(col,row,1) > 0.)then
                            weq(col,row,1) = weq(col,row,1) + gela
!� ------------------------------ Se non c'� n� neve, l'acqua liquida va in baseflow (non ricongela)
                        else
                            late = 0.
                            ic = it * bascicli + late
                            baseflow (ic,iz) = baseflow(ic,iz) + gela/real(zoncel(iz))
                        endif
                        mlt_type(0) = gela
                    endif        ! -------------------------------- tt <> BASEMELT
                endif            ! -------------------------------- WE  >  0
! ########################## FINE ALGORITMO ###########################################
! ------------------------------aggiorna l'equivalente in acqua medio
! --------------------------------------------------------------------
                dou(it,1) = dou(it,1) + weq(col,row,1)/true_cell
                dou(it,2) = dou(it,2) + weq(col,row,2)/true_cell
                dou(it,3) = dou(it,3) + fondi/true_cell
                dou(it,4) = dou(it,4) + snowfall/true_cell 
                dou(it,5) = dou(it,5) + rainfall/true_cell
                dou(it,6) = dou(it,6) + lqw(col,row)/true_cell
                dou(it,7) = dou(it,7) + tt/true_cell
                dou(it,8) = dou(it,8) + T_hr_Acc(ActiveLayer)/true_cell
                dou(it,9) = dou(it,9) + Albedo(wi)/true_cell
! -------------------------------------------------------mappe output
!� ------------------------------ Crea mappe output 
                WE_balance = (weq(col,row,1)+weq(col,row,2)) - WE_Zero
                do n=1,no_Mappe
                    if (dataCurrent_int == DataMap_int(n).and.current_hour == 24) then
                        write (n+1100) weq(col,row,1)
                        write (n+1200) weq(col,row,2)
                        write (n+1300) MeltTot
                        write (n+1400) lqw(col,row)
                        write (n+1500) WE_balance
                        exit
                    endif
                enddo
! ------------------------------------------ calcola l'albedo e aggiorna i layer per il prossimo step
               if (weq(col,row,1) <= 0. ) then
! ------------------------------------------ non c'� neve --> azzera tutto lo stack e prepara l'albedo per la prossima neve fresca
                    WELayer     = 0.
                    T_hr_Acc    = 0.
                    DataLayer   = 0
                    ActiveLayer = 0
                    Albedo(1) = fresh_snow_albedo 
               else
! --------------------------------------- WE cala --> fusione
                   if (weq(col,row,1) < WEQ1_last ) then
! --------------------------------------- Somma temperature massime positive 
                        if (hourly_data) then
                            if (tt > 0.) T_hr_Acc(ActiveLayer) = T_hr_Acc(ActiveLayer) + tt 
                        else
                            if (tt > 0.) T_hr_Acc(ActiveLayer) = T_hr_Acc(ActiveLayer) + tt*24. 
                        endif
! --------------------------------------- verfica il layer attivo                        
                        WEQ1_loss = WEQ1_last - weq(col,row,1)
                        do j = ActiveLayer, 2, -1 
                            if (WELayer(j) > WEQ1_loss) then
                                WELayer(j)  = WELayer(j) - WEQ1_loss
                                WEQ1_loss   = 0.
                                ActiveLayer = j
                                exit
                            else
                                WEQ1_loss     = WEQ1_loss - WELayer(j)
                                WELayer(j)    = 0.
                                T_hr_Acc(j-1) = max(T_hr_Acc(j-1),T_hr_Acc(j))
                                T_hr_Acc(j)   = 0.
                                DataLayer(j)  = 0
                                ActiveLayer   = j-1
                            endif
                        enddo
                        if (ActiveLayer == 1) then
! ----------------------------------------------- single layer (firn ?)
                            WELayer(1) = weq(col,row,1)
                        endif
!  --------------------------------------  Calcola albedo con la TAcc 
                        if (T_hr_Acc(ActiveLayer) >= 0.1) then
                            Albedo(1) = min(fresh_snow_albedo, (albedo_curve_intercept - albedo_curve_slope * log10(T_hr_Acc(ActiveLayer))) )
                        else
                            Albedo(1) = fresh_snow_albedo   ! albedo neve fresca
                        endif
! --------------------------------------- WE aumenta --> neve fresca o ricongelamento
! --------------------------------------- verificare anche la neve fresca e il cumulo termico
! --------------------------------------- 0.5 mm --> 0.5 cm di neve buono per un ora
                   !elseif ( weq(col,row,1) > (WEQ1_last+0.5) .and. snowfall > 0.0 ) then
                   else
                       if(snowfall > 0.0 ) then
                            Albedo(1) = fresh_snow_albedo   ! albedo neve fresca
                            if (T_hr_Acc(ActiveLayer) > 0. .or. ActiveLayer == 0) then
                                ActiveLayer = ActiveLayer + 1
                                WELayer(ActiveLayer)   = weq(col,row,1) - WEQ1_last
                                T_hr_Acc(ActiveLayer)  = 0.
                                DataLayer(ActiveLayer) = dataCurrent_int
                                if(isTPPixel)then
                                    data8  = aaaammdd(dataCurrent_int)
                                    write (304,2000) data8(1:4), data8(5:6), data8(7:8), current_hour, snowfall, real(ActiveLayer), weq(col,row,1), &
                                                        WELayer(ActiveLayer), tt
                                endif
                            else
! --------------------------------------- increase WE same snowfall                        
                                WELayer(ActiveLayer)   = WELayer(ActiveLayer) + weq(col,row,1) - WEQ1_last
                            endif
                       else
! --------------------------------------- accounts for other we changes (i.e. refreezing)
                           WELayer(ActiveLayer)   = WELayer(ActiveLayer) + weq(col,row,1) - WEQ1_last
                       endif
                   endif
               endif
               WEQ1_last = weq(col,row,1)
! ------------------------------ An old snow layer (1) becomes ice
                if (Snow_to_IceYears > 0 .and. dataCurrent_int - DataLayer(1) >= (Snow_to_IceYears * 365)) then
                    weq(col,row,2) = weq(col,row,2) + WELayer(1)
                    weq(col,row,1) = weq(col,row,1) - WELayer(1)
                    WEQ1_last = WEQ1_last - WELayer(1)
                    do j = 2, ActiveLayer
                        WELayer(j-1)   = WELayer(j)
                        T_hr_Acc(j-1)  = T_hr_Acc(j)
                        DataLayer(j-1) = DataLayer(j) 
                    enddo
                    WELayer(ActiveLayer)   = 0.
                    T_hr_Acc(ActiveLayer)  = 0.
                    DataLayer(ActiveLayer) = 0
                    ActiveLayer = max(ActiveLayer-1,0)
                endif
!� ---------------------------------------- write test point file record
                if(isTPPixel)then
                    data8  = aaaammdd(dataCurrent_int)
                    albedo_layer = real(ActiveLayer)
                    UpperLayerData = real(DataLayer(ActiveLayer)-dataInitSim_int+1)
                    if (weq(col,row,1) == 0) then
                        albedo_layer = 0.
                        UpperLayerData = 0.
                    endif
                    write (104,2000) data8(1:4), data8(5:6), data8(7:8), current_hour, weq(col,row,1), weq(col,row,2), WE_balance, fondi, MeltTot,    & 
                             snowfall, snowTot, rainfall, rainTot, lqw(col,row), gela, eccesso, RsolDif, RsolDir, En_Index, CloudIndex*100., &
                             albedo_layer, WELayer(ActiveLayer), tt, T_hr_Acc(ActiveLayer), Albedo(wi)*100., UpperLayerData
                    write (404,2000) data8(1:4), data8(5:6), data8(7:8), current_hour, tt, snowfall, rainfall, fondi,       & 
                            (mlt_type(n), n=1,4), mlt_type(0)
                endif
! -------------------------------------------------------------------------------------
                if(txtRadStep == 'h' .or. txtRadStep == 'H') then
                    En_Index = 0.
                    RSolDir  = 0.
                    RSolDif  = 0.
                endif
            enddo         ! ------------------------------ Fine loop su ore    
! HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

        enddo             ! ------------------------------ Fine loop su giorni
! DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
        if (fileTestPoints /= '.' .and. fileTestPoints /= 'none') then
            close (102)
            close (103)
        endif
    ! -------------------------------------------- acqua liquida residua
        rliquid = rliquid + lqw(col,row)/true_cell
    else               ! ------------------------------ DEM = esterno
!� ------------------------------ Aggiorna mappe per esterni
        if (cell_cnt == true_cell .and. .not.completed) then   
            if (showprogress) write (io_win,'(" ... Completing Maps ........................")')
            completed = .true.
        endif
        do n=1,no_Mappe
            inquire (n+1100, OPENED = aperto)
            if (aperto) then
                do j = 1100, 1500, 100
                    write (n+j) esterno
                enddo
            endif
        enddo
    endif              ! ------------------------------ Cella DEM <> esterno
    isTPPixel = .false.
  enddo                ! ------------------------------ Fine loop su colonne
enddo                  ! ------------------------------ Fine loop su righe
! -------------------------------------------------------------------
if (showprogress) write (io_win,'(" ... Writing Output .........................")')
do n=1,no_Mappe
    do j = 1100, 1500, 100
        close (n+j)
    enddo
end do
2000 format(a4,2(",",a2),",",i3.2, 25(',',f10.2:))
return
end
