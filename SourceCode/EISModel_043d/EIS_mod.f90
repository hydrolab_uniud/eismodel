module eis_mod
! --------------------------------------------------
parameter     (MAX_PREC_FILE=100, MAX_TEMP_FILE=100, MAX_MAP_FILE=100)
parameter     (MAX_THR=5000)
parameter     (NO_DAYS_YR=366)
parameter     (PI=3.141592653589793238462)
parameter     (A090 = PI/2.0)
parameter     (A180 = PI)
parameter     (A270 = PI*3.0/2.0)
parameter     (A360 = PI*2.0)
parameter     (VELOCITA = PI/12.0)             ! RADIANTI/ora
parameter     (radiante = PI/180.0)
parameter     (NUM_PARMS=77)
! ------------------------------------------------------------
character*256 intestazione
character*256 fileLog, fileOutput
character*256 fileComandi, fileFormat, fileMask
character*256 fileMaps(MAX_MAP_FILE)
character*256 filePrecip, fileTempera, fileLapseRate, fileNablaPrec, fileMonthPrec
character*256 fileElevation, fileSlope, fileAspect, fileZone, fileSRF
character*256 fileHorizon, fileClimatic, fileIceLength, fileVFLN 
character*256 fileInput_lqw, fileTestPoints, fileIceAlbedo, fileCloudFraction
character*256 fileInput_we(2) 
character*256 text
character*25  tipofile
character*10  :: byteorder = 'LSBFIRST'
character*10  adesso, PName(NUM_PARMS)
character*8   dataControl_str(0:2), DataEI_str, DataMap_str(MAX_MAP_FILE)
character*8   oggi
character*5   estensione
character*4   MapHeader(5)
character*1   d1, d2, d3, d4, d5   ! dummies
character*1 ::  suffix, txtRadStep = 'h', txtDataStep = 'h'
! ----------------------------------------------------------------------------------------------------
integer       DataEI_jul, dataEI_int, DataMap_int(MAX_MAP_FILE), DataWinterSnow_int, DataWinterSnow_jul
integer       dataCurrent_int, dataInitDat_int, dataInitSim_int, dataEnd_Sim_int, no_skip_days, no_siml_days, no_siml_hours
integer       no_Zone, no_Mappe, no_Precip, no_EnInd, no_Tempera
integer       no_columns, no_rows, row, col
integer       peasti(MAX_PREC_FILE), pnorth(MAX_PREC_FILE)
integer   ::  bascicli = 1, solcicli = 1, Algorithm = 1, LR_algo = 1
integer   ::  hrbeg = 1, hrint = 1
integer       retint, index1
integer       i4, iresponse
integer       wi, NumPoints, ActiveLayer, Snow_to_IceYears
integer       io_win, io_child, io_log, io_com
integer       no_glaciers, current_glacier
integer       DataLayer(0:MAX_THR)
! ----------------------------------------------------------------------------------------------------
logical   ::  isTPPixel = .false., aperto = .true., completed = .false., wide_P_gradient = .false.
logical   ::  retlog, enabled, parmfit = .false., CumulateEI = .false., NMFeqTMF =.true.
logical   ::  normalizeELE = .true., normalizeSCF = .true., normalizeSRF = .true.
logical   ::  asp_int =.false., slo_int = .false.
logical   ::  LR_data(5) = (/.true.,.false.,.false.,.false.,.false./) ! Lapse rate -> 1 const./2 comp_boud/3 comp_free/4 inp_lin/5 inp_pwr 
logical   ::  NP_cost    = .true. ! Vertical Lapse rate for precipitation costant 
logical   ::  HourlyCloudy  = .false. ! Hourly correction for cloud cover by input 
logical   ::  hourly_data   = .true.  ! Hourly input data and computation interval 
logical   ::  ClimaticCorrection  = .false. ! Spatial correction for cloud persistence 
logical   ::  KatabaticCooling  = .false. ! Temperature correction for katabatic cooling 
logical   ::  showprogress = .false.
! ----------------------------------------------------------------------------------------------------
real          Diffusivita, Profondita_ottica, Costante_solare
real          elebase, tropopause, FREEZE, RainMF, TMF(2), RMF(2), NMF, PXTEM, PXrange, RainFrac, scf, BASMELT, RLIQFR, ritardo, UpperLayerData
real          Albedo(2), albedo_curve_intercept, albedo_curve_slope, fresh_snow_albedo, const_ice_albedo, snow_init_albedo
real          RSolDir, RsolDif, CCF
! ----------------------------------------------------------------------------
real          pixsize, esterno, undefined
real          true_cell, cell_cnt       
real          SkyView
real      ::  ora, alba(NO_DAYS_YR),tramonto(NO_DAYS_YR), solpasso = 1.0, TimeFuse = -1.0
real          wgt(MAX_PREC_FILE)
real          Pxcoord(MAX_PREC_FILE), Pycoord(MAX_PREC_FILE), Pzcoord(MAX_PREC_FILE)
real          Txcoord(MAX_TEMP_FILE), Tycoord(MAX_TEMP_FILE), Tzcoord(MAX_TEMP_FILE)
real          melt(2), NablaPrec, TemperatureLapseRate, PrecEleRef, NablaPMIN, NablaPMAX, pp_low
real          WELayer(0:MAX_THR), T_hr_Acc(0:MAX_THR)
real          BulkTransferCoefficient, DryAdiabaticLapseRate, AmbientLapseRate, LRMax, LRmin
real          ZUpperGlacierLine, LapseRateSlope, EquilibriumTemperature, Zzero, Tzero, theta
real          FreezingLevel, RunLength_pixel, RunLength_ZT, RunLength, SlopeGlacier
real          ResponseLength, Xzero, WindLayerThickness
! ----------------------------------------------------------------------------------------------------
real*8        or_easting, or_northing
real*8        rliquid, rainsnow, barerain, daynig(10)
real*8        latitudine, longitudine, declinazione(NO_DAYS_YR),ecc2(NO_DAYS_YR)
real*8        sinlat, coslat, sindec(NO_DAYS_YR), cosdec(NO_DAYS_YR)
real*8        exp_ene, cos_teta, angolo
real*8        znt, azm, azm_alba, azm_tram
! ----------------------------------------------------------------------------------------------------
integer,allocatable:: zoncel(:), ncicli(:)
integer,allocatable:: cntr(:)
! ----------------------------------------------------------------------------------------------------
real,allocatable :: ele(:,:), pendenza(:,:), esposizione(:,:), maskGRD(:,:)
real,allocatable :: zon(:,:), SRF(:,:), ice_albedo(:,:), cli(:,:)
real,allocatable :: weq(:,:,:),lqw(:,:)                                  !  ,wbl(:,:)
real,allocatable :: baseflow(:,:), csr_zona(:,:)
real,allocatable :: Te_dat_in(:,:),Pr_dat_in(:,:),Te_LR(:,:),PM_dat_in(:,:),dou(:,:)
real,allocatable :: NabP_R(:), NabP_Rmm(:), NabP_S(:,:)
real,allocatable :: alzo(:,:,:),Cloudy(:)
real,allocatable :: RunLen(:,:), D_vector(:,:,:)
real,allocatable :: ResLen(:), Xz(:), WindThick(:)
! ----------------------------------------------------------------------------------------------------
real*8, allocatable :: zenith(:,:), azimuth(:,:)
real*8, allocatable :: TPxys(:,:)
! ----------------------------------------------------------------------------------------------------
data          MapHeader /'_we1','_we2','_mlt','_lqw','_wbl'/
data          undefined / -8888.0 /, esterno / -9999.0 /

data          fileLog /'C:\Users\Public\Documents\Command_log.csv'/, fileOutput /'C:\Users\Public\Documents\Output.txt'/  
data          fileComandi, fileFormat, fileMask  /3*'.'/
data          fileMaps /MAX_MAP_FILE*'.'/
data          filePrecip, fileTempera, fileLapseRate, fileNablaPrec, fileMonthPrec  /5*'.'/ 
data          fileElevation, fileSlope, fileAspect, fileZone, fileSRF  /5*'.'/
data          fileHorizon, fileClimatic, fileIceLength, fileVFLN  /4*'.'/
data          fileInput_lqw, fileIceAlbedo, fileCloudFraction, fileTestPoints  /4*'.'/ 
data          fileInput_we   /2*'.'/
data          Pname /'HEADER    ','LOGOUTTF  ','OUTCFILE  ','HDRTIMPF  ','ELEGIMPF  ','MSKGINPF  ','WE1GINPF  ','WE2GINPF  ','LQWGINPF  ','ZONGINPF  ', &
                     'TPNCINPF  ','BGDDATE   ','BGSDATE   ','ENDDATE   ','SIMULSTEP ','PXTEM     ','PXRANGE   ','BASEMELT  ','LIQFRAC   ','RAINMF    ', &
                     'TMPMF1    ','TMPMF2    ','RADMF1    ','RADMF2    ','NIGHTMF   ','FREEZE    ','LOWPREC   ','ALGORITHM ','NMFEQTMF  ','BASFDELAY ', &
                     'BASFCYCLE ','EICUMUL   ','CUMULDATE ','LATITUDE  ','LONGITUDE ','TIMEFUSE  ','DIFFUSIV  ','OPTICALD  ','SOLARCOST ','SOLARCYCLE', &
                     'EICMPSTEP ','ALBINTCPT ','ALBSLOPE  ','ALBFSNOW  ','ALBINIT   ','SNOWICEYRS','ALBICEFIX ','ALBGINPF  ','CLOUDCF   ','CLOUDCIMPF', &
                     'SLOGINPF  ','ASPGINPF  ','CLIMAGINPF','HORIZONBIF','LRALGO    ','TROPOPAUSE','LAPSERATE ','LRTCINPF  ','TEMCINPF  ','KLENIMPF  ', &
                     'KVFLIMPF  ','KDRYADLR  ','KBULKTC   ','PRECINPF  ','PGRADTYPE ','PGRADIENT ','PREFELE   ','PGRDCINPF ','PRECMONTH ','SCF       ', &
                     'SRFGIMPF  ','NORMELE   ','NORMSCF   ','NORMSRF   ','NUMMAPS   ','OUTMAPF   ','          '/
data          DataEI_str /'20161221'/
data          dataControl_str /3*'19570928'/
data          tropopause /5000./, ritardo /0.0/, scf /1.0/, FREEZE /0.03/
data          RainMF /0.300/,TMF /1.500, -9999./, RMF /1.000, -9999./, NMF /0.200/, CCF /0.000/
data          albedo_curve_intercept /0.85/, albedo_curve_slope /0.10/, fresh_snow_albedo /0.900/, const_ice_albedo /0.200/, snow_init_albedo /0.650/
data          Profondita_ottica /0.200/, Diffusivita /0.4/ 
data          latitudine /0.8020543292d0/, longitudine /0.2179046538d0/ ! 45�57'15".58; 12�29'06".06
data          Costante_solare /1.357/        ! 1.357 kW/m�  ->   0.0081 kJ/cm� min 
data          PXTEM /1.5/, PXrange /1.0/, BASMELT /0.0/, RLIQFR /0.10/, pp_low /0.4/
data          NablaPrec /20.00/, TemperatureLapseRate /-0.0065/, PrecEleRef /0.0/, NablaPMIN /-9999.0/, NablaPMAX /9999.0/
data          LRMin /-9999.0/, LRMax /9999.0/
data          txtRadStep /'h'/, txtDataStep /'h'/
data          hrbeg /1/, hrint /1/
data          ActiveLayer /0/, Snow_to_IceYears /0/
data          io_win /0/, io_child /12/, io_log /11/, io_com /10/
data          BulkTransferCoefficient /0.002/, DryAdiabaticLapseRate /-0.0098/, AmbientLapseRate /-0.0080/
! --------------------------------------------------------------------------------------------------
end module eis_mod

