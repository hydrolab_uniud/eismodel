! **********************************************************************
real function SkyViewFactor ()
use eis_mod
implicit none
integer   ih
real      cos_pend, sin_pend, volta
real*8    SKVF
! --------------------------------------------------------------------
cos_pend = cos (pendenza(col,row))
sin_pend = sin (pendenza(col,row))
SkyViewFactor = 0.
do ih = 0,23
! ------------------------------------ volta non ostruita dall'orizzonte allo zenith
  volta = (PI/2.0) - alzo(col,row,ih)
! --------------------------------------------- Dubayah et al. (1990)
  SKVF = ( cos_pend * (sin(volta))**2.0 +                                    &
                  sin_pend*cos((ih*VELOCITA)-esposizione(col,row))*(volta - sin(volta)*cos(volta)) )
  if (SKVF > 0.0)  SkyViewFactor = SkyViewFactor + SKVF
enddo
SkyViewFactor = SkyViewFactor / 24.    
return
end
! *********************************************************************
subroutine swift
use eis_mod
implicit none
integer   julian, ipnt, ipnt_i, ipnt_f
real      albamin, trammax
real*8    cosT, yearfrac
! write (io_win,'( '' .... computing astronomical parameters...........'')')
sinlat = dsin(latitudine)
coslat = dcos(latitudine)
albamin =  9999.0
trammax = -9999.0
do julian = 1, NO_DAYS_YR
! ---------------------------------------------- algoritmi di Swift (1976)
  ecc2        (julian) =  (1.0 - 0.0167 * cos((julian-3)*0.0172))**2.0
  !declinazione(julian) = asin(0.39785*sin(4.868961 + 0.017203*julian + 0.033446 * sin(6.224111 + 0.017202*julian)))
  yearfrac = (A360/365.25)*(julian+12./24)   ! year fraction in rad (0-2PI) at midday UT time
  declinazione(julian) = 0.006918-0.399912*dcos(yearfrac)+0.070257*dsin(yearfrac)-0.006758*dcos(2*yearfrac)+0.000907*dsin(2*yearfrac)-0.002697*dcos(3*yearfrac)+0.00148*dsin(3*yearfrac)  
  cosT = -dtan(latitudine) * dtan(declinazione(julian))
  cosT = dsign(dmin1(dabs(cosT),1.0_8), cosT)
  tramonto(julian) = (dacos(cosT) / VELOCITA)
  alba    (julian) = -tramonto(julian)
  sindec  (julian) = dsin(declinazione(julian))
  cosdec  (julian) = dcos(declinazione(julian))
  albamin = amin1(albamin,alba    (julian))
  trammax = amax1(trammax,tramonto(julian))
enddo
ipnt_i = int(albamin/solpasso)-1 
ipnt_f = int(trammax/solpasso)+1
allocate (zenith(NO_DAYS_YR,ipnt_i-1:ipnt_f+1),SOURCE=0.0_8)
allocate (azimuth(NO_DAYS_YR,ipnt_i-1:ipnt_f+1),SOURCE=0.0_8)
do julian =1,NO_DAYS_YR
  do ipnt = ipnt_i-1, ipnt_f+1
    ora = ipnt * solpasso + 0.5 * solpasso             ! centrale rispetto allo step
    call zntazm (julian)
    zenith(julian,ipnt) = znt
    azimuth(julian,ipnt) = azm
  enddo
enddo
return
end
! *****************************************************************
subroutine zntazm (julian)
use eis_mod
implicit none
integer   julian
real*8 declinat, yearfrac, TimeCorr, SolarHourAngle, UTtime
real*8 coszenith, sinazimuth, cosazimuth, sinzenith2, coszenith2, sinazimuth2, cosazimuth2
! ------------------------------------------------------------------------------------------------
UTtime = (ora+12) + TimeFuse
yearfrac = (A360/365.25)*(julian+UTtime/24)   ! year fraction in rad (0-2PI) at UT time
declinat = 0.006918-0.399912*dcos(yearfrac)+0.070257*dsin(yearfrac)-0.006758*dcos(2*yearfrac)+0.000907*dsin(2*yearfrac)-0.002697*dcos(3*yearfrac)+0.00148*dsin(3*yearfrac)  
TimeCorr = 0.000075+0.001868*dcos(yearfrac)-0.032077*dsin(yearfrac)-0.014615*dcos(2*yearfrac)-0.040849*dsin(2*yearfrac)
SolarHourAngle = ((UTtime-12.)*VELOCITA + longitudine + TimeCorr)
coszenith = dsin(latitudine)*dsin(declinat)+dcos(latitudine)*dcos(declinat)*dcos(SolarHourAngle)
znt  = dacos(coszenith)
cosazimuth =(dsin(declinat)-dsin(latitudine)*dcos(znt))/(dcos(latitudine)*dsin(znt))
if (znt /=  0.) then
! ----------------------------- Lee (1980) p.46  ----- [asin(cos(z))] = 90-abs(z) ---------
  if (dcos(latitudine)/= 0. .and. dsin(znt) /= 0.) then
    cosazimuth =(dsin(declinat)-dsin(latitudine)*dcos(znt))/(dcos(latitudine)*dsin(znt))
  else
    cosazimuth =(dsin(declinat)-dsin(latitudine)*dcos(znt))/1.0e-9
  endif
  cosazimuth = dsign(dmin1(dabs(cosazimuth),1.0_8),cosazimuth)
  if (SolarHourAngle < 0) then
    azm  = dacos(cosazimuth)
  else
    azm  = A360-dacos(cosazimuth)
  endif 
!  sinazimuth = dsin(dacos(cosazimuth))
!! ------------------------ verifica della fase (crescente-decrescente) della funzione armonica
!  coszenith2  = dsin(latitudine)*dsin(declinat)+dcos(latitudine)*dcos(declinat)*dcos(SolarHourAngle+0.1)
!  sinzenith2  = dsin(dacos(coszenith2))
!  if (dcos(latitudine)/= 0. .and. sinzenith2 /= 0.) then
!    cosazimuth2 =(dsin(declinat)-dsin(latitudine)*coszenith2)/(dcos(latitudine)*sinzenith2)
!  else
!    cosazimuth2 =(dsin(declinat)-dsin(latitudine)*coszenith2)/1.0e-9
!  endif
!  cosazimuth2 = dsign(dmin1(dabs(cosazimuth2),1.0_8),cosazimuth2)
!  if (SolarHourAngle+0.1 < 0) then
!    sinazimuth2 = dsin(dacos(cosazimuth2))
!  else
!    sinazimuth2 = dsin(A360-dacos(cosazimuth2))
!  endif 
!! ---------------- la declinazione � positiva in giugno e negativa in dicembre
!! ---------------- la latitudine � negativa a sud dell'equatore
!! ---------------- per latitudini a sud  della declinazione (minori)   l'azimuth alle 12 � Nord
!! ---------------- per latitudini a nord della declinazione (maggiori) l'azimuth alle 12 � Sud
!! ---------------- la verifica va fatta in funzione della fase della funzione armonica
!  if (latitudine > declinazione(julian)) then
!    if (sinazimuth2 < sinazimuth) azm = A180 - azm
!  else
!    if (sinazimuth2 > sinazimuth) azm = A180 - azm
!  endif
else
  azm = 0.               ! --------- sole sulla verticale
  if (latitudine > declinazione(julian)) azm = A180
endif
if (azm < 0) azm = azm + A360
return
end
! **********************************************************************
real function RadSolare (julian)
use eis_mod
implicit none
integer   ipnt, julian, posizione, start_time, end_time
real      h, rdir, rdif, R_astro
real*8    altezza_sole, rdummy
! ----------------------------------------------------------------------
Rdir = 0.
Rdif = 0.
start_time = floor(alba    (julian)/solpasso) * solpasso          ! dall'alba 
end_time   =   int(tramonto(julian)/solpasso) * solpasso          ! fino al tramonto 

R_astro = (Costante_solare/ecc2(julian)) * solpasso * 3600.
do h = start_time, (end_time-1e-4), solpasso
  ora = h + 0.5 * solpasso                   ! valore medio al centro intervallo
  ipnt = floor(ora/solpasso)
  altezza_sole = A090 - zenith(julian,ipnt)
  if (altezza_sole < 0.) altezza_sole = 0.0
  cos_teta = 0.
! ------------- controlla la visibilit� del sole per pendenza ed esposizione
  angolo = abs (azimuth(julian,ipnt) - esposizione(col,row))
  if (angolo <= A090 .or. angolo >= A270 .or. pendenza(col,row) < altezza_sole) then
    posizione = nint(h+12)
    if (altezza_sole > alzo(col,row,posizione)) then
! ------------------------------------------------------ algoritmo di Oke (1987)
      cos_teta = cos(pendenza(col,row))*cos(zenith(julian,ipnt))+sin(pendenza(col,row))*sin(zenith(julian,ipnt))*cos(azimuth(julian,ipnt)-esposizione(col,row))
    endif 
  endif
! ------------------------------ Ranzi e Rosso (1991) [da Dubayah et al. (1990)]
  exp_ene = exp(-Profondita_ottica / sin(altezza_sole))
  Rdir = Rdir + R_astro * exp_ene * cos_teta
  rdummy = diffusivita * R_astro * sin(A090 - zenith(julian,ipnt)) * (1.0 - exp_ene) * SkyView
  if (rdummy > 0.)  Rdif = Rdif + rdummy
enddo

! Il (* 1000 / 86400) serve a passare da kJ.d-1.m-2 a W.m-2
RadSolare = (Rdir + Rdif) / 86.400 
return
end
! **********************************************************************
real function RadSolareOraria (julian, end_time)
use eis_mod
implicit none
integer   ipnt, julian, posizione
real      h, rdir, rdif, R_astro, start_time, end_time
real*8    altezza_sole
! ----------------------------------------------------------------------
Rdir = 0.
Rdif = 0.
RadSolareOraria = 0.
start_time = end_time - 1
R_astro = (Costante_solare/ecc2(julian)) * solpasso * 3600.
do h = start_time, (end_time-1e-4), solpasso
  !� Verify if h is between alba and tramonto
  if(h >= alba(julian) .and. h+solpasso <= tramonto(julian)) then
!�� --------------------- valore medio al centro intervallo                   
      ora = h + 0.5 * solpasso                   ! valore medio al centro intervallo
      ipnt = floor(ora/solpasso)
      altezza_sole = A090 - zenith(julian,ipnt)
      cos_teta = 0.
! ------------- controlla la visibilit� del sole per pendenza ed esposizione
      angolo = abs (azimuth(julian,ipnt) - esposizione(col,row))
      if (angolo <= A090 .or. angolo >= A270 .or. pendenza(col,row) < altezza_sole) then
         posizione = mod(nint(azimuth(julian,ipnt) / VELOCITA),24)
        if (altezza_sole > alzo(col,row,posizione)) then
! ------------------------------------------------------ algoritmo di Oke (1987)
          cos_teta = cos(pendenza(col,row))*cos(zenith(julian,ipnt))+sin(pendenza(col,row))*sin(zenith(julian,ipnt))*cos(azimuth(julian,ipnt)-esposizione(col,row))
        endif 
      endif
! ------------------------------ Ranzi e Rosso (1991) [da Dubayah et al. (1990)]
      exp_ene = exp(-Profondita_ottica / sin(altezza_sole))
      Rdir = Rdir + R_astro * exp_ene * cos_teta
      Rdif = Rdif + diffusivita * R_astro * sin(A090 - zenith(julian,ipnt)) * (1.0 - exp_ene) * SkyView
  endif
enddo
! ---------------- the coefficient [1000/3600] converts from <kJ.h-1.m-2> to <W.m-2>
RSolDir = Rdir / 3.6 
RSolDif = Rdif / 3.6
RadSolareOraria = RSolDir + RSolDif
return 
end
! *****************************************************************
real function Cloud_Index ()
use eis_mod
implicit none
real      xKp, azm0, azm1
! --------------------------------------
if (esposizione(col,row) < azm_alba) then
    xKp  = A180/(A360 - (azm_tram - azm_alba))
    azm0 = -(azm_tram - A360)
    azm1 = -A090
elseif (esposizione(col,row) < azm_tram) then 
    xKp = A180/(azm_tram - azm_alba)
    azm0 = -azm_alba
    azm1 = A090
else
    xKp = A180/(A360 - (azm_tram - azm_alba))
    azm0 = -azm_tram
    azm1 = -A090
endif
Cloud_Index = 0.5 - 0.5 * sin( ((esposizione(col,row)) + azm0) * xKp + azm1 )
return
end
! *****************************************************************