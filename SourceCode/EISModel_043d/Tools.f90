! *************************************************************
real function Temperature(it, deltaq, elevation)
use eis_mod
implicit none
real deltaq, elevation
integer it
! ----------------------------------------------------
if (LR_algo <= 4) then
    if (Te_LR(it,2) > 0. .and. elevation > tropopause) then
! ------------------------------------------------------------------------------ positive LR means thermal inversion up to the tropopause
        Temperature = Te_LR(it,1) + (tropopause-elebase) * Te_LR(it,2)   
        Temperature = Temperature + (elevation-tropopause) * TemperatureLapseRate   ! ---------- input fixed LR over the tropopause
    else
        Temperature = Te_LR(it,1) + deltaq * Te_LR(it,2)    ! -------------- correct by lapse rate
    endif
else   ! ---------------------------------------- by double linear regression
    if (Te_LR(it,3) > 0. .and. elevation > tropopause) then
! ------------------------------------------------------------------------------ positive LR means thermal inversion up to the tropopause
! ------------------------------------------------------------------------------ the tropopause is alwais above the reference elevation <elebase>    
        Temperature = Te_LR(it,1) + (tropopause-elebase) * Te_LR(it,3)        ! ---------- correct by lapse rate above the reference elevation
        Temperature = Temperature + (elevation-tropopause) * TemperatureLapseRate   ! ---------- input fixed LR over the tropopause
    else
        if (deltaq >= 0.0) then  
            Temperature = Te_LR(it,1) + deltaq * Te_LR(it,3)    ! -------------- correct by lapse rate above the reference elevation
        else
            Temperature = Te_LR(it,1) + deltaq * Te_LR(it,2)    ! -------------- correct by lapse rate below the reference elevation
        endif
    endif
endif    
return
end
! *************************************************************
subroutine NablaComp
use eis_mod
implicit none
integer      j, k, iora
real         intercept
real*8       ST, SE, SE2, STE
character*8  aaaammdd, data8
! --------------------------------------------------------------------------
!if (showprogress) write (io_win,'(" ... Temperature Lapse Rate .................")')
!  Te_LR(k,2)  ---> Lapse Rate
retint = scan (fileOutput,'.',back=.true.)
if (retint <= 1) retint = len_trim(fileOutput)+1
! --------------------------------------------------
if (LR_algo == 2) then   ! ------------------- LR_algo = 2   bounded regression
! -------------------------- linear regression forced to the reference station data (no intercept)
! -------------------------- the reference station is T-invariant
    SE2 = 0.
    do j=1,no_Tempera
        SE2 = SE2 + (Tzcoord(j)-Tzcoord(1))**2
    enddo
    do k = 1,no_siml_hours
        STE = 0.
        do j=2,no_Tempera
            STE = STE + (Tzcoord(j)-Tzcoord(1))*(Te_dat_in(k,j)-Te_dat_in(k,1))
        enddo
        Te_LR(k,2) = STE/SE2         ! ------------------- Lapse Rate �C/m
        Te_LR(k,1) = Te_dat_in(k,1)  ! ------------------- reference station temperature
    enddo
else  ! ------------------- LR_algo = 3   free regression
    do k = 1,no_siml_hours
        ST  = 0.
        SE  = 0.
        SE2 = 0.
        STE = 0.
        do j=1,no_Tempera
            ST  = ST  + Te_dat_in(k,j)
            SE  = SE  + Tzcoord(j)
            SE2 = SE2 + Tzcoord(j)*Tzcoord(j)
            STE = STE + Tzcoord(j)*Te_dat_in(k,j)
        enddo
        Te_LR(k,2) = (no_Tempera*STE - ST*SE) / (no_Tempera*SE2 - SE*SE)  !�C/m
        intercept  = (ST * SE2 - SE * STE) / (no_Tempera*SE2 - SE*SE) 
        Te_LR(k,1) = Tzcoord(1) * Te_LR(k,2) + intercept
    enddo
! ------------------------------------ writes the temperature at the reference elevation    
    open  (1,file=trim(fileOutput(1:retint-1))//'Temperature.csv')
    write (1,'("yr,  m, d, hr, Computed_temperature ")')
    write (1,'("1, ., ., ., .")')
    write (1,'("., ., ., ., .")')
    write (1,'("., ., ., .,",f10.1)') Tzcoord(1)
    do k = 1,no_siml_hours
        data8  = aaaammdd(dataInitSim_int + int((k-1)/24))
        iora    = mod ((k-1),24) + 1
        write (1,'(a4,",",a2,",",a2,",",i3.2,",",f10.2)') data8(1:4), data8(5:6), data8(7:8), iora, Te_LR(k,1)
    enddo
endif
! -------------------------------------- writes the Temperature Linear Lapse Rate
open  (1,file=trim(fileOutput(1:retint-1))//'LapseRate.csv')
write (1,'("yr,  m, d, hr, Computed_lapserate ")')
write (1,'("., ., ., ., .")')
write (1,'("., ., ., ., .")')
write (1,'("., ., ., ., .")')
do k = 1,no_siml_hours
    data8  = aaaammdd(dataInitSim_int + int((k-1)/24))
    iora    = mod ((k-1),24) + 1
    write (1,'(a4,",",a2,",",a2,",",i3.2,",",f10.2)') data8(1:4), data8(5:6), data8(7:8), iora, Te_LR(k,2)*1e3
enddo
return
end
! ****************************************************************************
subroutine slope_aspect
use eis_mod
implicit none
integer  ir, ic, icnt, kr, kc, index
real     slope, aspect, b, c, z(9)
! --------------------------------------------------------------------------
if (showprogress) write (io_win,'(" ... Slope and Aspect .......................")')
slope  = esterno
aspect = esterno
do ir = 1 , no_rows
  do ic = 1, no_columns
    if (ele(ic,ir) /= esterno) then
      icnt = 0
      do kr = -1,1
        do kc = -1,1
		  index = (kr+1) * 3 + (kc+2)
		    if (ic+kc > 0 .and. ic+kc <= no_columns .and. ir+kr > 0 .and. ir+kr <= no_rows ) then
		      if (ele(ic+kc,ir+kr) /= esterno) then
                z(index) = ele(ic+kc,ir+kr) - ele(ic,ir)
		        icnt = icnt + 1
              else
                z(index) = 0.
              endif
            else
              z(index) = 0.
            endif
        enddo
      enddo
	  icnt = icnt - 1  ! -------------------- tolto il pixel centrale
      b = (z(3)+2*z(6)+z(9)-z(1)-2*z(4)-z(7))/(icnt*pixsize)
      c = (z(1)+2*z(2)+z(3)-z(7)-2*z(8)-z(9))/(icnt*pixsize)
! ------------------------------------------------------------------
      slope = 100*(sqrt(b*b+c*c))
      if (abs(c) > epsilon(c)) then
        aspect = atand(b/c)
      else
	    aspect = 0.  
      endif
      if (c > 0) then
        aspect = aspect + 180
      elseif (c < 0 .and. b > 0) then
        aspect = aspect + 360
      endif
    endif
    if (slo_int) pendenza(ic,ir) = slope
	if (asp_int) esposizione(ic,ir) = aspect
  enddo
enddo
return
end
! *******************************************************
subroutine PrecWeight
! ---------------------------------------------------------------------
use eis_mod
implicit none
integer  k
real*8 distanza2, sigmad2
! --------------------------------------------------------------------------
!if (showprogress) write (io_win,'('' ... Computing Precipitation Weight ...................'')')
! ------------------------------------------------ calcola il denominatore
sigmad2 = 0.
do k = 1, no_Precip
  distanza2 = real(peasti(k)-col)**2 + real(pnorth(k)-row)**2
  if (distanza2 > 0.001) then
! ------------------------------------------------ se distanza2 < 0.001 pixel� la stazione coincide con la cella
    sigmad2 = sigmad2 + 1.0/distanza2
  else
    sigmad2 = -k    ! ---------------------------- just to be less than zero
    exit
  endif
enddo
! ------------------------------------------------ compute weight
do k = 1, no_Precip
  if (sigmad2 > 0.) then
    distanza2 = real(peasti(k)-col)**2 + real(pnorth(k)-row)**2
    if (distanza2 > 0.001) then
      wgt(k) = (1.0/distanza2) / sigmad2
    else
      wgt(k) = 1.0
      exit
    endif
  else
! ------------------------------------------------- se la stazione coincide il peso � 1.0
    if (sigmad2 == -k) then
      wgt(k) = 1.0
    else
      wgt(k) = 0.0
    endif
  endif
enddo
! ----------------------------------------------- verifica di congruenza
!wgt_sum = 0.0
!do k = 1, no_Precip
!  wgt_sum = wgt_sum + wgt(k)
!enddo
!if (wgt_sum < 0.999 .or. wgt_sum > 1.001 .and. showprogress) then
!  write (io_win,'('' column'',i6,''  row'',i6)') ic, ir
!  write (io_win,'('' Sum of weights'',e15.10)') wgt_sum
!  stop ' Error condition - Sum of weights not equal to one'
!endif
return
end
! ***********************************************************************
subroutine NormalizePrec
!� Correct precipitation values with SCF when the temperature of the pluviometer is lt snow/rain threshold

!� Normalize all the precipitation values to the reference elevation
!� K is about 20% every 1000 m
! if Elev2 > EleRef: P2 is greater than the normalized value (referred to EleRef) due to elevation effect
! so with reference to EleRef the normalized precipitation at Elev2 is decreased: P2n < P2

! if Elev2 < EleRef: P2 is smaller than the normalized value (referred to EleRef) due to elevation effect
! so with reference to EleRef the normalized precipitation at Elev2 is increased: P2n > P2
use eis_mod
implicit none
integer j, it
logical Pinside
real :: Temperature, tt, deltaq, RAINfraction, SNOWfraction
! --------------------------------------------------------------------------
if (normalizeSCF .or. normalizeSRF) then
    if (showprogress) write (io_win,'(" ... Normalize Precipitation ................"//)')
!dir$ loop count min(4)
    do j= 1, no_Precip
        deltaq = Pzcoord(j) - elebase   
        Pinside = (peasti(j) > 0 .and. peasti(j) <= no_columns .and. pnorth(j) > 0 .and. pnorth(j) <= no_rows) ! stazione dentro il bacino
        if (Pinside) Pinside = (Pinside .and. SRF(peasti(j),pnorth(j)) /= esterno .and. SRF(peasti(j),pnorth(j)) /= 0.0)   ! SRF diverso da esterno e da zero
! ---------------------------------------------------
        do it = 1, no_siml_hours
! ------------------------------------------------------------------------ compute temperature 
            tt = Temperature(it, deltaq, Pzcoord(j))
! -----------------------------------------------------------------------------------------------------------------------            
            if (tt <= (PXTEM-PXrange)) then ! ------------------------------ 100% snow
                if (normalizeSCF) then                    ! ------------------------------- normalize for SCF 
                    Pr_dat_in(it,j) = Pr_dat_in(it,j) * SCF      
                endif
                if (normalizeSRF .and. Pinside) then      ! ------------------------------- normalize for SRF
                    Pr_dat_in(it,j) = Pr_dat_in(it,j) / SRF(peasti(j),pnorth(j))
                endif
            elseif (tt >= (PXTEM+PXrange)) then  ! --------------------------- 100% rain
                continue                                  ! ------------------------------  do nothing
            else         ! ----------------------------------------------------- xx% rain and yy% snow
! ------------------------------- compute snow and rain fractions based on temperature
                RAINfraction = (tt-(PXTEM-PXrange))/(2*PXrange)
                SNOWfraction = 1.0 - RAINfraction
                if (normalizeSCF) then                    ! ------------------------------- normalize for SCFfraction 
                    Pr_dat_in(it,j) = Pr_dat_in(it,j) * (RAINfraction + SNOWfraction*SCF)     
                endif
                if (normalizeSRF .and. Pinside) then      ! ------------------------------- normalize for half SRF
                    Pr_dat_in(it,j) = Pr_dat_in(it,j) * (RAINfraction + SNOWfraction/SRF(peasti(j),pnorth(j)))
                endif
            endif
        enddo
    enddo
endif
! ------------------------------- normalize for elevation
if (wide_P_gradient) then
! ------------------------------- compute gradients
! ------------------------------- reference gradient in mm/km
    do it=1, no_siml_hours
        NabP_Rmm(it) = 0.01 * NabP_R(it) * PM_dat_in(it,1)  ! -------- The first station is the reference one 
    end do
! ------------------------------- station grtadients in %/km
    do j= 1, no_Precip
      do it=1, no_siml_hours
          if (PM_dat_in(it,j) > 0.001) NabP_S(it,j) = 100. * NabP_Rmm(it) / PM_dat_in(it,j)  
      end do
    end do
! ------------------------------- transpose the monthly precip. to Prec. Reference Elevation
    do j= 1, no_Precip
      do it=1, no_siml_hours
        PM_dat_in(it,j)=PM_dat_in(it,j)*(1.0+NabP_S(it,j)*1e-5*(PrecEleRef-Pzcoord(j)))
      end do
    end do
elseif (normalizeELE) then
    do j= 1, no_Precip
        do it=1, no_siml_hours
            Pr_dat_in(it,j)=Pr_dat_in(it,j)/(1.0+NabP_R(it)*1e-5*(Pzcoord(j)-PrecEleRef))
        end do
    end do
endif
return
end
! ***********************************************************************
subroutine Katabatic_Cooling (current_hour, it, tt)
! -------------------------------  Katabatic Cooling, Greuell&Bohm [1998]
use eis_mod
implicit none
real          tt, KataTemp, Dummy
character*8   aaaammdd, data8
integer       current_hour, it
! ----------------------------------------------------------------------------    
!    1 - FreezingLevel = (ele(col,row) * AmbientLapseRate - tt) / AmbientLapseRate
!--- se ele(col,row) < FreezingLevel 
!    2 - RunLength_pixel = RunLen(col,row) --> else RunLength_pixel = 0
!    3 - risali fino a FreezingLevel, trova RunLength_FreezingLevel
!    4 - RunLength = RunLength_pixel - RunLength_FreezingLevel
!    5 - SlopeGlacier% --> calcola la pendenza media del tratto FreezingLevel - ele(col,row)
!    6 - LapseRateSlope = 1e-3*DryAdiabaticLapseRate * (-SlopeGlacier%/100.)     ---> DryAdiabaticLapseRate is in �K/km
!    7 - EquilibriumTemperature = LapseRateSlope * ResponseLength
!    8 - se FreezingLevel > ZUpperGlacierLine --> Tzero = (ZUpperGlacierLine - ele(col,row)) * AmbientLapseRate + tt  --> else Tzero = 0
!    9 - se FreezingLevel > ZUpperGlacierLine --> Zzero =  ZUpperGlacierLine --> else Zzero = FreezingLevel   
!   10 - theta = (Tzero-EquilibriumTemperature)*exp(-(RunLength+Xzero)/ResponseLength)-LapseRateSlope*(RunLength+Xzero)+EquilibriumTemperature
!   11 - if (FreezingLevel <= ele(col,row) .or. RunLength == 0.) --> KataTemp = tt --> else  KataTemp = theta - 1e-3*DryAdiabaticLapseRate * (Zzero - ele(col,row))
! ----------------------------------------------------------------------------    
if (LR_algo <= 4) then
    AmbientLapseRate = Te_LR(it,2)
else
! ---------------------------------- double linear LR, use the upper zone LR
    AmbientLapseRate = Te_LR(it,3)
endif    
FreezingLevel = (ele(col,row) * AmbientLapseRate - tt) / AmbientLapseRate
if (FreezingLevel <= ele(col,row) .or. RunLen(col,row) == esterno) then 
    RunLength_pixel = 0.
    RunLength_ZT = 0.
    RunLength = 0.
    SlopeGlacier = 0.
    LapseRateSlope = 0.
    EquilibriumTemperature = 0.
    Tzero = 0.
    Zzero = 0.
    theta = 0.
    KataTemp = tt
    if (RunLen(col,row) == esterno) current_glacier = 0
else
    call find_RunLength_and_Slope 
    LapseRateSlope = DryAdiabaticLapseRate * (-SlopeGlacier/100.)
    EquilibriumTemperature = LapseRateSlope * ResponseLength
    if (FreezingLevel > ZUpperGlacierLine) then
        Tzero = (ZUpperGlacierLine - ele(col,row)) * AmbientLapseRate + tt
        Zzero =  ZUpperGlacierLine
    else
        Tzero = 0.0
        Zzero = FreezingLevel
    endif
    theta = (Tzero-EquilibriumTemperature)*exp(-(RunLength+Xzero)/ResponseLength)-LapseRateSlope*(RunLength+Xzero)+EquilibriumTemperature
    if (FreezingLevel <= ele(col,row) .or. RunLength == 0.) then
        KataTemp = tt
    else
        KataTemp = theta - DryAdiabaticLapseRate * (Zzero - ele(col,row))
    endif
endif
if (isTPPixel) then
    data8  = aaaammdd(dataCurrent_int)
    write (204,'(a4,2(",",a2),2(",",i3.2),20(",",f15.5))') data8(1:4), data8(5:6), data8(7:8), current_hour, current_glacier,tt, AmbientLapseRate*1e3, &
        ele(col,row), ZUpperGlacierLine, ResponseLength, Xzero, BulkTransferCoefficient, DryAdiabaticLapseRate*1e3, & 
        RunLength_pixel, RunLength_ZT, RunLength, SlopeGlacier, FreezingLevel, LapseRateSlope, EquilibriumTemperature, Tzero, Zzero, theta, KataTemp, (KataTemp-tt)
endif                
tt = KataTemp    
    
return
end
! ***********************************************************************
subroutine find_RunLength_and_Slope
! ------------------------------- 
use eis_mod
implicit none
integer j, g
! --------------------------- the RunLen grid is the integer RunLength + the glacier number/100 (decimals) -----------
RunLength_pixel = real(int(RunLen(col,row)))
g = nint(100.*(RunLen(col,row)-RunLength_pixel))
ZUpperGlacierLine  = D_vector(3,g,1)
ResponseLength     = ResLen(g)
Xzero              = Xz(g)
WindLayerThickness = WindThick(g)
current_glacier    = g
! ----------------------- find RL for Freezinglevel on the main flowline
do j=1, cntr(g)
    if (FreezingLevel >= D_vector(3,g,j)) exit
enddo
RunLength_ZT = D_vector(4,g,j)
! ----------------------- find RL_pixel on the main flowline
do j=1, cntr(g)
    if (RunLength_pixel <= D_vector(4,g,j)) exit
enddo
j = min(j,cntr(g)) 
RunLength = D_vector(4,g,j)-RunLength_ZT
if (RunLength <= 0.) then
    RunLength = 0.
    SlopeGlacier = 0.
else
! ------------------------------------- slope is computed along the main flow line
    SlopeGlacier = 100*(min(FreezingLevel,ZUpperGlacierLine)-ele(col,row))/RunLength
endif
RunLength = RunLength_pixel - RunLength_ZT
if (RunLength <= 0.) RunLength = 0.
return
end
! ***********************************************************************
character*256 function NoCString (stringa)
implicit none
integer  i
character*(*) stringa
i = SCAN (stringa, char(0))
if (i > 0)  then
  NoCString = adjustl(stringa(1:i-1))
else
  NoCString = adjustl(stringa)
endif
if (NoCString == ' ') NoCString = '.'
end function
! **************************************************************
subroutine hhmmss(time,ih,im,is,ic)
implicit none
integer  ic, is, im, ih
real     time
is = int(time)
ic = nint(time*100.-is*100.)
im = int (is/60.)
ih = int (im/60.)
is = is - im * 60
im = im - ih * 60
return
end
! *****************************************************************
integer function number_of_days (alfadata)
! --------------------------------  input: stringa 'AAAAMMGG'
! -------------------------------- output: numero di giorni dall' 1/1/1900 compreso
implicit none
integer       yr, mo, dy
character*8 alfadata
read (alfadata,'(i4,2i2)') yr, mo, dy
mo = mod((mo + 9),12)         ! mar=0, feb=11 
yr = yr - int(mo/10)       ! if Jan/Feb, year = year - 1
number_of_days =  yr*365 + int(yr/4) - int(yr/100) + int(yr/400) + int((mo*306 + 5)/10) + (dy - 1)
! -------------- 6930900 --> 31/12/1899 -  con il +1 il numero diventa uguale a quello usato da excel
number_of_days =  number_of_days - 693900 + 1
return
end
! *****************************************************************
subroutine y_m_d (days, yr, mo, dy)
! --------------------------------  input: numero di giorni dall' 1/1/1900 compreso
! -------------------------------- output: anno mese giorno
implicit none
integer*8 da
integer   days, yr, mo, dy
! -------------- 6930900 --> 31/12/1899 -  con il +1 il numero diventa uguale a quello usato da excel
da = days + 693900 - 1
yr = int((10000*da + 14780)/3652425)
dy = da - (yr*365 + int(yr/4) - int(yr/100) + int(yr/400) )
if (dy < 0) then
    yr = yr - 1
    dy = da - (yr*365 + int(yr/4) - int(yr/100) + int(yr/400) )
endif
mo = int((52 + 100*dy)/3060)
yr = yr + int((mo + 2)/12)
dy = dy - int((mo*306 + 5)/10) + 1
mo = mod((mo + 2),12) + 1
return
end
! *****************************************************************
integer function julian_data (day)
! -------------------------------- input: giorno dal 1/1/1900
! ------------------------------- output: data giuliana dal 1 GEN 
! ------------------------------- trova giorno, mese, anno - calcola i giorni dal 31/12/(anno-1)
implicit none
integer  day, yr, mo, dy, last_year_day
julian_data = day - last_year_day(day)
return
end
! *****************************************************************
integer function last_year_day (day)
! -------------------------------- input: giorno dal 1/1/1900
! -------------------------------- trova giorno, mese, anno - calcola i giorni al 31/12/(anno-1)
implicit none
integer  number_of_days
integer  day, yr, mo, dy
character*8 alfadata
call y_m_d (day, yr, mo, dy)
write (alfadata,'(i4,"1231")') yr-1
last_year_day = number_of_days(alfadata)
return
end
! *****************************************************************
character*8 function aaaammdd (day)
! -------------------------------- funzione di conversione per l'output dei file 
implicit none
integer  day, yr, mo, dy
call y_m_d (day, yr, mo, dy)
write (aaaammdd,'(i4,2i2.2)') yr, mo, dy
return
end
! *****************************************************************
logical function leapyear(day)
! -------------------------------- bissextile year T/F 
implicit none
integer  day, yr, mo, dy
call y_m_d (day, yr, mo, dy)
leapyear = (mod(yr,400)==0 .or. (mod(yr,100)/=0 .and. mod(yr,4)==0))
return
end
