! *****************************************************************
subroutine EnergyIndexFiles
USE DFLOGM
use eis_mod
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype, IDC
TYPE (dialog) dlg
external  aggiorna_energy
! ----------------------------------------------------------------
IF ( .not. DlgInit( IDD_EnergyIndex, dlg ) ) THEN
   WRITE (*,*) "Error: dialog not found"
ELSE
! Activate the modal dialog. 
!   retlog = DlgSet( dlg, IDC_CHECK_EIcum, CumulateEI)
   retlog = DlgSet( dlg, IDC_EDT_date3, DataEI_str)
   write (text,'(f14.5)') Latitudine*180./PI
   retlog = DlgSet( dlg, IDC_EDT_Latitude, text)
   write (text,'(f14.5)') Longitudine*180./PI
   retlog = DlgSet( dlg, IDC_EDT_Longitude, text)
   write (text,'(f14.5)') TimeFuse
   retlog = DlgSet( dlg, IDC_EDT_TimeFuse, text)
   write (text,'(f14.5)') diffusivita
   retlog = DlgSet( dlg, IDC_EDT_Diffuse, text)
   write (text,'(f14.5)') Profondita_ottica
   retlog = DlgSet( dlg, IDC_EDT_Deepness, text)
   write (text,'(f14.5)') Costante_solare
   retlog = DlgSet( dlg, IDC_EDT_SolarC, text)
   retlog = DlgSet( dlg, (IDC_RAD_solcomp1-1+solcicli), .TRUE.)
   if(txtRadStep.eq.'d')then
    retlog = DlgSet( dlg, IDC_RAD_RadStep1,.true.)
   elseif(txtRadStep.eq.'h')then
    retlog = DlgSet( dlg, IDC_RAD_RadStep2,.true.)
    CumulateEI = .false.
   endif
   retlog = DlgSet( dlg, IDC_CHECK_EIcum, CumulateEI)
   call enable_cumEI_cntrl( dlg, control_name, callbacktype )
   write (text,'(f14.5)') albedo_curve_intercept
   retlog = DlgSet( dlg, IDC_EDT_WE1, text)
   write (text,'(f14.5)') albedo_curve_slope
   retlog = DlgSet( dlg, IDC_EDT_WE2, text)
   write (text,'(f14.5)') fresh_snow_albedo
   retlog = DlgSet( dlg, IDC_EDT_WE3, text)  
   write (text,'(f14.5)') const_ice_albedo
   retlog = DlgSet( dlg, IDC_EDT_WE4, text)  
   write (text,'(f14.5)') snow_init_albedo
   retlog = DlgSet( dlg, IDC_EDT_WE5, text)  
   write (text,'(i14)') Snow_to_IceYears
   retlog = DlgSet( dlg, IDC_EDT_Snow_to_IceYears, text )

   
   retlog = DlgSet( dlg, IDC_CloudFraction_File, fileCloudFraction )
   retlog = DlgSet( dlg, IDC_Albedo_File, fileIceAlbedo )
   retlog = DlgSet( dlg, IDC_Slope_File, fileSlope )
   retlog = DlgSet( dlg, IDC_Aspect_File, fileAspect )
   retlog = DlgSet( dlg, IDC_Climatic_File, fileClimatic )
   retlog = DlgSet( dlg, IDC_Horizon_File, fileHorizon )
   write (text,'(f14.5)') CCF
   retlog = DlgSet( dlg, IDC_EDT_CCF, text)

! ----------------------------------------------------------
   retlog = DlgSetSub( dlg, IDC_CHECK_EIcum,  aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_EDT_date3,    aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_EDT_Latitude, aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_EDT_Longitude, aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_EDT_TimeFuse, aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_EDT_Diffuse,  aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_EDT_Deepness, aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_EDT_SolarC,   aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_RAD_solcomp1, aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_RAD_solcomp2, aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_RAD_solcomp4, aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_RAD_solcomp6, aggiorna_energy )
   
   retlog = DlgSetSub (dlg, IDC_Rad_RadStep1, aggiorna_energy)
   retlog = DlgSetSub (dlg, IDC_Rad_RadStep2, aggiorna_energy)
 
   retlog = DlgSetSub( dlg, IDC_EDT_WE1, aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_EDT_WE2, aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_EDT_WE3, aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_EDT_WE4, aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_EDT_WE5, aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_EDT_Snow_to_IceYears, aggiorna_energy )
   
   retlog = DlgSetSub( dlg, IDC_BTN_CloudFraction, aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_BTN_Albedo, aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_BTN_Slope, aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_BTN_Aspect, aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_BTN_Climatic, aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_BTN_Horizon, aggiorna_energy )
   retlog = DlgSetSub( dlg, IDC_EDT_CCF, aggiorna_energy )
   
! ...............................................................
retint = DlgModal( dlg ) 
CALL DlgUninit( dlg )
END IF
return
end
! *******************************************************************
subroutine aggiorna_energy ( dlg, control_name, callbacktype )
!DEC$ ATTRIBUTES DEFAULT :: aggiorna_energy
USE DFLOGM
use eis_mod
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype
character*256 NoCString
TYPE (dialog) dlg
! -------------------------------------------------------------
SELECT CASE (control_name)

  CASE (IDC_CHECK_EIcum)
    CumulateEI = .not.CumulateEI
  CASE (IDC_EDT_date3)
    retlog = DlgGet( dlg, IDC_EDT_date3, DataEI_str )
    DataEI_str = NoCString(DataEI_str) 
! .......................................................
  CASE (IDC_EDT_Latitude)
    retlog = DlgGet( dlg, IDC_EDT_Latitude, text )
    read (text, *, iostat=retint) latitudine
    latitudine = latitudine * PI / 180.0                  ! radianti
  CASE (IDC_EDT_Longitude)
    retlog = DlgGet( dlg, IDC_EDT_Longitude, text )
    read (text, *, iostat=retint) longitudine
    longitudine = longitudine * PI / 180.0                  ! radianti
  CASE (IDC_EDT_TimeFuse)
    retlog = DlgGet( dlg, IDC_EDT_TimeFuse, text )
    read (text, *, iostat=retint) TimeFuse
  CASE (IDC_EDT_Diffuse)
    retlog = DlgGet( dlg, IDC_EDT_Diffuse, text )
    read (text, *, iostat=retint) Diffusivita
  CASE (IDC_EDT_Deepness)
    retlog = DlgGet( dlg, IDC_EDT_Deepness, text )
    read (text, *, iostat=retint) Profondita_ottica
  CASE (IDC_EDT_SolarC)
    retlog = DlgGet( dlg, IDC_EDT_SolarC, text )
    read (text, *, iostat=retint) Costante_solare
  CASE (IDC_RAD_solcomp1)
       solcicli = 1
  CASE (IDC_RAD_solcomp2)
       solcicli = 2
  CASE (IDC_RAD_solcomp4)
       solcicli = 4
  CASE (IDC_RAD_solcomp6)
       solcicli = 6
  
  CASE (IDC_RAD_RadStep1)
       txtRadStep = 'd'
       call enable_cumEI_cntrl( dlg, control_name, callbacktype )
  CASE (IDC_RAD_RadStep2)
       txtRadStep = 'h' 
       CumulateEI = .false.
       call enable_cumEI_cntrl( dlg, control_name, callbacktype )
 !� ..............................................   
  CASE (IDC_EDT_WE1)
    retlog = DlgGet( dlg, IDC_EDT_WE1, text )
    read (text, *, iostat=retint) albedo_curve_intercept
  CASE (IDC_EDT_WE2)
    retlog = DlgGet( dlg, IDC_EDT_WE2, text )
    read (text, *, iostat=retint) albedo_curve_slope
  CASE (IDC_EDT_WE3)
    retlog = DlgGet( dlg, IDC_EDT_WE3, text )
    read (text, *, iostat=retint) fresh_snow_albedo
  CASE (IDC_EDT_WE4)
    retlog = DlgGet( dlg, IDC_EDT_WE4, text )
    read (text, *, iostat=retint) const_ice_albedo
  CASE (IDC_EDT_WE5)
    retlog = DlgGet( dlg, IDC_EDT_WE5, text )
    read (text, *, iostat=retint) snow_init_albedo
  CASE (IDC_EDT_Snow_to_IceYears)
    retlog = DlgGet( dlg, IDC_EDT_Snow_to_IceYears, text )
    read (text, *, iostat=retint) Snow_to_IceYears
    if (Snow_to_IceYears < 0) Snow_to_IceYears = 0
! -------------------------------------------
  CASE (IDC_BTN_CloudFraction)
    estensione = '*.csv'
    tipofile = 'Cloud Cover Reduction hourly file'
    call OpenOldFile (retint, estensione, tipofile, fileCloudFraction)
    fileCloudFraction = NoCString(fileCloudFraction)
    retlog = DlgSet( dlg, IDC_CloudFraction_File, fileCloudFraction )
! -------------------------------------------
  CASE (IDC_BTN_Albedo)
    estensione = '*.flt'
    tipofile = 'Ice ALbedo Grid File'
    call OpenOldFile (retint, estensione, tipofile, fileIceAlbedo)
    fileIceAlbedo = NoCString(fileIceAlbedo)
    retlog = DlgSet( dlg, IDC_Albedo_File, fileIceAlbedo )
! -------------------------------------------
  CASE (IDC_BTN_Slope)
    estensione = '*.flt'
    tipofile = 'Slope Grid File'
    call OpenOldFile (retint, estensione, tipofile, fileSlope)
    fileSlope = NoCString(fileSlope)
    retlog = DlgSet( dlg, IDC_Slope_File, fileSlope )
! -------------------------------------------
  CASE (IDC_BTN_Aspect)
    estensione = '*.flt'
    tipofile = 'Aspect Grid File'
    call OpenOldFile (retint, estensione, tipofile, fileAspect)
    fileAspect = NoCString(fileAspect)
    retlog = DlgSet( dlg, IDC_Aspect_File, fileAspect )
! -------------------------------------------
  CASE (IDC_BTN_Climatic)
    estensione = '*.flt'
    tipofile = 'Climatic Correction Grid File'
    call OpenOldFile (retint, estensione, tipofile, fileClimatic)
    fileClimatic = NoCString(fileClimatic)
    retlog = DlgSet( dlg, IDC_Climatic_File, fileClimatic )
! -------------------------------------------
  CASE (IDC_BTN_Horizon)
    estensione = '*.hrz'
    tipofile = 'Horizon Angle File'
    call OpenOldFile (retint, estensione, tipofile, fileHorizon)
    fileHorizon = NoCString(fileHorizon)
    retlog = DlgSet( dlg, IDC_Horizon_File, fileHorizon )
! -------------------------------------------
  CASE (IDC_EDT_CCF)
    retlog = DlgGet( dlg, IDC_EDT_CCF, text )
    read (text, *, iostat=retint) CCF
      
! -------------------------------------------
END SELECT

end subroutine aggiorna_energy
! ***************************************************************************
subroutine enable_cumEI_cntrl( dlg, control_name, callbacktype )
use dflogm
use eis_mod
implicit none
INCLUDE 'RESOURCE.FD'
integer control_name, callbacktype
type (dialog) dlg
! -----------------------------------------------------------------
enabled = (txtRadStep.eq.'d')
retlog = DlgSet( dlg, IDC_CHECK_EIcum, CumulateEI)
retlog = DlgSet( dlg, IDC_CHECK_EIcum, enabled, dlg_enable  )
retlog = DlgSet( dlg, IDC_EDT_date3,   enabled, dlg_enable  )
retlog = DlgSet( dlg, IDC_STA_date3,   enabled, dlg_enable  )
return
end
