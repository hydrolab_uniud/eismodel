subroutine CommandFile
USE DFLOGM
use eis_mod
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype
TYPE (dialog) dlg
external aggiorna_campi, leggi_da_file, ferma_tutto, salva_comandi, messagebox
! ----------------------------------------------------------------
IF ( .not. DlgInit( idd_command, dlg ) ) THEN
   WRITE (*,*) "Error: dialog not found"
ELSE
! Activate the modal dialog. 

! ======================================= INIZIALIZZA I CAMPI
! ...............................................................
   retlog = DlgSet( dlg, IDC_SimHeader, intestazione )
   retlog = DlgSet( dlg, IDC_Log_File, fileLog )
   retlog = DlgSet( dlg, IDC_Output_File, fileOutput )
   retlog = DlgSet( dlg, IDC_Format_File, fileFormat )
   retlog = DlgSet( dlg, IDC_Elevation_File, fileElevation )

   retlog = DlgSet( dlg, IDC_Input_Mask, fileMask )
   retlog = DlgSet( dlg, IDC_Input_File, fileInput_we(1) )
   retlog = DlgSet( dlg, IDC_Input_File2, fileInput_we(2) )
   retlog = DlgSet( dlg, IDC_Input_File4, fileInput_lqw )
   retlog = DlgSet( dlg, IDC_Zone_File, fileZone )
   retlog = DlgSet( dlg, IDC_ShapeFile, fileTestPoints )
   
   write (text,'(i2)') no_Tempera
   retlog = DlgSet( dlg, IDC_EDT_noTemp, text)
   write (text,'(i2)') no_Precip
   retlog = DlgSet( dlg, IDC_EDT_noPrec, text)
   write (text,'(i2)') no_Mappe
   text = 'OnLine'
   retlog = DlgSet( dlg, IDC_EDT_noEneInd, text)

   retlog = DlgSet( dlg, IDC_CHECK_Parameters, parmfit)

! --------------------------------------------------------------- buttons
   retlog = DlgSetSub( dlg, IDLoad, leggi_da_file )
   retlog = DlgSetSub( dlg, ID_Cancella, ferma_tutto)
   retlog = DlgSetSub( dlg, IDSalva, salva_comandi)
   retlog = DlgSetSub( dlg, IDInformations, messagebox)
! ======================================= AVVIA IL CONTROLLO
   retlog = DlgSetSub( dlg, IDC_SimHeader, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_BTN_LOG, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_BTN_OutputF, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_BTN_Format, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_BTN_Elevation, aggiorna_campi )

   retlog = DlgSetSub( dlg, IDC_BTN_Mask, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_BTN_InputF, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_BTN_InputF2, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_BTN_InputF4, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_BTN_Zone, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_BTN_Shapefile, aggiorna_campi )
! ............................................................... forms
   retlog = DlgSetSub( dlg, IDC_BTN_PrecFiles, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_BTN_TempFiles, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_BTN_PrecCntrl, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_BTN_TempCntrl, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_BTN_Parameters, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_BTN_EneInd, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_BTN_OutMaps, aggiorna_campi )   
! Release dialog resources.
   retint = DlgModal( dlg ) 
   CALL DlgUninit( dlg )
END IF
return
end
! *******************************************************************
subroutine aggiorna_campi ( dlg, control_name, callbacktype )
!DEC$ ATTRIBUTES DEFAULT :: aggiorna_campi
USE DFLOGM
use eis_mod
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype
character*256 NoCString
TYPE (dialog) dlg
! -------------------------------------------------------------
SELECT CASE (control_name)
! -------------------------------------------
  CASE (IDC_SimHeader)
    retlog = DlgGet( dlg, IDC_SimHeader, intestazione )
    intestazione = NoCString(intestazione)
! -------------------------------------------
  CASE (IDC_BTN_LOG)
    estensione = '*.csv'
    tipofile = 'Simulation Log File'
    call OpenNewFile (retint, estensione, tipofile, fileLOG)
    fileLOG = NoCString(fileLOG)
    retlog = DlgSet( dlg, IDC_LOG_File, fileLOG )
! -------------------------------------------
  CASE (IDC_BTN_OutputF)
    estensione = '*.txt'
    tipofile = 'Simulation Output File'
    call OpenNewFile (retint, estensione, tipofile, fileOutput)
    fileOutput = NoCString(fileOutput)
    retlog = DlgSet( dlg, IDC_Output_File, fileOutput )
! -------------------------------------------
  CASE (IDC_BTN_Format)
    estensione = '*.hdr'
    tipofile = 'Header File'
    call OpenOldFile (retint, estensione, tipofile, fileFormat)
    if (retint .neqv. .false.) then
      fileFormat = NoCString(fileFormat)
      retlog = DlgSet( dlg, IDC_Format_File, fileFormat )
      open (1,file=fileFormat,action='read',err=10)
      call hdrread (1)
      close (1)
10    continue
    endif
! -------------------------------------------
  CASE (IDC_BTN_Elevation)
    estensione = '*.flt'
    tipofile = 'Elevation Grid File'
    call OpenOldFile (retint, estensione, tipofile, fileElevation)
    fileElevation = NoCString(fileElevation)
    retlog = DlgSet( dlg, IDC_Elevation_File, fileElevation )
! -------------------------------------------
  CASE (IDC_BTN_Mask)
    estensione = '*.flt'
    tipofile = 'Simulation Area Mask File'
    call OpenOldFile (retint, estensione, tipofile, fileMask)
    fileMask = NoCString(fileMask)
    retlog = DlgSet( dlg, IDC_Input_Mask, fileMask)
! -------------------------------------------
  CASE (IDC_BTN_InputF)
    estensione = '*.flt'
    tipofile = 'WE1 Input Grid File'
    call OpenOldFile (retint, estensione, tipofile, fileInput_we(1))
    fileInput_we(1) = NoCString(fileInput_we(1))
    retlog = DlgSet( dlg, IDC_Input_File, fileInput_we(1))
! -------------------------------------------
  CASE (IDC_BTN_InputF2)
    estensione = '*.flt'
    tipofile = 'WE2 Input Grid File'
    call OpenOldFile (retint, estensione, tipofile, fileInput_we(2))
    fileInput_we(2) = NoCString(fileInput_we(2))
    retlog = DlgSet( dlg, IDC_Input_File2, fileInput_we(2))
! -------------------------------------------
  CASE (IDC_BTN_InputF4)
    estensione = '*.flt'
    tipofile = 'LQW Input Grid File'
    call OpenOldFile (retint, estensione, tipofile, fileInput_lqw)
    fileInput_lqw = NoCString(fileInput_lqw)
    retlog = DlgSet( dlg, IDC_Input_File4, fileInput_lqw )
! -------------------------------------------
  CASE (IDC_BTN_Zone)
    estensione = '*.flt'
    tipofile = 'Contrib. Areas Grid File'
    call OpenOldFile (retint, estensione, tipofile, fileZone)
    fileZone = NoCString(fileZone)
    retlog = DlgSet( dlg, IDC_Zone_File, fileZone )
! -------------------------------------------
  CASE (IDC_BTN_ShapeFile)
    estensione = '*.csv'
    tipofile = 'Test Points CSV file'
    call OpenOldFile (retint, estensione, tipofile, fileTestPoints)
    fileTestPoints = NoCString(fileTestPoints)
    retlog = DlgSet( dlg, IDC_Shapefile, fileTestPoints )
! -------------------------------------------
  CASE (IDC_BTN_PrecFiles)
    call PrecipitationFiles
    write (text,'(i2)', iostat=retint) no_Precip
    retlog = DlgSet( dlg, IDC_EDT_noPrec, text)
! -------------------------------------------
  CASE (IDC_BTN_TempFiles)
    call TemperatureFiles
    write (text,'(i2)', iostat=retint) no_Tempera
    retlog = DlgSet( dlg, IDC_EDT_noTemp, text)
! -------------------------------------------
  CASE (IDC_BTN_PrecCntrl)
    call PrecipitationControl
! -------------------------------------------
  CASE (IDC_BTN_TempCntrl)
    call TemperatureControl
! -------------------------------------------
  CASE (IDC_BTN_Parameters)
    call Parameters
    retlog = DlgSet( dlg, IDC_CHECK_Parameters, parmfit)
! -------------------------------------------
  CASE (IDC_BTN_EneInd)
    call EnergyIndexFiles
    write (text,'(i2)', iostat=retint) no_EnInd
	  text = 'OnLine'
	retlog = DlgSet( dlg, IDC_EDT_noEneInd, text)
! -------------------------------------------
  CASE (IDC_BTN_OutMaps)
    call OutMapsFiles
    write (text,'(i2)', iostat=retint) no_Mappe
    retlog = DlgSet( dlg, IDC_EDT_noMaps, text)
! -------------------------------------------
END SELECT
end subroutine aggiorna_campi
! *****************************************************************
subroutine messagebox ( dlg, control_name, callbacktype )
USE DFLOGM
USE DFLIB
implicit none
INTEGER control_name, callbacktype
TYPE (dialog) dlg
INTEGER retint
retint = MESSAGEBOXQQ ('Energy Index Snow Model'C,'EISModel'C,MB$OK)
return
end
! *******************************************************************
subroutine ferma_tutto ( dlg, control_name, callbacktype )
!DEC$ ATTRIBUTES DEFAULT :: ferma_tutto
USE DFLOGM
use dflib
use eis_mod
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype
TYPE (dialog) dlg
retint = SETEXITQQ(QWIN$EXITNOPERSIST)
stop
return
end
! *******************************************************************
subroutine salva_comandi ( dlg, control_name, callbacktype )
!DEC$ ATTRIBUTES DEFAULT :: salva_comandi
USE DFLOGM
use eis_mod
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype
TYPE (dialog) dlg
estensione = '*.csv'
tipofile = 'Command File'
call OpenNewFile (retint, estensione, tipofile, fileComandi)
if (retint .neqv. .false.) then
   open (io_com,file=fileComandi)
   call writeinput (io_com)
   close (io_com)
endif
return
end
! *******************************************************************
subroutine leggi_da_file ( dlg, control_name, callbacktype )
!DEC$ ATTRIBUTES DEFAULT :: leggi_da_file
USE DFLOGM
use eis_mod
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype, IDC, j
TYPE (dialog) dlg
! ------------------------------------------------
estensione = '*.csv'
tipofile = 'Command File'
call OpenOldFile (retint, estensione, tipofile, fileComandi)
if (retint .eqv. .FALSE.) return
call readinput
! ------------------------------------------------------
   retlog = DlgSet( dlg, IDC_SimHeader, intestazione )
   retlog = DlgSet( dlg, IDC_Log_File, fileLog )
   retlog = DlgSet( dlg, IDC_Output_File, fileOutput )
   retlog = DlgSet( dlg, IDC_Format_File, fileFormat )
   retlog = DlgSet( dlg, IDC_Elevation_File, fileElevation )
   retlog = DlgSet( dlg, IDC_Input_Mask, fileMask )
   retlog = DlgSet( dlg, IDC_Input_File, fileInput_we(1) )
   retlog = DlgSet( dlg, IDC_Input_File2, fileInput_we(2) )
   retlog = DlgSet( dlg, IDC_Input_File4, fileInput_lqw )
   retlog = DlgSet( dlg, IDC_Slope_File, fileSlope )
   retlog = DlgSet( dlg, IDC_Aspect_File, fileAspect )
   retlog = DlgSet( dlg, IDC_Albedo_File, fileIceAlbedo )
   retlog = DlgSet( dlg, IDC_Climatic_File, fileClimatic )
   retlog = DlgSet( dlg, IDC_IceLength_File, fileIceLength )
   retlog = DlgSet( dlg, IDC_Zone_File, fileZone )
   retlog = DlgSet( dlg, IDC_Horizon_File, fileHorizon )
   retlog = DlgSet( dlg, IDC_Shapefile, fileTestPoints)
   write (text,'(i2)') no_Precip
   retlog = DlgSet( dlg, IDC_EDT_noPrec, text)
   write (text,'(i2)') no_Tempera
   retlog = DlgSet( dlg, IDC_EDT_noTemp, text)
   write (text,'(i2)') no_Mappe
   retlog = DlgSet( dlg, IDC_EDT_noMaps, text)
   text = 'OnLine'
   retlog = DlgSet( dlg, IDC_EDT_noEneInd, text)
   retlog = DlgSet( dlg, IDC_CHECK_Parameters, parmfit)
! ----------------------------------------------------- form parametri 
   write (text,'(f14.5)') FREEZE
   retlog = DlgSet( dlg, IDC_EDT_Freez, text)
   write (text,'(f14.5)') RainMF
   retlog = DlgSet( dlg, IDC_EDT_RainMF, text)
   write (text,'(f14.5)') TMF(1)
   retlog = DlgSet( dlg, IDC_EDT_TMF1, text)
   write (text,'(f14.5)') TMF(2)
   retlog = DlgSet( dlg, IDC_EDT_TMF2, text)
   write (text,'(f14.5)') RMF(1)
   retlog = DlgSet( dlg, IDC_EDT_RMF1, text)
   write (text,'(f14.5)') RMF(2)
   retlog = DlgSet( dlg, IDC_EDT_RMF2, text)
   write (text,'(f14.5)') pp_low
   retlog = DlgSet( dlg, IDC_EDT_PPlow, text)
   write (text,'(f14.5)') PXTEM
   retlog = DlgSet( dlg, IDC_EDT_PXTEM, text)
   write (text,'(f14.5)') SCF
   retlog = DlgSet( dlg, IDC_EDT_SCF, text)
   write (text,'(f14.5)') BASMELT
   retlog = DlgSet( dlg, IDC_EDT_BASMELT, text)
   write (text,'(f14.5)') RLIQFR
   retlog = DlgSet( dlg, IDC_EDT_RLIQFR, text)
! -------------------------------------------
   SELECT CASE (hourly_data)
       CASE (.true.)
           retlog = DlgSet( dlg, IDC_RAD_stepH1, .TRUE.)  ! hourly
       CASE (.false.)
           retlog = DlgSet( dlg, IDC_RAD_stepH2, .TRUE.)  ! daily
   END SELECT
! -------------------------------------------
   SELECT CASE (Algorithm)
       CASE (1)
           retlog = DlgSet( dlg, IDC_RAD_Algo1, .TRUE.)  ! multiplicative
       CASE (2)
           retlog = DlgSet( dlg, IDC_RAD_Algo2, .TRUE.)  ! additive
       CASE (3)
           retlog = DlgSet( dlg, IDC_RAD_Algo3, .TRUE.)  ! enhanced
       END SELECT
   retlog = DlgSet( dlg, IDC_CHECK_FNMF, NMFeqTMF)
   write (text,'(f14.5)') Snow_to_IceYears
   retlog = DlgSet( dlg, IDC_EDT_Snow_to_IceYears, text)
   write (text,'(f14.5)') albedo_curve_intercept
   retlog = DlgSet(dlg, IDC_EDT_WE1, text)
   write (text,'(f14.5)') albedo_curve_slope
   retlog = DlgSet(dlg, IDC_EDT_WE2, text)
   write (text,'(f14.5)') fresh_snow_albedo
   retlog = DlgSet(dlg, IDC_EDT_WE3, text)
   write (text,'(f14.5)') const_ice_albedo
   retlog = DlgSet(dlg, IDC_EDT_WE4, text)
   write (text,'(f14.5)') snow_init_albedo
   retlog = DlgSet(dlg, IDC_EDT_WE5, text)
   write (text,'(f14.5)') ritardo
   retlog = DlgSet( dlg, IDC_EDT_delay, text)
   SELECT CASE (bascicli)
       CASE (1)
           retlog = DlgSet( dlg, IDC_RAD_bascomp1, .TRUE.)
       CASE (2)
           retlog = DlgSet( dlg, IDC_RAD_bascomp2, .TRUE.)
       CASE (4)
           retlog = DlgSet( dlg, IDC_RAD_bascomp4, .TRUE.)
       CASE (6)
           retlog = DlgSet( dlg, IDC_RAD_bascomp6, .TRUE.)
   END SELECT
!-------------------------------------------------------  form precipitazione
call PrecCoordWriter ( dlg, control_name, callbacktype )
write (text,'(i2)', iostat=retint) no_Precip
retlog = DlgSet( dlg, IDC_EDT_noPrec, text)
call PrecLapseRWriter ( dlg, control_name, callbacktype )
write (text,'(f20.8)') NablaPrec
retlog = DlgSet( dlg, IDC_EDT_NablaPrec, text)
write (text,'(f20.8)') SCF
retlog = DlgSet( dlg, IDC_EDT_SCF, text)
retlog = DlgSet( dlg, IDC_Input_SRF, fileSRF )
retlog = DlgSet( dlg, IDC_CHECK_NELE, normalizeELE)
retlog = DlgSet( dlg, IDC_CHECK_NSCF, normalizeSCF)
retlog = DlgSet( dlg, IDC_CHECK_NSRF, normalizeSRF)
!-------------------------------------------------------  form temperatura
if (fileLapseRate =='.' .and. (LR_algo == 4 .or. LR_algo == 5)) LR_algo = 1
if (no_Tempera < 2          .and. (LR_algo == 2 .or. LR_algo == 3)) LR_algo = 1
write (text,'(f13.1)') tropopause
retlog = DlgSet( dlg, IDC_EDT_TermInv, text)
write (text,'(f15.4)') TemperatureLapseRate
retlog = DlgSet( dlg, IDC_EDT_NablaTemp, text)
retlog = DlgSet( dlg, IDC_LapseRate_File, fileLapseRate )
call TempCoordWriter ( dlg, control_name, callbacktype )
write (text,'(i2)', iostat=retint) no_Tempera
retlog = DlgSet( dlg, IDC_EDT_noTemp, text)
call LROnline( dlg, control_name, callbacktype )
!-------------------------------------------------------  form mappe
do IDC = IDC_EDT_Maps1, IDC_EDT_Maps10
  index1 = IDC-IDC_EDT_Maps1+1
  retlog = DlgSet( dlg, IDC, DataMap_str(index1))
!  call aggiorna_outmaps( dlg, IDC, dlg_change)
enddo
do IDC = IDC_Maps_File1, IDC_Maps_File10
  index1 = IDC - IDC_Maps_File1 + 1
  retlog = DlgSet( dlg, IDC, fileMaps(index1))
enddo
! ------------------------------------------------------- form energia
   retlog = DlgSet( dlg, IDC_CHECK_EIcum, CumulateEI)
   retlog = DlgSet( dlg, IDC_EDT_date3, DataEI_str)
   write (text,'(f14.5)') Latitudine*180./PI
   retlog = DlgSet( dlg, IDC_EDT_Latitude, text)
   write (text,'(f14.5)') diffusivita
   retlog = DlgSet( dlg, IDC_EDT_Diffuse, text)
   write (text,'(f14.5)') Profondita_ottica
   retlog = DlgSet( dlg, IDC_EDT_Deepness, text)
   write (text,'(f14.5)') Costante_solare
   retlog = DlgSet( dlg, IDC_EDT_SolarC, text)
   retlog = DlgSet( dlg, IDC_RAD_solcomp1, .TRUE.)
   if(txtRadStep.eq.'d')then
    retlog = DlgSet( dlg, IDC_RAD_RadStep1,.true.)
   elseif(txtRadStep.eq.'h')then
    retlog = DlgSet( dlg, IDC_RAD_RadStep2,.true.)
    CumulateEI = .false.
   endif
   call enable_cumEI_cntrl( dlg, control_name, callbacktype )
! -----------------------------------------------------------------------
return
end subroutine leggi_da_file
! *************************************************************************
! -----------------------------------------
subroutine OpenOldFile (istat, estensione, tipofile, lpStrPtr)
use dfwin
implicit none
integer   iret, istat
integer , parameter :: FILE_LEN = 80
type (T_OPENFILENAME)   OpenFN
character*256           szDirName
character*256           szFile
character*256           szFileTitle
character*256           szFilter
character*100           lpszTitle, lpszDefExt, string 
character*256           lpStrPtr
character*25            tipofile
character*5             estensione
integer :: hWnd = 0, hInst = 0
lpStrPtr = ' '
szFile = estensione//char(0)
szFileTitle = ' '//char(0)
lpszTitle= 'Open an Existing File'//char(0)
lpszDefExt= estensione//char(0)

!szFilter = "Text Files"C//"*.txt"C// "command Files"C//"*.csv"C//""C
szFilter = tipofile//char(0)//estensione//char(0)//"All Files"C//"*.*"C//""C


   OpenFN%lStructSize       = sizeof(OpenFN)
   OpenFN%hwndOwner         = hWnd
   OpenFN%hInstance         = hInst
   OpenFN%lpstrFilter       = LOC(szFilter)
   OpenFN%lpstrCustomFilter = NULL
   OpenFN%nMaxCustFilter    = 0
   OpenFN%nFilterIndex      = 1
   OpenFN%lpstrFile         = LOC(szFile)
   OpenFN%nMaxFile          = sizeof(szFile)
   OpenFN%lpstrFileTitle    = LOC(szFileTitle)
   OpenFN%nMaxFileTitle     = sizeof(szFileTitle)
   OpenFN%lpstrInitialDir   = NULL
   OpenFN%lpstrTitle        = LOC(lpszTitle)
   OpenFN%nFileOffset       = 0
   OpenFN%nFileExtension    = 0
   OpenFN%lpstrDefExt       = LOC(lpszDefExt)
   OpenFN%lCustData         = 0

OpenFN%Flags = IOR(OFN_SHOWHELP , IOR(OFN_PATHMUSTEXIST, IOR(OFN_FILEMUSTEXIST , OFN_HIDEREADONLY)))

istat = GetOpenFileName(OpenFN) 
if (istat .neqv. .FALSE.) then
   lpStrPtr = szFile
  return
endif

end
! *************************************************************************
! -----------------------------------------
subroutine OpenNewFile (istat, estensione, tipofile, lpStrPtr)
use dfwin
implicit none
integer   iret, istat
integer , parameter :: FILE_LEN = 80
type (T_OPENFILENAME)   OpenFN
character*256           szDirName
character*256           szFile
character*256           szFileTitle
character*256           szFilter
character*100           lpszTitle, lpszDefExt, string 
character*256           lpStrPtr
character*25            tipofile
character*5             estensione
integer :: hWnd = 0, hInst = 0

lpStrPtr = ' '
szFile = estensione//char(0)
szFileTitle = ' '//char(0)
lpszTitle= 'Open a New File'//char(0)
lpszDefExt= estensione//char(0)

!szFilter = "Text Files"C//"*.txt"C// "Fortran Files"C//"*.f90;*.f"C//""C
szFilter = tipofile//char(0)//estensione//char(0)//"All Files"C//"*.*"C//""C


   OpenFN%lStructSize       = sizeof(OpenFN)
   OpenFN%hwndOwner         = hWnd
   OpenFN%hInstance         = hInst
   OpenFN%lpstrFilter       = LOC(szFilter)
   OpenFN%lpstrCustomFilter = NULL
   OpenFN%nMaxCustFilter    = 0
   OpenFN%nFilterIndex      = 1
   OpenFN%lpstrFile         = LOC(szFile)
   OpenFN%nMaxFile          = sizeof(szFile)
   OpenFN%lpstrFileTitle    = LOC(szFileTitle)
   OpenFN%nMaxFileTitle     = sizeof(szFileTitle)
   OpenFN%lpstrInitialDir   = NULL
   OpenFN%lpstrTitle        = LOC(lpszTitle)
   OpenFN%nFileOffset       = 0
   OpenFN%nFileExtension    = 0
   OpenFN%lpstrDefExt       = LOC(lpszDefExt)
   OpenFN%lCustData         = 0
OpenFN%Flags = IOR(OFN_SHOWHELP , IOR(OFN_PATHMUSTEXIST, OFN_HIDEREADONLY))

istat = GetSaveFileName(OpenFN) 
if (istat .neqv. .FALSE.) then
  lpStrPtr = szFile
  return
endif
end
