! *****************************************************************
subroutine Parameters
USE DFLOGM
use eis_mod
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype, IDC
TYPE (dialog) dlg
external  aggiorna_Parameters
! ----------------------------------------------------------------
IF ( .not. DlgInit( IDD_Parameters, dlg ) ) THEN
   WRITE (*,*) "Error: dialog not found"
ELSE
! Activate the modal dialog. 
   retlog = DlgSet( dlg, IDC_EDT_date0, dataControl_str(0))
   retlog = DlgSet( dlg, IDC_EDT_date1, dataControl_str(1))
   retlog = DlgSet( dlg, IDC_EDT_date2, dataControl_str(2))
! -------------------------------------------
   SELECT CASE (hourly_data)
       CASE (.true.)
           retlog = DlgSet( dlg, IDC_RAD_stepH1, .TRUE.)  ! hourly
       CASE (.false.)
           retlog = DlgSet( dlg, IDC_RAD_stepH2, .TRUE.)  ! daily
   END SELECT
! --------------------------------------------------- 
   write (text,'(f14.5)') FREEZE
   retlog = DlgSet( dlg, IDC_EDT_Freez, text)
   write (text,'(f14.5)') RainMF
   retlog = DlgSet( dlg, IDC_EDT_RainMF, text)
   write (text,'(f14.5)') TMF(1)
   retlog = DlgSet( dlg, IDC_EDT_TMF1, text)
   write (text,'(f14.5)') TMF(2)
   retlog = DlgSet( dlg, IDC_EDT_TMF2, text)
   write (text,'(f14.5)') RMF(1)
   retlog = DlgSet( dlg, IDC_EDT_RMF1, text)
   write (text,'(f14.5)') RMF(2)
   retlog = DlgSet( dlg, IDC_EDT_RMF2, text)
   write (text,'(f14.5)') NMF
   retlog = DlgSet( dlg, IDC_EDT_NMF, text)
   write (text,'(f14.5)') PXTEM
   retlog = DlgSet( dlg, IDC_EDT_PXTEM, text)
   write (text,'(f14.5)') PXRANGE
   retlog = DlgSet( dlg, IDC_EDT_PXRANGE, text)
   write (text,'(f14.5)') BASMELT
   retlog = DlgSet( dlg, IDC_EDT_BASMELT, text)
   write (text,'(f14.5)') RLIQFR
   retlog = DlgSet( dlg, IDC_EDT_RLIQFR, text)
   write (text,'(f14.5)') pp_low
   retlog = DlgSet( dlg, IDC_EDT_PPlow, text)
! -------------------------------------------
   SELECT CASE (Algorithm)
       CASE (1)
           retlog = DlgSet( dlg, IDC_RAD_Algo1, .TRUE.)  ! multiplicative
       CASE (2)
           retlog = DlgSet( dlg, IDC_RAD_Algo2, .TRUE.)  ! additive
       CASE (3)
           retlog = DlgSet( dlg, IDC_RAD_Algo3, .TRUE.)  ! combined  
   END SELECT
   retlog = DlgSet( dlg, IDC_CHECK_FNMF, NMFeqTMF)
   write (text,'(f14.5)') ritardo
   retlog = DlgSet( dlg, IDC_EDT_delay, text)
! ---------------------------------------------------   
   SELECT CASE (bascicli)
       CASE (1)
           retlog = DlgSet( dlg, IDC_RAD_bascomp1, .TRUE.)
       CASE (2)
           retlog = DlgSet( dlg, IDC_RAD_bascomp2, .TRUE.)
       CASE (4)
           retlog = DlgSet( dlg, IDC_RAD_bascomp4, .TRUE.)
       CASE (6)
           retlog = DlgSet( dlg, IDC_RAD_bascomp6, .TRUE.)
   END SELECT
! -------------------------------------------------------------------------
! ............................................................... edit
   retlog = DlgSetSub( dlg, IDC_EDT_date0, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_EDT_date1, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_EDT_date2, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_RAD_stepH1, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_RAD_stepH2, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_EDT_Freez, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_EDT_RainMF, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_EDT_TMF1, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_EDT_TMF2, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_EDT_RMF1, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_EDT_RMF2, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_EDT_NMF, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_EDT_PXTEM, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_EDT_PXRANGE, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_EDT_BASMELT, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_EDT_RLIQFR, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_EDT_PPlow, aggiorna_Parameters)
! ............................................................... radio button
   retlog = DlgSetSub( dlg, IDC_RAD_Algo1, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_RAD_Algo2, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_RAD_Algo3, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_CHECK_FNMF, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_EDT_delay, aggiorna_Parameters )
! ............................................................... radio button
   retlog = DlgSetSub( dlg, IDC_RAD_bascomp1, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_RAD_bascomp2, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_RAD_bascomp4, aggiorna_Parameters )
   retlog = DlgSetSub( dlg, IDC_RAD_bascomp6, aggiorna_Parameters )

! ...............................................................
retint = DlgModal( dlg ) 
CALL DlgUninit( dlg )
END IF
return
end
! *******************************************************************
subroutine aggiorna_Parameters ( dlg, control_name, callbacktype )
!DEC$ ATTRIBUTES DEFAULT :: aggiorna_MinEI
USE DFLOGM
use eis_mod
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype
character*256 NoCString
TYPE (dialog) dlg
! -------------------------------------------------------------
SELECT CASE (control_name)
! -------------------------------------------
  CASE (IDC_EDT_date0:IDC_EDT_date2)
    retlog = DlgGet( dlg, control_name, text )
    index1 = control_name-IDC_EDT_date0
    read (text,*, iostat=retint) dataControl_str(index1)
    parmfit = (dataControl_str(0) /=' '.and. dataControl_str(1) /=' ' .and. dataControl_str(2) /= ' '.and. &   
               dataControl_str(0) /='.'.and. dataControl_str(1) /='.' .and. dataControl_str(2) /= '.')    
! .....................................................
  CASE (IDC_RAD_stepH1)
    hourly_data = .true.         ! hourly
  CASE (IDC_RAD_stepH2)
    hourly_data = .false.         ! daily
    txtRadStep = 'd'
  CASE (IDC_EDT_Freez)
    retlog = DlgGet( dlg, IDC_EDT_Freez, text )
    read (text, *, iostat=retint) Freeze
  CASE (IDC_EDT_RainMF)
    retlog = DlgGet( dlg, IDC_EDT_RainMF, text )
    read (text, *, iostat=retint) RainMF
  CASE (IDC_EDT_TMF1)
    retlog = DlgGet( dlg, IDC_EDT_TMF1, text )
    read (text, *, iostat=retint) TMF(1)
    if (TMF(2) < 0) then
        TMF(2) = TMF(1)
        write (text,'(f14.5)') TMF(2)
        retlog = DlgSet( dlg, IDC_EDT_TMF2, text)
    endif   
  CASE (IDC_EDT_TMF2)
    retlog = DlgGet( dlg, IDC_EDT_TMF2, text )
    read (text, *, iostat=retint) TMF(2)
    if (TMF(2) < 0) then
        TMF(2) = TMF(1)
        write (text,'(f14.5)') TMF(2)
        retlog = DlgSet( dlg, IDC_EDT_TMF2, text)
    endif   
  CASE (IDC_EDT_RMF1)
    retlog = DlgGet( dlg, IDC_EDT_RMF1, text )
    read (text, *, iostat=retint) RMF(1)
    if (RMF(2) < 0) then
        RMF(2) = TMF(1)
        write (text,'(f14.5)') RMF(2)
        retlog = DlgSet( dlg, IDC_EDT_RMF2, text)
    endif   
  CASE (IDC_EDT_RMF2)
    retlog = DlgGet( dlg, IDC_EDT_RMF2, text )
    read (text, *, iostat=retint) RMF(2)
    if (RMF(2) < 0) then
        RMF(2) = TMF(1)
        write (text,'(f14.5)') RMF(2)
        retlog = DlgSet( dlg, IDC_EDT_RMF2, text)
    endif   
  CASE (IDC_EDT_NMF)
    retlog = DlgGet( dlg, IDC_EDT_NMF, text )
    read (text, *, iostat=retint) NMF
  CASE (IDC_EDT_PPlow)
    retlog = DlgGet( dlg, IDC_EDT_PPlow, text )
    read (text, *, iostat=retint) pp_low
  CASE (IDC_EDT_PXTEM)
    retlog = DlgGet( dlg, IDC_EDT_PXTEM, text )
    read (text, *, iostat=retint) PXTEM
  CASE (IDC_EDT_PXRANGE)
    retlog = DlgGet( dlg, IDC_EDT_PXRANGE, text )
    read (text, *, iostat=retint) PXrange
  CASE (IDC_EDT_BASMELT)
    retlog = DlgGet( dlg, IDC_EDT_BASMELT, text )
    read (text, *, iostat=retint) BASMELT
  CASE (IDC_EDT_RLIQFR)
    retlog = DlgGet( dlg, IDC_EDT_RLIQFR, text )
    read (text, *, iostat=retint) RLIQFR
! -------------------------------------------
  CASE (IDC_RAD_Algo1)
       Algorithm = 1         ! multiplicative
  CASE (IDC_RAD_Algo2)
       Algorithm = 2         ! additive
  CASE (IDC_RAD_Algo3)
       Algorithm = 3         ! combined
  CASE (IDC_CHECK_FNMF)
    NMFeqTMF = .not.NMFeqTMF
    continue
! ..............................................
  CASE (IDC_EDT_delay)
    retlog = DlgGet( dlg, IDC_EDT_delay, text )
    read (text, *, iostat=retint) ritardo
    if (.not.hourly_data) then
        ritardo = 0.
        write (text,'(f14.5)') ritardo
        retlog = DlgSet( dlg, IDC_EDT_delay, text)
    endif
! -------------------------------------------
  CASE (IDC_RAD_bascomp1)
       bascicli = 1
  CASE (IDC_RAD_bascomp2)
       bascicli = 2
       if (.not.hourly_data) then
            bascicli = 1
            retlog = DlgSet( dlg, IDC_RAD_bascomp2, .false.)
            retlog = DlgSet( dlg, IDC_RAD_bascomp1, .true.)
       endif
  CASE (IDC_RAD_bascomp4)
       bascicli = 4
       if (.not.hourly_data) then
            bascicli = 1
            retlog = DlgSet( dlg, IDC_RAD_bascomp4, .false.)
            retlog = DlgSet( dlg, IDC_RAD_bascomp1, .true.)
       endif
  CASE (IDC_RAD_bascomp6)
       bascicli = 6
       if (.not.hourly_data) then
            bascicli = 1
            retlog = DlgSet( dlg, IDC_RAD_bascomp6, .false.)
            retlog = DlgSet( dlg, IDC_RAD_bascomp1, .true.)
       endif
END SELECT
end subroutine aggiorna_Parameters
! *******************************************************************
