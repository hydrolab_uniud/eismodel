//{{NO_DEPENDENCIES}}
// File di inclusione generato con Microsoft Visual C++.
// Utilizzato da Command.rc
//
#define IDD_ComForm                     1001
#define IDSalva                         1002
#define IDLoad                          1003
#define IDInformations                  1004
#define ID_Cancella                     1005
#define IDI_ICON2                       1101
#define IDC_BTN_Header                  2001
#define IDC_EDT_Header                  2002
#define IDC_BTN_Elevation               2003
#define IDC_EDT_Elevation               2004
#define IDC_BTN_Horizon                 2005
#define IDC_EDT_Horizon                 2006
#define IDC_BTN_Vector                  2007
#define IDC_EDT_Vector                  2008
#define IDC_STA_Precision               2101
#define IDC_STA_Samples                 2102
#define IDC_STA_Samples2                2103
#define IDC_Precision0                  2200
#define IDC_Precision1                  2201
#define IDC_Precision2                  2202
#define IDC_Precision3                  2203
#define IDC_Precision4                  2204
#define IDC_Precision5                  2205
#define IDC_Precision6                  2206

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        1102
#define _APS_NEXT_COMMAND_VALUE         1201
#define _APS_NEXT_CONTROL_VALUE         2301
#define _APS_NEXT_SYMED_VALUE           3001
#endif
#endif
