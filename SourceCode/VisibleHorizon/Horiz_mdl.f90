module horiz_mdl
! ----------------------------------------------------------------------
parameter    (PI=3.141592653589793238462)
parameter    (NUMVEC=10001)              ! numero massimo di vertici finali
character*256 fileComandi, fileLog, fileFormat
character*256 fileElevation, fileHorizon, fileVector
character*256 text
character*25  tipofile
character*15  dettaglio
character*10  :: byteorder = 'LSBFIRST'
character*5   estensione
logical       retlog
logical       sector, correggi_hrz
integer       no_columns, no_rows, maxcel, numero_vertici
integer       icol,irow, true_cell, cell_cnt, precisione
integer       xi(NUMVEC),yi(NUMVEC)
integer       retint, io_com
integer*2, allocatable :: alzo(:,:,:)
integer*2, allocatable :: cxy(:,:,:,:)
real,      allocatable :: ele(:,:)
real          pixsize, or_easting, or_northing, esterno, undefined
real          zi(NUMVEC)
data          undefined / -8888.0 /, esterno / -9999.0 /
data          precisione /6/, dettaglio /'Full Precision '/,sector /.false./
data          fileVector /'         '/, io_com /10/
! ----------------------------------------------------------------------
end module horiz_mdl
