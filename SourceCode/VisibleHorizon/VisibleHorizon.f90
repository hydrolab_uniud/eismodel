program VisibleHorizon
! -------------------------- trova gli angoli del contorno apparente
use dflib
use horiz_mdl
! ----------------------------------------------------------
integer*2      hr, min, sec, cent
integer*2      get_status
character*8    orario
logical        esiste
! ------------------------------------------------------------------
open ( 0,file='user',title='Visible Horizon',carriagecontrol='fortran')
i4 = setbkcolor(15)
i4 = settextcolor(INT2(0)) 
call clearscreen($GCLEARSCREEN)
open (2,file='user',title='Parameter Definition',carriagecontrol='fortran')
i4 = setbkcolor(15)
i4 = settextcolor(INT2(0)) 
call clearscreen($GCLEARSCREEN)
call getarg (1, fileComandi, get_status)
fileFormat = '  '
fileElevation = '  '
fileHorizon = '  '
if (get_status > 0) then
  i4 = SETEXITQQ(QWIN$EXITNOPERSIST)
  call readinput
  call decodeinput (2)
else
  i4 = SETEXITQQ(QWIN$EXITPERSIST)
  call CommandFile
  do while (fileFormat(1:1)==' '.or.fileElevation(1:1)==' '.or.fileHorizon(1:1)==' ')
    iresponse = MESSAGEBOXQQ( 'A Compulsory File Name is Missing'C,'Retry or Cancel?'C, MB$RETRYCANCEL )
    if (iresponse == MB$IDRETRY ) then
      call CommandFile
    else
      stop
    endif
  enddo
  estensione = '*.hor'
  tipofile = 'File Comandi'
  call OpenNewFile (istat, estensione, tipofile, fileComandi)
  if (istat .eqv. .FALSE.) stop
  call decodeinput (2)
  call writeinput  (3)
endif
call cpu_time (ora_inizio)
call time(orario)
write (0,'('' execution begins at '',a8)') orario
! ..................................................................
call detail
if (sector) then
  call settori
else
  call angoli
endif
if (correggi_hrz) call interpola
if (correggi_hrz) call correggi
call scrivi
close (12)
! ..............................................................
call cpu_time (ora_fine)
call time(orario)
write (0,'('' execution  ends  at '',a8)') orario
tempo   = ora_fine - ora_inizio
call hhmmss(tempo, iore, minuti,isecond)
write (0,'('' execution  time  is '',i2,a1,i2.2,a1,i2.2,a1,a1)')        &
              iore,char(104),minuti,char(39),isecond,char(39),char(39)
stop '-------------------------------------------------------- cz'
end
! ****************************************************************************
subroutine readinput
use horiz_mdl
character*256 NoCString
open (10, file=fileComandi, status='old',err=100)
! -------------------------------------------------------------------
read (10,'(30x,a)')   fileFormat
fileFormat = NoCString(fileFormat)
read (10,'(30x,a)')   fileElevation 
fileElevation = NoCString(fileElevation)
read (10,'(30x,a)')   fileHorizon 
fileHorizon = NoCString(fileHorizon)
read (10,'(30x,a)')   fileVector 
fileVector = NoCString(fileVector)
read (10,'(30x,a)')   dettaglio
dettaglio = NoCString(dettaglio)
close (10)
do j=1,20
  if (dettaglio(j:j) /= ' ') exit
enddo
precisione = ichar(dettaglio(j:j)) - 48           ! 49 = 1, 50=2, ecc.
sector = (precisione >=1 .and. precisione <= 6)
precisione = max(precisione,1)
precisione = min(precisione,6)
correggi_hrz = (fileVector(1:1) /= ' ')
! .................................................................
return
100 stop ' -------- Error opening a file ---------------------------'
end
! ****************************************************************************
subroutine writeinput (ifile)
use horiz_mdl
integer ifile
open (ifile,file=fileComandi)
! -------------------------------------------------------------------
write (ifile,'(''Header               File    :'',a)')   fileFormat
write (ifile,'(''Elevation            Grid    :'',a)')   fileElevation
write (ifile,'(''Visible Horizon      File    :'',a)')   fileHorizon  
write (ifile,'(''Vector Profile       File    :'',a)')   fileVector 
write (ifile,'(''Computing detail             :'',a)')   dettaglio
close (ifile)
! .................................................................
return
end
! *************************************************************
subroutine decodeinput (iout)
use horiz_mdl
! --------------------------------------------------
write (0,'(//'' .... vector initialization ................'')')
! ------------------------------------------------------------------------------
open (1,file=fileFormat,mode='read',err=100)
call hdrread (1)
close (1)
! call keywrite (iout)
! --------------------------------------------------------------------
allocate (alzo(no_columns, no_rows,0:23))
alzo = 0
allocate (ele (no_columns, no_rows))
open  ( 1,file=fileElevation,mode='read',form='binary',recl=4)
do row = 1, no_rows
  read  (1) (ele(col,row),col=1,no_columns) 
enddo
close (1)
true_cell = count(ele /= esterno)
maxcel = max(no_columns,no_rows)
open  (12,file=fileHorizon,form='binary')
open  (13,file=trim(fileHorizon)//'.txt')
write (iout,'('' Header file                 _'',a)') trim(fileFormat)
write (iout,'('' Elevation            Grid   _'',a)') trim(fileElevation)
write (iout,'('' Horizon              data   _'',a)') trim(fileHorizon)
write (iout,'('' Vector Profile       File   _'',a)') trim(fileVector) 
write (iout,'('' Computing detail     code   _'',a)') trim(dettaglio)
return
100 stop ' -------- Error opening a file ---------------------------'
end
! ***************************************************************
subroutine detail
use horiz_mdl
real ang(6)
! ----------------------------------------------------------------------
if (sector) then
  write (0,'(  '' .... sampling procedure ...................'')')
  write (0,'(  '' .... '',i1,'' samples per sector ........................'')') precisione
  allocate (cxy(maxcel,0:23,precisione,2))
  step = 15./precisione            
  do j=1,maxcel
    do ia = 0,23
      sector_beg = ia*15. - 7.5
      do ip = 1,precisione
        angolo = sector_beg + (ip-0.5) * step 
        cxy (j,ia,ip, 1) =  inint(j * sind(angolo))
        cxy (j,ia,ip, 2) = -inint(j * cosd(angolo))
      enddo
    enddo
  enddo
else
  write (0,'(  '' .... detailed analisys   .........................'')')
endif
return
end
! ***************************************************************
subroutine scrivi
use horiz_mdl
character*1 orizzonte(0:23)
integer*2   col, row
!radiante = PI*100./180.0
write (0,'(  '' .... scrittura risultati .........................''/)')
! ----------------------------------------------------------------------
do row = 1, no_rows
  do col = 1, no_columns
    if (ele(col,row) /= esterno) then
      do ih=0,23
!       orizzonte(ih) = char(alzo(col,row,ih)*radiante)
        orizzonte(ih) = char(alzo(col,row,ih))             ! in gradi
      enddo
      write (12) col, row, orizzonte
!      write (13,'(2i4,i5,24i4)') col, row, nint(ele(col,row)), (alzo(col,row,ih),ih=0,23)
      write (13,'(2i4,24i4)') col, row, (alzo(col,row,ih),ih=0,23)
    endif
  enddo
enddo
deallocate (ele)
deallocate (alzo)
return
end
! **********************************************************************
subroutine settori
use horiz_mdl
integer*2 angolo, vert_ang
! ----------------------------------------------------------------------
write (0,'(  '' .... Visible Orographic Horizon  .........................'')')
write (0,'(  ''     0.00 percent done'')') 
! -------------------------------------------------------------------
cell_cnt = 0
do irow = 1, no_rows
  do icol = 1, no_columns
! ---------------------------------------------------------
    if (ele(icol,irow) /= esterno ) then 
      cell_cnt = cell_cnt + 1
      percento = 100. * real(cell_cnt)/real(true_cell)
      if (mod(cell_cnt,100) == 0 .or. cell_cnt == true_cell)  write (0,'( ''+'',f8.2)') percento
      do ih = 0,23
       do ip = 1, precisione
        do j=1,maxcel
          ix = icol + cxy(j,ih,ip,1)
          iy = irow + cxy(j,ih,ip,2)
          if (ix <= 0 .or. ix > no_columns .or. iy <= 0 .or. iy > no_rows) exit
          if (ele(ix,iy) /= esterno .and. ele(ix,iy) > ele(icol,irow)) then
            raggio = sqrt(((cxy(j,ih,ip,1)*pixsize)**2 + (cxy(j,ih,ip,2)*pixsize)**2))    ! in metri
            vert_ang = inint(atand((ele(ix,iy) - ele(icol,irow))/raggio))
            alzo(icol,irow,ih) = imax0(alzo(icol,irow,ih),vert_ang)
          endif
        enddo
       enddo
      enddo
    endif
  enddo
enddo
return
end
! **********************************************************************
subroutine angoli
use horiz_mdl
integer*2 vert_ang, horz_ang, hour
! ----------------------------------------------------------------------
write (0,'(  '' .... Visible Orographic Horizon  .........................'')')
write (0,'(  ''     0.00 percent done'')') 
! -------------------------------------------------------------------
cell_cnt = 0
do irow = 1, no_rows
  do icol = 1, no_columns
! ---------------------------------------------------------
    if (ele(icol,irow) /= esterno ) then 
      cell_cnt = cell_cnt + 1
      percento = 100. * real(cell_cnt)/real(true_cell)
      if (mod(cell_cnt,100) == 0 .or. cell_cnt == true_cell)  write (0,'( ''+'',f8.2)') percento
      do ir = 1, no_rows
        do ic = 1, no_columns
          if (ele(ic,ir) /= esterno .and. ele(ic,ir) > ele(icol,irow)) then
            ix = ic - icol
            iy = ir - irow
            raggio = sqrt(((ix * pixsize)**2 + (iy * pixsize)**2))    ! in metri
! ------------------------------------ angolo verticale rispetto al piano
            vert_ang = inint(atand((ele(ic,ir) - ele(icol,irow))/raggio))
! ------------------------------------ angolo orizzontale rispetto al nord
            if     (ix == 0 )       then
              if     (iy < 0) then
                angolo =   0.                        ! asse 4-1
              elseif (iy > 0) then
                angolo = 180.                        ! asse 2-3
              endif
            elseif ( iy == 0)       then
              if     (ix < 0) then
                angolo = 270.                        ! asse 3-4
              elseif (ix > 0) then
                angolo =  90.                        ! asse 1-2
              endif
            else
              angolo = atand(real(iy)/real(ix))
! ------------------------------------ identificazione del quadrante
              if     (ix > 0) then
                angolo =   90. + angolo        ! quadranti 1 e 2
              elseif (ix < 0) then
                angolo =  270. + angolo        ! quadranti 3 e 4
              endif
            endif
            horz_ang = mod(inint(angolo/15.),24_2)
! ----------------------------------------------- ora da 0 a 23
            alzo(icol,irow,horz_ang) = imax0(alzo(icol,irow,horz_ang),vert_ang)
! ---------------------------- per le celle del kernel 3x3 e per quelle sugli assi del kernel 5x5
            if (raggio <= (2.*pixsize+0.0001)) then
              hour = mod(horz_ang+23_2,24_2)
              alzo(icol,irow,hour) = imax0(alzo(icol,irow,hour),vert_ang)
              hour = mod(horz_ang+ 1_2,24_2)
              alzo(icol,irow,hour) = imax0(alzo(icol,irow,hour),vert_ang)
            endif
          endif
        enddo
      enddo
    endif
  enddo
enddo
return
end
! **********************************************************************
subroutine interpola
use horiz_mdl
parameter    (NUMPNT=1001)              ! numero massimo di vertici per polilinea
real         x(NUMPNT),y(NUMPNT),z(NUMPNT)
character*1  code
! ----------------------------------------------------------------------
write (0,'(  '' .... vector data interpolation .................'')')
! -------------------------------------------------------------------
! ------------------------------- i vertici di ciascuna polilinea vengono interpolati
! ------------------------------- alla distanza massima di  pixsize
distmax = pixsize 
iv = 0
open (13, file=fileVector, mode='read', err=200)
read  (13,'(a1)',end=200) code
do np=1,1e5
  do j=1,NUMPNT
    if (j == NUMPNT)    stop ' .... ERROR! a polyline has too many vertex ...'
    read  (13,'(a1,f11.0,2f12.0)',end=100, err=200) code,x(j),y(j),z(j)
    if (code /= ' ') exit
  enddo
100 numdat = j-1
  iv = iv + 1
  xi(iv) = nint((x(1) - or_easting )/pixsize )
  yi(iv) = no_rows - int((y(1) - or_northing)/pixsize)
  zi(iv) = z(1)
  do j = 2, numdat
    nvert = int(sqrt((x(j)-x(j-1))**2+(y(j)-y(j-1))**2)/distmax)+1
    distx = (x(j)-x(j-1)) / nvert
    disty = (y(j)-y(j-1)) / nvert
    distz = (z(j)-z(j-1)) / nvert
    do k = 1, nvert
      iv = iv + 1
      if (iv == NUMVEC) stop ' .... ERROR! ............... too many vertex ...'
      xi(iv) = nint(((x(j-1) + k * distx) - or_easting )/pixsize)
      yi(iv) = no_rows - int(((y(j-1) + k * disty) - or_northing)/pixsize)
      zi(iv) = z(j-1) + k * distz
    enddo
  enddo
  if (eof(13)) exit
enddo
numero_vertici = iv
write (0,'(i10,'' vertex were rebuilt ....................'')') numero_vertici
return
200 write (0,'('' WARNING! - error related to the vector file. No correction will be made.'')')
correggi_hrz = .false.
return
end
! **********************************************************************
subroutine correggi
use horiz_mdl
integer*2 vert_ang, horz_ang, hour
! ----------------------------------------------------------------------
write (0,'(  '' .... External Horizon Correction .................'')')
write (0,'(  ''     0.00 percent done'')') 
! -------------------------------------------------------------------
cell_cnt = 0
do irow = 1, no_rows
  do icol = 1, no_columns
! ---------------------------------------------------------
    if (ele(icol,irow) /= esterno ) then 
      cell_cnt = cell_cnt + 1
      percento = 100. * real(cell_cnt)/real(true_cell)
      if (mod(cell_cnt,100) == 0 .or. cell_cnt == true_cell)  write (0,'( ''+'',f8.2)') percento
! ---------------------------------------------------------
      do nv = 1, numero_vertici
        if (zi(nv) > ele(icol,irow)) then
          ix = xi(nv) - icol
          iy = yi(nv) - irow
          raggio = sqrt(((ix * pixsize)**2 + (iy * pixsize)**2))    ! in metri
! ------------------------------------ angolo verticale rispetto al piano
          vert_ang = inint(atand((zi(nv) - ele(icol,irow))/raggio))
! ------------------------------------ angolo orizzontale rispetto al nord
          if     (ix == 0 )       then
            if     (iy < 0) then
              angolo =   0.                        ! asse 4-1
            elseif (iy > 0) then
              angolo = 180.                        ! asse 2-3
            endif
          elseif ( iy == 0)       then
            if     (ix < 0) then
              angolo = 270.                        ! asse 3-4
            elseif (ix > 0) then
              angolo =  90.                        ! asse 1-2
            endif
          else
            angolo = atand(real(iy)/real(ix))
! ------------------------------------ identificazione del quadrante
            if     (ix > 0) then
              angolo =   90. + angolo        ! quadranti 1 e 2
            elseif (ix < 0) then
              angolo =  270. + angolo        ! quadranti 3 e 4
            endif
          endif
          horz_ang = imod(inint(angolo/15.),24_2)
! ----------------------------------------------- ora da 0 a 23
          alzo(icol,irow,horz_ang) = imax0(alzo(icol,irow,horz_ang),vert_ang)
! ---------------------------- per le celle del kernel 3x3 e per quelle sugli assi del kernel 5x5
          if (raggio <= (2.*pixsize+0.0001)) then
            hour = imod(horz_ang+23_2,24_2)
            alzo(icol,irow,hour) = imax0(alzo(icol,irow,hour),vert_ang)
            hour = imod(horz_ang+ 1_2,24_2)
            alzo(icol,irow,hour) = imax0(alzo(icol,irow,hour),vert_ang)
          endif
        endif
      enddo
    endif
  enddo
enddo
return
end
! **************************************************************
character*256 function NoCString (stringa)
character*(*) stringa
i = SCAN (stringa, char(0))
if (i > 0)  then
  NoCString = adjustl(stringa(1:i-1))
else
  NoCString = adjustl(stringa)
endif
end function
! **************************************************************
subroutine hhmmss(time,ih,im,is)
is = nint(time)
im = int (is/60.)
ih = int (im/60.)
is = is - im * 60
im = im - ih * 60
return
end
! *****************************************************************
subroutine hdrread (n)
use horiz_mdl
character*60 alfa
character*12 ,beta, keyword(8)
data keyword /'ncols','nrows','xllcorner','yllcorner','cellsize','nodata_value','byteorder','undef_value'/
! --------------------------------------------------------------------
do j=1,8
  read (n,'(a60)',end=10) alfa
! ---------------------------------- reduce to lowercase
  beta = trim(ADJUSTL(alfa(1:scan(alfa,' ')-1)))
  do k = 1, len_trim(beta)
    if (ichar(beta(k:k)) >= 65 .and. ichar(beta(k:k)) <= 90) beta(k:k) = char(ichar(beta(k:k)) + 32)
  enddo
  do k=1,30
    if (beta == keyword(k)) then
      call hdrdecode (k, trim(ADJUSTL(alfa(scan(alfa,' '):60))))
      exit
    endif
  enddo
enddo
! ----------------------------------------- origine nel vertice NW
10 return
entry keywrite (iout)
write (iout,1000) keyword(1), real(no_columns)
write (iout,1000) keyword(2), real(no_rows)
write (iout,1000) keyword(3), or_easting
write (iout,1000) keyword(4), or_northing
write (iout,1000) keyword(5), pixsize
write (iout,1000) keyword(6), esterno
write (iout,1000) keyword(8), undefined
write (iout,'(1x,a12,17x,''_'',a10)') keyword(7), byteorder
1000  format( 1x,a12,17x,'_',f12.2)
return
end
! ********************************************************************
subroutine hdrdecode (k, alfa)
use horiz_mdl
character*(*)   alfa
! --------------------------------------------------------------------
select case (k)
  case (1)
    read  (alfa,*)           no_columns
  case (2)
    read  (alfa,*)           no_rows
  case (3)
    read  (alfa,*)           or_easting
  case (4)
    read  (alfa,*)           or_northing
  case (5)
    read  (alfa,*)           pixsize
  case (6)
    read  (alfa,*)           esterno
  case (7)
    read  (alfa,*)           byteorder
  case (8)
    read  (alfa,*)           undefined
end select
return
end
