subroutine CommandFile
USE DFLOGM
use horiz_mdl
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
!INTEGER control_name, callbacktype
TYPE (dialog) dlg
external aggiorna_campi, leggi_da_file, ferma_tutto, salva_comandi, messagebox
! ----------------------------------------------------------------
retlog =  DlgInit( IDD_ComForm, dlg )
IF (.not. retlog) THEN
   WRITE (*,*) "Error: dialog not found"
ELSE
! Activate the modal dialog. 
   retlog = DlgSet( dlg, IDC_EDT_Header, fileFormat )
   retlog = DlgSet( dlg, IDC_EDT_Elevation, fileElevation )
   retlog = DlgSet( dlg, IDC_EDT_Horizon, fileHorizon )
   retlog = DlgSet( dlg, IDC_EDT_Vector, fileVector)

   retlog = DlgSet( dlg, IDC_Precision0, .TRUE.)

! --------------------------------------------------------------- buttons
   retlog = DlgSetSub( dlg, IDLoad, leggi_da_file )
   retlog = DlgSetSub( dlg, ID_Cancella, ferma_tutto)
   retlog = DlgSetSub( dlg, IDSalva, salva_comandi)
   retlog = DlgSetSub( dlg, IDInformations, messagebox)
! ............................................................... files
   retlog = DlgSetSub( dlg, IDC_BTN_Header, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_BTN_Elevation, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_BTN_Horizon, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_BTN_Vector, aggiorna_campi )
! ............................................................... radio button
   retlog = DlgSetSub( dlg, IDC_Precision0, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_Precision1, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_Precision2, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_Precision3, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_Precision4, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_Precision5, aggiorna_campi )
   retlog = DlgSetSub( dlg, IDC_Precision6, aggiorna_campi )

! Release dialog resources.
   retint = DlgModal( dlg ) 
   CALL DlgUninit( dlg )
END IF
return
end
! *******************************************************************
subroutine aggiorna_campi ( dlg, control_name, callbacktype )
!DEC$ ATTRIBUTES DEFAULT :: aggiorna_campi
USE DFLOGM
use horiz_mdl
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype
character*256 NoCString
TYPE (dialog) dlg
! -------------------------------------------------------------
SELECT CASE (control_name)
! -------------------------------------------
  CASE (IDC_BTN_Header)
    estensione = '*.hdr'
    tipofile = 'Header File'
    call OpenOldFile (retint, estensione, tipofile, fileFormat)
    if (retint .neqv. .false.) then
      fileFormat = NoCString(fileFormat)
      retlog = DlgSet( dlg, IDC_EDT_Header, fileFormat )
      open (1,file=fileFormat,mode='read',err=10)
      call hdrread (1)
      close (1)
!	  call keywrite (20)
10    continue
    endif
! -------------------------------------------
  CASE (IDC_BTN_Elevation)
    estensione = '*.flt'
    tipofile = 'Elevation Grid File'
    call OpenOldFile (retint, estensione, tipofile, fileElevation)
    fileElevation = NoCString(fileElevation)
    retlog = DlgSet( dlg, IDC_EDT_Elevation, fileElevation )
! -------------------------------------------
  CASE (IDC_BTN_Horizon)
    estensione = '*.hrz'
    tipofile = 'Horizon Angles Output File'
    call OpenNewFile (retint, estensione, tipofile, fileHorizon)
    fileHorizon = NoCString(fileHorizon)
    retlog = DlgSet( dlg, IDC_EDT_Horizon, fileHorizon )
! -------------------------------------------
  CASE (IDC_BTN_Vector)
    estensione = '*.vec'
    tipofile = 'Vector Propfile File'
    call OpenOldFile (retint, estensione, tipofile, fileVector)
    fileVector = NoCString(fileVector)
    retlog = DlgSet( dlg, IDC_EDT_Vector, fileVector )
    correggi_hrz = (fileVector(1:1) /= ' ')
! -------------------------------------------
  CASE (IDC_Precision0)
       dettaglio = 'Full Precision '
       sector = .false.
  CASE (IDC_Precision1:IDC_Precision6)
       precisione = control_name-IDC_Precision1+1
       write (dettaglio,'(i3,'' sample(s)'')')  precisione
       sector = .true.
END SELECT
end subroutine aggiorna_campi
! *****************************************************************
subroutine messagebox ( dlg, control_name, callbacktype )
USE DFLOGM
USE DFLIB
INTEGER control_name, callbacktype
TYPE (dialog) dlg
INTEGER retint
retint = MESSAGEBOXQQ ('Computes the visible orographic horizon'C,'VisibleHorizon'C,MB$OK)
return
end
! *******************************************************************
subroutine ferma_tutto ( dlg, control_name, callbacktype )
!DEC$ ATTRIBUTES DEFAULT :: ferma_tutto
USE DFLOGM
use dflib
use horiz_mdl
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype
TYPE (dialog) dlg
retint = SETEXITQQ(QWIN$EXITNOPERSIST)
stop
return
end
! *******************************************************************
subroutine salva_comandi ( dlg, control_name, callbacktype )
!DEC$ ATTRIBUTES DEFAULT :: salva_comandi
USE DFLOGM
use horiz_mdl
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype
TYPE (dialog) dlg
estensione = '*.hor'
tipofile = 'Command File'
call OpenNewFile (retint, estensione, tipofile, fileComandi)
if (retint .neqv. .false.) then
   open (io_com,file=fileComandi)
   call writeinput (io_com)
endif
return
end
! *******************************************************************
subroutine leggi_da_file ( dlg, control_name, callbacktype )
!DEC$ ATTRIBUTES DEFAULT :: leggi_da_file
USE DFLOGM
use horiz_mdl
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype, IDC
TYPE (dialog) dlg
! ------------------------------------------------
estensione = '*.hor'
tipofile = 'Command File'
call OpenOldFile (retint, estensione, tipofile, fileComandi)
if (retint .eqv. .FALSE.) return
call readinput
! --------------------------------------------------------
 retlog = DlgSet( dlg, IDC_EDT_Header, fileFormat )
 retlog = DlgSet( dlg, IDC_EDT_Elevation, fileElevation )
 retlog = DlgSet( dlg, IDC_EDT_Horizon, fileHorizon )
 retlog = DlgSet( dlg, IDC_EDT_Vector, fileVector)
! ------------------------------------------------------ aggiorna i radio buttons
if (sector) then
  retlog = DlgSetLog( dlg, IDC_Precision1+precisione-1, .TRUE.)
else
  retlog = DlgSetLog( dlg, IDC_Precision0, .TRUE.)
endif
!------------------------------------------------------- file sugli altri forms
return
end subroutine leggi_da_file
! *************************************************************************
! -----------------------------------------
subroutine OpenOldFile (istat, estensione, tipofile, lpStrPtr)
use dfwin
implicit none
integer   iret, istat
integer , parameter :: FILE_LEN = 80
type (T_OPENFILENAME)   OpenFN
character*256           szDirName
character*256           szFile
character*256           szFileTitle
character*256           szFilter
character*100           lpszTitle, lpszDefExt, string 
character*256           lpStrPtr
character*25            tipofile
character*5             estensione
integer :: hWnd = 0, hInst = 0
lpStrPtr = ' '
szFile = estensione//char(0)
szFileTitle = ' '//char(0)
lpszTitle= 'Open an Existing File'//char(0)
lpszDefExt= estensione//char(0)

!szFilter = "Text Files"C//"*.txt"C// "command Files"C//"*.eis;*.csv"C//""C
szFilter = tipofile//char(0)//estensione//char(0)//"All Files"C//"*.*"C//""C


   OpenFN%lStructSize       = sizeof(OpenFN)
   OpenFN%hwndOwner         = hWnd
   OpenFN%hInstance         = hInst
   OpenFN%lpstrFilter       = LOC(szFilter)
   OpenFN%lpstrCustomFilter = NULL
   OpenFN%nMaxCustFilter    = 0
   OpenFN%nFilterIndex      = 1
   OpenFN%lpstrFile         = LOC(szFile)
   OpenFN%nMaxFile          = sizeof(szFile)
   OpenFN%lpstrFileTitle    = LOC(szFileTitle)
   OpenFN%nMaxFileTitle     = sizeof(szFileTitle)
   OpenFN%lpstrInitialDir   = NULL
   OpenFN%lpstrTitle        = LOC(lpszTitle)
   OpenFN%nFileOffset       = 0
   OpenFN%nFileExtension    = 0
   OpenFN%lpstrDefExt       = LOC(lpszDefExt)
   OpenFN%lCustData         = 0

OpenFN%Flags = IOR(OFN_SHOWHELP , IOR(OFN_PATHMUSTEXIST, IOR(OFN_FILEMUSTEXIST , OFN_HIDEREADONLY)))

istat = GetOpenFileName(OpenFN) 
if (istat .neqv. .FALSE.) then
   lpStrPtr = szFile
  return
endif

end
! *************************************************************************
! -----------------------------------------
subroutine OpenNewFile (istat, estensione, tipofile, lpStrPtr)
use dfwin
implicit none
integer   iret, istat
integer , parameter :: FILE_LEN = 80
type (T_OPENFILENAME)   OpenFN
character*256           szDirName
character*256           szFile
character*256           szFileTitle
character*256           szFilter
character*100           lpszTitle, lpszDefExt, string 
character*256           lpStrPtr
character*25            tipofile
character*5             estensione
integer :: hWnd = 0, hInst = 0

lpStrPtr = ' '
szFile = estensione//char(0)
szFileTitle = ' '//char(0)
lpszTitle= 'Open a New File'//char(0)
lpszDefExt= estensione//char(0)

!szFilter = "Text Files"C//"*.txt"C// "Fortran Files"C//"*.f90;*.f"C//""C
szFilter = tipofile//char(0)//estensione//char(0)//"All Files"C//"*.*"C//""C


   OpenFN%lStructSize       = sizeof(OpenFN)
   OpenFN%hwndOwner         = hWnd
   OpenFN%hInstance         = hInst
   OpenFN%lpstrFilter       = LOC(szFilter)
   OpenFN%lpstrCustomFilter = NULL
   OpenFN%nMaxCustFilter    = 0
   OpenFN%nFilterIndex      = 1
   OpenFN%lpstrFile         = LOC(szFile)
   OpenFN%nMaxFile          = sizeof(szFile)
   OpenFN%lpstrFileTitle    = LOC(szFileTitle)
   OpenFN%nMaxFileTitle     = sizeof(szFileTitle)
   OpenFN%lpstrInitialDir   = NULL
   OpenFN%lpstrTitle        = LOC(lpszTitle)
   OpenFN%nFileOffset       = 0
   OpenFN%nFileExtension    = 0
   OpenFN%lpstrDefExt       = LOC(lpszDefExt)
   OpenFN%lCustData         = 0
OpenFN%Flags = IOR(OFN_SHOWHELP , IOR(OFN_PATHMUSTEXIST, OFN_HIDEREADONLY))

istat = GetSaveFileName(OpenFN) 
if (istat .neqv. .FALSE.) then
  lpStrPtr = szFile
  return
endif
end   
! ************************************************************************
