//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Command.rc
//
#define IDD_Command                     101
#define IDSalva                         102
#define IDLoad                          103
#define IDInformations                  104
#define ID_Cancella                     105
#define IDI_SnowRedistribution          106
#define IDC_BTN_Header                  1001
#define IDC_EDT_Header                  1002
#define IDC_BTN_Elevation               1003
#define IDC_EDT_Elevation               1004
#define IDC_BTN_Mask                    1005
#define IDC_EDT_Mask                    1006
#define IDC_BTN_SRF                     1007
#define IDC_EDT_SRF                     1008
#define IDC_BTN_MassFile                1009
#define IDC_EDT_MassFile                1010
#define IDC_BTN_MTDFile                 1011
#define IDC_EDT_MTDFile                 1012
#define IDC_BTN_REAFile                 1013
#define IDC_EDT_REAFile                 1014
#define IDC_BTN_REAFile2                1015
#define IDC_EDT_REAFile2                1016
#define IDC_STA_Algo                    1101
#define IDC_STA_NetAlgo                 1102
#define IDC_CHECK_SRF2                  1104
#define IDC_Algo01                      1111
#define IDC_Algo02                      1112
#define IDC_Algo03                      1113
#define IDC_NetAlgo01                   1201
#define IDC_NetAlgo02                   1202
#define IDC_NetAlgo03                   1203
#define IDC_CHECK_MTD                   1301
#define IDC_CHECK_MTD2                  1302
#define IDC_STA_Dlim                    1303
#define IDC_STA_Slim                    1304
#define IDC_STA_MFW                     1305
#define IDC_EDT_Massa                   1322
#define IDC_EDT_Dlim                    1323
#define IDC_EDT_Slim                    1324
#define IDC_EDT_MFW                     1325
#define IDC_CHECK_REA                   1401
#define IDC_CHECK_REA2                  1402
#define IDC_STA_REA1                    1403
#define IDC_STA_REA2                    1404
#define IDC_STA_ELEREA1                 1405
#define IDC_STA_ELEREA2                 1406
#define IDC_STA_REAI1                   1407
#define IDC_STA_REAI2                   1408
#define IDC_STA_Mass                    1409
#define IDC_EDT_REA1                    1412
#define IDC_EDT_REA2                    1413
#define IDC_EDT_REA3                    1414
#define IDC_EDT_ELEREA1                 1415
#define IDC_EDT_ELEREA2                 1416
#define IDC_EDT_REAI1                   1417
#define IDC_EDT_REAI2                   1418
#define IDC_EDT_REAI3                   1419
#define IDC_STATIC_general              1501
#define IDC_STATIC_MTD                  1502
#define IDC_STATIC_REA                  1503

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        108
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1603
#define _APS_NEXT_SYMED_VALUE           201
#endif
#endif
