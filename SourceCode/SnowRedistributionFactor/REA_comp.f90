! *******************************************************
subroutine REA_comp
!        calcolo REA
!        riordino i dati in senso crescente in un vettore (che ha un andamento a S speculare)
!        faccio una regressione lineare.
!        trovo le intersezioni (A e B) della retta con i dati REA (verso gli estremi).
!        calcolo l'indice per REA da A a zero --> da 0.5 a  1.0 (oppure da 0 a 1)
!        calcolo l'indice per REA da zero a B --> da 1.0 a 1.5 (oppure da 1 a 2)
!        assegno agli estremi bassi (prima di A) il valore 0.5 (0.0)
!        e a quelli alti (dopo B) il valore 1.5 (2.0)
!        verifica che la somma dei REA sia neutra (bilancio = 1)
!        se il bilancio � > 1 abbassa i valori > 1
!        se il bilancio � < 1 alza i valori < 1
! -------------------------------------------------------
use SRF_mdl
USE IFPORT
implicit none
integer*4 iconta, ishift, ipnter, ic0, ic1, ir0, ir1, ic, ir, j
INTEGER (KIND=INT_PTR_KIND()) cell_true
!integer*4 kernel_size
real   radius, emme, qu, rea_syn, rea_min, rea_max, rea_maxval, rea_minval
real   deltalow, deltaupp, reai_average, reaic_min, reaic_max, reaic_med, correction
real*8 esomma, SX, SY, SX2, SXY
! -------------------------------------------------------
write (iwo,'('' ----- Relative Elevation Attribute ---------'')') 
! ------------------------------------------------------- verifiche Raggio >= pixsize
REA_Radius1 = max(REA_Radius1, pixsize)
REA_Radius2 = max(REA_Radius2, pixsize)
!
!
!
!ishift = min(int(REA_Radius1/pixsize),int(REA_Radius2/pixsize))
!kernel_size = 1 +  2 * ishift
!! -------------------------- se il raggio � troppo piccolo annulla la redistribuzione
!if (kernel_size < 5) then
!    where (maschera /= esterno) 
!        REA = 1.0
!    elsewhere
!        REA = esterno
!    endwhere
!else 
    allocate (ReaArray(true_cell))
    ipnter = 0
    do row = 1, no_rows
        do col = 1, no_columns
            if (maschera(col,row) /= esterno) then
! -------------------------- trova REARadius in funzione di ELE
                if (elevation(col,row) >= EleREA2) then
                    Radius = REA_Radius2
                elseif (elevation(col,row) <= EleREA1) then
                    Radius = REA_Radius1
                elseif (abs(EleREA2-EleREA1) > 100.0) then
                    Radius = ((REA_Radius2-REA_Radius1)/(EleREA2-EleREA1)) * (elevation(col,row)-EleREA1) + REA_Radius1
                else
                    Radius = 0.5 * (REA_Radius1 + REA_Radius2)
                endif
                ishift = nint(Radius/pixsize)
                if (ishift >= 1) then
                    ic0 = max(1, col-ishift)
                    ic1 = min(no_columns, col+ishift) 
                    ir0 = max(1, row-ishift)
                    ir1 = min(no_rows, row+ishift) 
                    esomma = 0.
                    iconta = 0            
                    do ir = ir0, ir1
                        do ic = ic0, ic1
                            if (elevation(ic,ir) /= esterno) then
                                esomma = esomma + elevation(ic,ir)
                                iconta = iconta + 1              
                            endif
                        enddo
                    enddo
                    if (iconta > 0) then
                        REA(col,row) = (esomma / iconta) - elevation(col,row)
                    else                 ! ipotesi non realizzabile, solo prudenza
                        REA(col,row) = 0.0    
                    endif
                else
                    REA(col,row) = 0.0
                endif
                ipnter = ipnter + 1
                ReaArray(ipnter) = REA(col,row)
            else
                REA(col,row) = esterno
            endif
        enddo
    enddo
! -------------------- a questo punto REA � la differenza di quota 
    if (fileREA(1:1) /= ' ') then
        open (127, file = fileREA, form='binary',recl=4)
        do row=1,no_rows
            write (127) (REA(col,row), col=1,no_columns)  
        enddo
        close (127)
    endif
! -----------------------------------------------
    if (fileREAtxt(1:1) /= ' ') then
        open (127, file = fileREAtxt)
        do row=1,no_rows
            do col=1,no_columns,idata_skip
                if (REA(col,row) /= esterno) write (127,'(2i5,f10.2,f10.6)') col, row, elevation(col,row), REA(col,row)
            enddo
        enddo
        close (127)
    endif
! ---------------------- calcolo di REA_index
    cell_true = true_cell
    Call SORTQQ (LOC(ReaArray), cell_true, SRT$REAL4)
! -----------------------------------------------
!    open (127, file ='rea_array.txt')
!    do j=1,true_cell
!        write (127,'(f10.6)') ReaArray(j)
!    enddo
!    close (127)
! -----------------------------------------------
    SY  = 0.
    SX  = 0.
    SX2 = 0.
    SXY = 0.
    do j=1,true_cell
        SY  = SY  + ReaArray(j)
        SX  = SX  + real(j)
        SX2 = SX2 + real(j)*real(j)
        SXY = SXY + real(j)*ReaArray(j)
    enddo
    emme = ((true_cell*SXY)-(SX*SY)) / ((true_cell*SX2)-(SX*SX))
    qu   =    ((SY*SX2)-(SX*SXY))    / ((true_cell*SX2)-(SX*SX))
    do j = 1,true_cell
        REA_syn = emme*real(j) + qu
        if (ReaArray(j) > REA_syn) then
            REA_Min = ReaArray(j)
            exit
        endif
    enddo
    do j = true_cell,1,-1
        REA_syn = emme*real(j) + qu
        if (ReaArray(j) < REA_syn) then
            REA_Max = ReaArray(j)
            exit
        endif
    enddo
    deallocate (ReaArray)
    log_mask = (REA /= esterno)
    REA_MaxVal   = maxval(REA, log_mask)
    REA_MinVal   = minval(REA, log_mask)
! -----------------------------------------------
    DeltaLow = (1.0 - REAI_low) / REA_Min
    DeltaUpp = (REAI_upp - 1.0) / REA_Max
    where (REA /= esterno .and. REA <=0) 
        REA = 1.0 - REA * DeltaLow
    elsewhere (REA /= esterno .and. REA > 0) 
        REA = 1.0 + REA * DeltaUpp
    endwhere
    where (REA /= esterno .and. REA <= REAI_low) REA = REAI_low
    where (REA /= esterno .and. REA >= REAI_upp) REA = REAI_upp
! --------------------------------------------------------------- algoritmo Leopardo
    if (algoritmo == 3) then
!        j = count(MTD /= 1.0 .and. maschera /= esterno)
        where (MTD /= 1.0 .and. maschera /= esterno) REA = 1.0
    endif
    REAI_Average = sum(REA, log_mask) / true_cell
    if (balanceREA) then
! -------------------- verifica che la somma dei REA sia neutra (bilancio = 1)
! -------------------------------------- se il bilancio � > 1 abbassa i valori > 1
        if (REAI_Average > 1.0) then
         log_mask = (REA /= esterno .and. REA > 1.0)
! -------------------------------------- se il bilancio � < 1 alza i valori < 1
       elseif (REAI_Average < 1.0) then
          log_mask = (REA /= esterno .and. REA < 1.0)
       endif
       correction = true_cell * (REAI_Average - 1.0) / real(count(log_mask))
       where (log_mask) REA = REA - correction
    endif
! -----------------------------------------------
    log_mask = (REA /= esterno)
    REAIC_min = minval(REA, log_mask)
    REAIC_max = maxval(REA, log_mask)
    REAIC_med =    sum(REA, log_mask) / true_cell
! -----------------------------------------------
    if (fileStatistic(1:1) /= ' ') then
!        write (101,'('' ==============================================='')')
!        write (101,'('' Numero di Pixel            '',i13  )') true_cell
!        write (101,'('' REA Minimo  reale          '',f20.6)') REA_MinVal
!        write (101,'('' REA Massimo reale          '',f20.6)') REA_MaxVal
!        write (101,'('' REA Minimo  interpolato    '',f20.6)') REA_Min
!        write (101,'('' REA Massimo interpolato    '',f20.6)') REA_Max
!        write (101,'('' -----------------------------------------------'')')
!        write (101,'('' REA Index minimo           '',f20.6)') REAI_low
!        write (101,'('' REA Index massimo          '',f20.6)') REAI_upp
!        write (101,'('' REA Index medio            '',f20.6)') REAI_Average
!        write (101,'('' eccesso di REA Index       '',f20.6)') (REAI_Average - 1.0) * true_cell
!        write (101,'('' -----------------------------------------------'')')
!        write (101,'('' REA Index Corretto minimo  '',f20.6)') REAIC_min
!        write (101,'('' REA Index Corretto massimo '',f20.6)') REAIC_max
!        write (101,'('' REA Index Corretto medio   '',f20.6)') REAIC_med 
!        write (101,'('' eccesso di REA Index Corr. '',f20.6)') (REAIC_med - 1.0) * true_cell

        write (101,'('' ==============================================='')')
        write (101,'('' Number of Pixels           '',i13  )') true_cell
        write (101,'('' REA Minimum computed       '',f20.6)') REA_MinVal
        write (101,'('' REA Maximum computed       '',f20.6)') REA_MaxVal
        write (101,'('' REA Minimum interpolated   '',f20.6)') REA_Min
        write (101,'('' REA Maximum interpolated   '',f20.6)') REA_Max
        write (101,'('' -----------------------------------------------'')')
        write (101,'('' REA Index minimum          '',f20.6)') REAI_low
        write (101,'('' REA Index maximum          '',f20.6)') REAI_upp
        write (101,'('' REA Index average          '',f20.6)') REAI_Average
        write (101,'('' REA index excess           '',f20.6)') (REAI_Average - 1.0) * true_cell
        write (101,'('' -----------------------------------------------'')')
        write (101,'('' REA Index minimum adjusted '',f20.6)') REAIC_min
        write (101,'('' REA Index maximum adjusted '',f20.6)') REAIC_max
        write (101,'('' REA Index average adjusted '',f20.6)') REAIC_med 
        write (101,'('' adjusted REA Index excess  '',f20.6)') (REAIC_med - 1.0) * true_cell
    endif
! -----------------------------------------------------------------
    if (fileREAindex(1:1) /= ' ') then
        open (127, file = fileREAindex, form='binary',recl=4)
        do row=1,no_rows
            write (127) (REA(col,row), col=1,no_columns)  
        enddo
        close (127)
    endif
! -----------------------------------------------
    if (fileREAindextxt(1:1) /= ' ') then
        open (127, file = fileREAindextxt)
        do row=1,no_rows
            do col=1,no_columns,idata_skip
                if (REA(col,row) /= esterno) write (127,'(2i5,f10.2,f10.6)') col, row, elevation(col,row), REA(col,row)
            enddo
        enddo
        close (127)
    endif
! -----------------------------------------------
!endif
return
end
