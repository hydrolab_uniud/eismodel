! **************************************************************************
module SRF_mdl
! ----------------------------------------------------------------------
parameter (PI=3.141592653589793238462)
parameter (A090 = PI/2.0, A180 = PI, A270 = PI*3.0/2.0, A360 = PI*2.0)
parameter (GPI  = 180.0, GPI2 =  90.0, GPI4 =  45.0)
parameter (OUTLET=-100., GRANDE=1e9)
integer    no_columns, no_rows, row, col, iwo, ifo, ico
integer    i_sh_col(8), i_sh_row(8), ipos(1), shc(4), shr(4)
integer :: retint, algoritmo = 3, net_algo = 1
integer    cell_cnt, true_cell, idata_skip
integer    ic_outlet, ir_outlet
integer, allocatable :: assegnato(:,:)
real*8     aspect(8), slope(8), peso(8)
real*8     or_easting, or_northing
real       pixsize, esterno, undefined
real       quota(8), blue_weight
real    :: MassInput = 10., Dmax, Dlim = 80., Slim =40., pesoMF = 70.0, pesatoreMF
real    :: REA_Radius1=40., REA_Radius2=120., EleREA1=2700., EleREA2=3500., REAI_low=0.5, REAI_upp=1.5
real, allocatable :: elevation(:,:), maschera(:,:), pendenza(:,:)    
real, allocatable :: angle(:,:), MTD(:,:), REA(:,:), SRF(:,:)
real, allocatable :: InputMass(:,:), MobileMass(:,:), DeposMass(:,:), Dmaxgrd(:,:)
real, allocatable :: denominatore(:,:)
real, allocatable :: ReaArray(:)
logical       retlog, outlet_down, outlet_left, esiste, allowdisp
logical       calcolaREA, calcolaMTD, balanceREA, balanceMTD, balanceSRF
logical, allocatable :: log_mask(:,:)
character*256 fileComandi, fileFormat, fileElevation, fileMassa, fileMask
character*256 fileMTD, fileSRF, fileStatistic
character*256 fileREA, fileREAtxt, fileREAindex, fileREAindextxt
character*256 text
character*25  tipofile
character*10  :: byteorder = 'LSBFIRST'
character*9   estensione
data          undefined / -8888.0 /, esterno / -9999.0 /
data          iwo /0/, ifo /20/, ico /10/
data          fileComandi, fileFormat, fileElevation, fileMassa, fileMask, fileSRF /6*' '/
data          fileREA, fileREAtxt, fileREAindex, fileREAindextxt, fileREAstatistic /5*' '/
data          fileMTD, fileMTDbal  /2*' '/
data          calcolaMTD /.true./, calcolaREA /.true./ 
data          balanceMTD /.true./, balanceREA /.true./, balanceSRF /.true./
data          i_sh_col   /  1,  0, -1, -1, -1,  0,  1,  1/
data          i_sh_row   / -1, -1, -1,  0,  1,  1,  1,  0/
data          shc        /  0, -1,  1,  0/ 
data          shr        / -1,  0,  0,  1/
! -----------------------------------------------------------------------------
end module SRF_mdl


