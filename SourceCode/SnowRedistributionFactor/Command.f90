! *****************************************************************
subroutine FormControl
USE DFLOGM
use SRF_mdl
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype, IDC, IndexAlgo, IndexNetAlgo
!LOGICAL vero
TYPE (dialog) dlg
external  aggiorna_form, leggi_da_file, ferma_tutto, salva_comandi, messagebox

! ----------------------------------------------------------------
IF ( .not. DlgInit( IDD_Command, dlg ) ) THEN
   WRITE (*,*) "Error: dialog not found"
ELSE
    retlog = DlgSet( dlg, IDC_EDT_Header, fileFormat )
    retlog = DlgSet( dlg, IDC_EDT_Elevation, fileElevation )
    retlog = DlgSet( dlg, IDC_EDT_Mask, fileMask )
    retlog = DlgSet( dlg, IDC_EDT_SRF, fileSRF )    
    retlog = DlgSet( dlg, IDC_EDT_MassFile, fileMassa)    
    retlog = DlgSet( dlg, IDC_EDT_MTDFile, fileMTD )
    retlog = DlgSet( dlg, IDC_EDT_REAFile, fileREA )
    retlog = DlgSet( dlg, IDC_EDT_REAFile2, fileREAindex )

    write (text,'(f14.5)') MassInput
    retlog = DlgSet( dlg, IDC_EDT_Massa, text)
    write (text,'(f14.5)') Dlim
    retlog = DlgSet( dlg, IDC_EDT_Dlim, text)
    write (text,'(f14.5)') Slim
    retlog = DlgSet( dlg, IDC_EDT_Slim, text)
    write (text,'(f14.5)') pesoMF
    retlog = DlgSet( dlg, IDC_EDT_MFW, text)
    write (text,'(f14.5)') REA_Radius1
    retlog = DlgSet( dlg, IDC_EDT_REA1, text)
    write (text,'(f14.5)') REA_Radius2
    retlog = DlgSet( dlg, IDC_EDT_REA2, text)
    write (text,'(f14.5)') EleREA1
    retlog = DlgSet( dlg, IDC_EDT_ELEREA1, text)
    write (text,'(f14.5)') EleREA2
    retlog = DlgSet( dlg, IDC_EDT_ELEREA2, text)
    write (text,'(f14.5)') REAI_low
    retlog = DlgSet( dlg, IDC_EDT_REAI1, text)
    write (text,'(f14.5)') REAI_upp
    retlog = DlgSet( dlg, IDC_EDT_REAI2, text)
! -------------------------------------------
    IndexAlgo = algoritmo + IDC_Algo01 - 1
    retlog = DlgSet( dlg, IndexAlgo, .true.)
    IndexNetAlgo = net_algo + IDC_NetAlgo01 - 1
    retlog = DlgSet( dlg, IndexNetAlgo, .true.)
    
    retlog = DlgSet( dlg, IDC_CHECK_MTD, calcolaMTD)
    retlog = DlgSet( dlg, IDC_CHECK_REA, calcolaREA)
    retlog = DlgSet( dlg, IDC_CHECK_MTD2, balanceMTD)
    retlog = DlgSet( dlg, IDC_CHECK_REA2, balanceREA)
    retlog = DlgSet( dlg, IDC_CHECK_SRF2, balanceSRF)

! --------------------------------------------------------------- buttons
    retlog = DlgSetSub( dlg, IDLoad, leggi_da_file )
    retlog = DlgSetSub( dlg, ID_Cancella, ferma_tutto)
    retlog = DlgSetSub( dlg, IDSalva, salva_comandi)
    retlog = DlgSetSub( dlg, IDInformations, messagebox)
! ...............................................................
    retlog = DlgSetSub( dlg, IDC_BTN_Header, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_BTN_Elevation, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_BTN_Mask, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_BTN_SRF, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_BTN_MassFile, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_BTN_MTDFile, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_BTN_REAFile, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_BTN_REAFile2, aggiorna_form )
    
    retlog = DlgSetSub( dlg, IDC_EDT_Massa, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_EDT_Dlim, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_EDT_Slim, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_EDT_MFW, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_EDT_REA1, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_EDT_REA2, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_EDT_ELEREA1, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_EDT_ELEREA2, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_EDT_REAI1, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_EDT_REAI2, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_Algo01, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_Algo02, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_Algo03, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_NetAlgo01, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_NetAlgo02, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_NetAlgo03, aggiorna_form )
    retlog = DlgSetSub( dlg, IDC_CHECK_MTD,  aggiorna_form)
    retlog = DlgSetSub( dlg, IDC_CHECK_REA,  aggiorna_form)
    retlog = DlgSetSub( dlg, IDC_CHECK_MTD2, aggiorna_form)
    retlog = DlgSetSub( dlg, IDC_CHECK_REA2, aggiorna_form)
    retlog = DlgSetSub( dlg, IDC_CHECK_SRF2, aggiorna_form)
! ...............................................................
    retint = DlgModal( dlg ) 
    CALL DlgUninit( dlg )
END IF
return
end
! *****************************************************************
subroutine messagebox ( dlg, control_name, callbacktype )
USE DFLOGM
USE DFLIB
INTEGER control_name, callbacktype
TYPE (dialog) dlg
INTEGER retint
retint = MESSAGEBOXQQ ('Snow Redist. Factor'C,'SRF'C,MB$OK)
return
end
! *******************************************************************
subroutine ferma_tutto ( dlg, control_name, callbacktype )
!DEC$ ATTRIBUTES DEFAULT :: ferma_tutto
USE DFLOGM
use dflib
use SRF_mdl
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype
TYPE (dialog) dlg
retint = SETEXITQQ(QWIN$EXITNOPERSIST)
stop
return
end
! *******************************************************************
subroutine salva_comandi ( dlg, control_name, callbacktype )
!DEC$ ATTRIBUTES DEFAULT :: salva_comandi
USE DFLOGM
use SRF_mdl
INCLUDE 'RESOURCE.FD'
TYPE (dialog) dlg
estensione = '*.srf'
tipofile = 'Command File'
call OpenNewFile (retint, estensione, tipofile, fileComandi)
if (retint .neqv. .false.) then
   open (ico,file=fileComandi)
   call writeinput (ico)
   close (ico) 
endif
return
end
! *******************************************************************
subroutine leggi_da_file ( dlg, control_name, callbacktype )
!DEC$ ATTRIBUTES DEFAULT :: leggi_da_file
USE DFLOGM
use SRF_mdl
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype, IDC, IndexAlgo, IndexNetAlgo
TYPE (dialog) dlg
! ------------------------------------------------
estensione = '*.srf'
tipofile = 'Command File'
call OpenOldFile (retint, estensione, tipofile, fileComandi)
if (retint .eqv. .FALSE.) return
call readinput
! ------------------------------------------------------
    retlog = DlgSet( dlg, IDC_EDT_Header, fileFormat )
    retlog = DlgSet( dlg, IDC_EDT_Elevation, fileElevation )
    retlog = DlgSet( dlg, IDC_EDT_Mask, fileMask )
    retlog = DlgSet( dlg, IDC_EDT_SRF, fileSRF )    
    retlog = DlgSet( dlg, IDC_EDT_MassFile, fileMassa )    
    retlog = DlgSet( dlg, IDC_EDT_MTDFile, fileMTD )
    retlog = DlgSet( dlg, IDC_EDT_REAFile, fileREA )
    retlog = DlgSet( dlg, IDC_EDT_REAFile2, fileREAindex )

    write (text,'(f14.5)') MassInput
    retlog = DlgSet( dlg, IDC_EDT_Massa, text)
    write (text,'(f14.5)') Dlim
    retlog = DlgSet( dlg, IDC_EDT_Dlim, text)
    write (text,'(f14.5)') Slim
    retlog = DlgSet( dlg, IDC_EDT_Slim, text)
    write (text,'(f14.5)') pesoMF
    retlog = DlgSet( dlg, IDC_EDT_MFW, text)
    write (text,'(f14.5)') REA_Radius1
    retlog = DlgSet( dlg, IDC_EDT_REA1, text)
    write (text,'(f14.5)') REA_Radius2
    retlog = DlgSet( dlg, IDC_EDT_REA2, text)
    write (text,'(f14.5)') EleREA1
    retlog = DlgSet( dlg, IDC_EDT_ELEREA1, text)
    write (text,'(f14.5)') EleREA2
    retlog = DlgSet( dlg, IDC_EDT_ELEREA2, text)
    write (text,'(f14.5)') REAI_low
    retlog = DlgSet( dlg, IDC_EDT_REAI1, text)
    write (text,'(f14.5)') REAI_upp
    retlog = DlgSet( dlg, IDC_EDT_REAI2, text)
! -------------------------------------------
    IndexAlgo = algoritmo + IDC_Algo01 - 1
    retlog = DlgSet( dlg, IndexAlgo, .true.)
    IndexNetAlgo = net_algo + IDC_NetAlgo01 - 1
    retlog = DlgSet( dlg, IndexNetAlgo, .true.)
    
    retlog = DlgSet( dlg, IDC_CHECK_MTD, calcolaMTD)
    retlog = DlgSet( dlg, IDC_CHECK_REA, calcolaREA)
    retlog = DlgSet( dlg, IDC_CHECK_MTD2, balanceMTD)
    retlog = DlgSet( dlg, IDC_CHECK_REA2, balanceREA)
    retlog = DlgSet( dlg, IDC_CHECK_SRF2, balanceSRF)

! -----------------------------------------------------------------------
return
end subroutine leggi_da_file
! *******************************************************************
subroutine aggiorna_form ( dlg, control_name, callbacktype )
!DEC$ ATTRIBUTES DEFAULT :: aggiorna_form
USE DFLOGM
use SRF_mdl
IMPLICIT NONE
INCLUDE 'RESOURCE.FD'
INTEGER control_name, callbacktype
character*256 NoCString
TYPE (dialog) dlg
! -------------------------------------------------------------
SELECT CASE (control_name)

! -------------------------------------------
  CASE (IDC_BTN_Header)
    estensione = '*.hdr'
    tipofile = 'Header File'
    call OpenOldFile (retint, estensione, tipofile, fileFormat)
    if (retint .neqv. .false.) then
      fileFormat = NoCString(fileFormat)
      retlog = DlgSet( dlg, IDC_EDT_Header, fileFormat )
      open (1,file=fileFormat,mode='read',err=10)
      call hdrread (1)
      close (1)
10    continue
    endif
! -------------------------------------------
  CASE (IDC_BTN_Elevation)
    estensione = '*.flt'
    tipofile = 'Elevation Grid File'
    call OpenOldFile (retint, estensione, tipofile, fileElevation)
    fileElevation = NoCString(fileElevation)
    retlog = DlgSet( dlg, IDC_EDT_Elevation, fileElevation )
! -------------------------------------------
  CASE (IDC_BTN_Mask)
    estensione = '*.flt'
    tipofile = 'Opt. Area Mask File'
    call OpenOldFile (retint, estensione, tipofile, fileMask)
    fileMask = NoCString(fileMask)
    retlog = DlgSet( dlg, IDC_EDT_Mask, fileMask)
! -------------------------------------------
  CASE (IDC_BTN_MassFile)
    estensione = '*.flt'
    tipofile = 'Opt.Input Mass File'
    call OpenOldFile (retint, estensione, tipofile, fileMassa)
    fileMassa = NoCString(fileMassa)
    retlog = DlgSet( dlg, IDC_EDT_MassFile, fileMassa)
! -------------------------------------------
  CASE (IDC_BTN_SRF)
    estensione = '*.flt'
    tipofile = 'Output SRF File'
    call OpenNewFile (retint, estensione, tipofile, fileSRF)
    fileSRF = NoCString(fileSRF)
    retlog = DlgSet( dlg, IDC_EDT_SRF, fileSRF)
! -------------------------------------------
  CASE (IDC_BTN_MTDFile)
    estensione = '*.flt'
    tipofile = 'Opt.Output MTD File'
    call OpenNewFile (retint, estensione, tipofile, fileMTD)
    fileMTD = NoCString(fileMTD)
    retlog = DlgSet( dlg, IDC_EDT_MTDFile, fileMTD)
! -------------------------------------------
  CASE (IDC_BTN_REAFile)
    estensione = '*.flt'
    tipofile = 'Opt.Output REA File'
    call OpenNewFile (retint, estensione, tipofile, fileREA)
    fileREA = NoCString(fileREA)
    retlog = DlgSet( dlg, IDC_EDT_REAFile, fileREA)
! -------------------------------------------
  CASE (IDC_BTN_REAFile2)
    estensione = '*.flt'
    tipofile = 'Opt.Output REAindex File'
    call OpenNewFile (retint, estensione, tipofile, fileREAIndex)
    fileREAIndex = NoCString(fileREAIndex)
    retlog = DlgSet( dlg, IDC_EDT_REAFile2, fileREAIndex)
! -------------------------------------------
  CASE (IDC_EDT_Massa)
    retlog = DlgGet( dlg, IDC_EDT_Massa, text )
    read (text, *, iostat=retint) MassInput
  CASE (IDC_EDT_Dlim)
    retlog = DlgGet( dlg, IDC_EDT_Dlim, text )
    read (text, *, iostat=retint) Dlim
  CASE (IDC_EDT_Slim)
    retlog = DlgGet( dlg, IDC_EDT_Slim, text )
    read (text, *, iostat=retint) Slim
  CASE (IDC_EDT_MFW)
    retlog = DlgGet( dlg, IDC_EDT_MFW, text )
    read (text, *, iostat=retint) PesoMF
! -------------------------------------------
  CASE (IDC_EDT_REA1)
    retlog = DlgGet( dlg, IDC_EDT_REA1, text )
    read (text, *, iostat=retint) REA_Radius1
  CASE (IDC_EDT_REA2)
    retlog = DlgGet( dlg, IDC_EDT_REA2, text )
    read (text, *, iostat=retint) REA_Radius2
  CASE (IDC_EDT_ELEREA1)
    retlog = DlgGet( dlg, IDC_EDT_ELEREA1, text )
    read (text, *, iostat=retint) EleREA1
  CASE (IDC_EDT_ELEREA2)
    retlog = DlgGet( dlg, IDC_EDT_ELEREA2, text )
    read (text, *, iostat=retint) EleREA2
  CASE (IDC_EDT_REAI1)
    retlog = DlgGet( dlg, IDC_EDT_REAI1, text )
    read (text, *, iostat=retint) REAI_low
  CASE (IDC_EDT_REAI2)
    retlog = DlgGet( dlg, IDC_EDT_REAI2, text )
    read (text, *, iostat=retint) REAI_upp
! -------------------------------------------
  CASE (IDC_Algo01:IDC_Algo03)
       algoritmo = control_name - IDC_Algo01 + 1
  CASE (IDC_NetAlgo01:IDC_NetAlgo03)
       net_algo = control_name - IDC_NetAlgo01 + 1
  CASE (IDC_CHECK_MTD)
       calcolaMTD = .not.calcolaMTD
  CASE (IDC_CHECK_REA)
       calcolaREA = .not.calcolaREA
  CASE (IDC_CHECK_MTD2)
       balanceMTD = .not.balanceMTD
  CASE (IDC_CHECK_REA2)
       balanceREA = .not.balanceREA
  CASE (IDC_CHECK_SRF2)
       balanceSRF = .not.balanceSRF
! -------------------------------------------
END SELECT
end subroutine aggiorna_form
! *************************************************************************
! -----------------------------------------
subroutine OpenOldFile (istat, estensione, tipofile, lpStrPtr)
use dfwin
implicit none
integer   iret, istat
integer , parameter :: FILE_LEN = 80
type (T_OPENFILENAME)   OpenFN
character*256           szDirName
character*256           szFile
character*256           szFileTitle
character*256           szFilter
character*100           lpszTitle, lpszDefExt, string 
character*256           lpStrPtr
character*25            tipofile
character*5             estensione
integer :: hWnd = 0, hInst = 0
lpStrPtr = ' '
szFile = estensione//char(0)
szFileTitle = ' '//char(0)
lpszTitle= 'Open an Existing File'//char(0)
lpszDefExt= estensione//char(0)

!szFilter = "Text Files"C//"*.txt"C// "command Files"C//"*.eis;*.csv"C//""C
szFilter = tipofile//char(0)//estensione//char(0)//"All Files"C//"*.*"C//""C


   OpenFN%lStructSize       = sizeof(OpenFN)
   OpenFN%hwndOwner         = hWnd
   OpenFN%hInstance         = hInst
   OpenFN%lpstrFilter       = LOC(szFilter)
   OpenFN%lpstrCustomFilter = NULL
   OpenFN%nMaxCustFilter    = 0
   OpenFN%nFilterIndex      = 1
   OpenFN%lpstrFile         = LOC(szFile)
   OpenFN%nMaxFile          = sizeof(szFile)
   OpenFN%lpstrFileTitle    = LOC(szFileTitle)
   OpenFN%nMaxFileTitle     = sizeof(szFileTitle)
   OpenFN%lpstrInitialDir   = NULL
   OpenFN%lpstrTitle        = LOC(lpszTitle)
   OpenFN%nFileOffset       = 0
   OpenFN%nFileExtension    = 0
   OpenFN%lpstrDefExt       = LOC(lpszDefExt)
   OpenFN%lCustData         = 0

OpenFN%Flags = IOR(OFN_SHOWHELP , IOR(OFN_PATHMUSTEXIST, IOR(OFN_FILEMUSTEXIST , OFN_HIDEREADONLY)))

istat = GetOpenFileName(OpenFN) 
if (istat .neqv. .FALSE.) then
   lpStrPtr = szFile
  return
endif

end
! *************************************************************************
! -----------------------------------------
subroutine OpenNewFile (istat, estensione, tipofile, lpStrPtr)
use dfwin
implicit none
integer   iret, istat
integer , parameter :: FILE_LEN = 80
type (T_OPENFILENAME)   OpenFN
character*256           szDirName
character*256           szFile
character*256           szFileTitle
character*256           szFilter
character*100           lpszTitle, lpszDefExt, string 
character*256           lpStrPtr
character*25            tipofile
character*5             estensione
integer :: hWnd = 0, hInst = 0

lpStrPtr = ' '
szFile = estensione//char(0)
szFileTitle = ' '//char(0)
lpszTitle= 'Open a New File'//char(0)
lpszDefExt= estensione//char(0)

!szFilter = "Text Files"C//"*.txt"C// "Fortran Files"C//"*.f90;*.f"C//""C
szFilter = tipofile//char(0)//estensione//char(0)//"All Files"C//"*.*"C//""C


   OpenFN%lStructSize       = sizeof(OpenFN)
   OpenFN%hwndOwner         = hWnd
   OpenFN%hInstance         = hInst
   OpenFN%lpstrFilter       = LOC(szFilter)
   OpenFN%lpstrCustomFilter = NULL
   OpenFN%nMaxCustFilter    = 0
   OpenFN%nFilterIndex      = 1
   OpenFN%lpstrFile         = LOC(szFile)
   OpenFN%nMaxFile          = sizeof(szFile)
   OpenFN%lpstrFileTitle    = LOC(szFileTitle)
   OpenFN%nMaxFileTitle     = sizeof(szFileTitle)
   OpenFN%lpstrInitialDir   = NULL
   OpenFN%lpstrTitle        = LOC(lpszTitle)
   OpenFN%nFileOffset       = 0
   OpenFN%nFileExtension    = 0
   OpenFN%lpstrDefExt       = LOC(lpszDefExt)
   OpenFN%lCustData         = 0
OpenFN%Flags = IOR(OFN_SHOWHELP , IOR(OFN_PATHMUSTEXIST, OFN_HIDEREADONLY))

istat = GetSaveFileName(OpenFN) 
if (istat .neqv. .FALSE.) then
  lpStrPtr = szFile
  return
endif
end

