! **********************************************************************
subroutine MTD_comp
! -------------------------- costruzione Grids drenaggio multialgoritmo
use SRF_mdl
implicit none
integer ic, ir
real excessmass, singleexcess, rmassainput, rmassamobile, rmassadepos,  correction
real rmtd_maxval, rmtd_minval, rmtd_average, rmtdic_min, rmtdic_max, rmtdic_med
! ----------------------------------------------------------------------
write (iwo,'('' ----- Mass Transport Deposition ------------'')') 
log_mask = (maschera /= esterno)
select case (net_algo)
case (1)
    allowdisp = .true.
    call dinfinito_azimuth
    call reticolo
case (2)
    allowdisp = .false.
    call singleflow_azimuth
    call reticolo
case (3)
    allowdisp = .true.
    call multiflow_denominatore
    call reticolo
!case (4)
!    allowdisp = .true.
!    call dinfinito_azimuth
!    call Dinf4_denominatore
!    call reticolo
case default   ! ------------------ default = d-infinite
    allowdisp = .true.
    call dinfinito_azimuth
    call reticolo
end select
ExcessMass = 0.
do ir = 1, no_rows
    do ic = 1, no_columns
        if (log_mask(ic,ir) .and. MobileMass(ic,ir) > Dmaxgrd(ic,ir)) then
            ExcessMass = ExcessMass + MobileMass(ic,ir) - Dmaxgrd(ic,ir)
            MobileMass(ic,ir) = Dmaxgrd(ic,ir)
        endif
    enddo
enddo   
! ---------------------------------------------------------------- redistribuzione dell'eccesso sulle zone di deposito
MTD = esterno
cell_cnt = count(log_mask .and. MobileMass > InputMass)
SingleExcess = ExcessMass / cell_cnt 
where(log_mask .and. MobileMass > InputMass) MobileMass = MobileMass + SingleExcess
where(log_mask) MTD = MobileMass / InputMass
! ---------------------------------------------------------------- marca i pixel modificati
!ic = count(MTD /= 1.0 .and. maschera /= esterno)
if (fileMTD(1:1) /= ' ') then
    open (127,file=adjustl(fileMTD),form='binary')
    do ir = 1, no_rows
      write (127) (MTD(ic,ir),ic=1,no_columns)
    enddo
    close (127)
endif
RmassaInput  = sum(InputMass ,log_mask) 
RmassaMobile = sum(MobileMass,log_mask)
RmassaDepos  = sum(DeposMass ,log_mask)          
! -----------------------------------
log_mask = (MTD /= esterno)
rMTD_MaxVal   = maxval(MTD, log_mask)
rMTD_MinVal   = minval(MTD, log_mask)
! -------------------- verifica che la somma dei MTD sia neutra (bilancio = 1)
rMTD_Average = sum(MTD, log_mask) / true_cell
if (balanceMTD) then
! -------------------------------------- se il bilancio � /= 1 aggiusta i valori > 1
    if (rMTD_Average /= 1.0) then
        log_mask = (MTD /= esterno .and. MTD > 1.0)
    endif
    correction = true_cell * (rMTD_Average - 1.0) / real(count(log_mask))
    where (log_mask) MTD = MTD - correction
endif
log_mask = (MTD /= esterno)
rMTDIC_min = minval(MTD, log_mask)
rMTDIC_max = maxval(MTD, log_mask)
rMTDIC_med =    sum(MTD, log_mask) / true_cell
! -----------------------------------------------
if (fileStatistic(1:1) /= ' ') then
!    write (101,'('' ==============================================='')')
!    write (101,'('' Numero di Pixel            '',i13  )') true_cell
!    write (101,'('' MTD Minimo  reale          '',f20.6)') rMTD_MinVal
!    write (101,'('' MTD Massimo reale          '',f20.6)') rMTD_MaxVal
!    write (101,'('' MTD medio   reale          '',f20.6)') rMTD_Average
!    write (101,'('' -----------------------------------------------'')')
!    write (101,'('' MTD Corretto minimo        '',f20.6)') rMTDIC_min
!    write (101,'('' MTD Corretto massimo       '',f20.6)') rMTDIC_max
!    write (101,'('' MTD Corretto medio         '',f20.6)') rMTDIC_med 
!    write (101,'('' eccesso di MTD Corretto    '',f20.6)') (rMTDIC_med - 1.0) * true_cell
!    write (101,'('' -----------------------------------------------'')')
!    write (101,'('' Massa Iniziale             '',f20.6)') RmassaInput
!    write (101,'('' Massa Finale               '',f20.6)') RmassaMobile
!    write (101,'('' Massa Depositi             '',f20.6)') RmassaDepos

    write (101,'('' ==============================================='')')
    write (101,'('' Number of Pixels           '',i13  )') true_cell
    write (101,'('' MTD Minimum computed       '',f20.6)') rMTD_MinVal
    write (101,'('' MTD Maximum computed       '',f20.6)') rMTD_MaxVal
    write (101,'('' MTD Average computed       '',f20.6)') rMTD_Average
    write (101,'('' -----------------------------------------------'')')
    write (101,'('' MTD Minimum adjusted       '',f20.6)') rMTDIC_min
    write (101,'('' MTD Maximum adjusted       '',f20.6)') rMTDIC_max
    write (101,'('' MTD Average adjusted       '',f20.6)') rMTDIC_med 
    write (101,'('' adjusted MTD excess        '',f20.6)') (rMTDIC_med - 1.0) * true_cell
    write (101,'('' -----------------------------------------------'')')
    write (101,'('' Mass  Initial              '',f20.6)') RmassaInput
    write (101,'('' Mass  Final                '',f20.6)') RmassaMobile
    write (101,'('' Mass  Deposition           '',f20.6)') RmassaDepos
endif
return
end
! **********************************************************************
subroutine reticolo
use SRF_mdl
implicit none
integer i_frst_row, i_last_row, i_step_row, i_frst_col, i_last_col, i_step_col
integer irow, icol
! ----------------------------------------------------------------------
write (iwo,'( ''          0 row     -> Upslope Area computation ...'')')
! -------------------------------------------------------------------
call begin (i_frst_row, i_last_row, i_step_row, i_frst_col, i_last_col, i_step_col)
! .........................................................
! ----------------------------------------- loop delle y
do irow = i_frst_row, i_last_row, i_step_row
! -------------------------------------------- loop delle x
        do icol = i_frst_col, i_last_col, i_step_col
        ! ---------------------------------------------------------
            write (iwo,'(''+'',2i10)') irow
                select case (net_algo)
            case (1)
!                if (angle(icol,irow)        == OUTLET)  call DI_area (icol,irow)
                call DI_area (icol,irow)
            case (2)
!                if (angle(icol,irow)        == OUTLET)  call SF_area (icol,irow) 
                call SF_area (icol,irow)
            case (3)
!                if (denominatore(icol,irow) == OUTLET)  call MF_area (icol,irow)  
                call MF_area (icol,irow)
!            case (4)
!                if (denominatore(icol,irow) == OUTLET)  call D4_area (icol,irow)  
            case default   ! ------------------ default = d-infinite
!                if (angle(icol,irow)        == OUTLET)  call DI_area (icol,irow) 
                call DI_area (icol,irow)
            end select
        enddo
enddo
! ------------------------------------ loop colonne e righe
return
end
! **********************************************************************
